import React from 'react'
import styled from 'styled-components/native'
import {SafeAreaView} from 'react-native'
import 'react-native-gesture-handler'
import {NavigationContainer, DefaultTheme} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'

import {SignIn} from './src/components/sign-in/sign-in'
import {Dashboard} from './src/components/dashboard/dashboard'
import {AllApplications} from './src/components/all-applications/all-applications'
import {FormRenderer} from './src/components/forms/form-renderer'
import {FormComplete} from "./src/components/forms/form-complete"
import {FormType, ValueType} from "./src/services/form-service"
import {Processing} from './src/components/forms/processing'
import {FormOnSuccess} from "./src/models/form"
import {FormError} from "./src/components/forms/form-error"
import {AppStorageView} from './src/components/app-storage-view'
import {FormPending} from './src/components/forms/form-pending'
import {LogViewer} from "./src/components/debug/log-viewer"
import {OfflineApplications} from "./src/components/offline/offline-applications"
import {ActivateCard} from "./src/components/data-display/activate-card"
import {ActivateCardComplete} from "./src/components/data-display/activate-card-complete"
import {ActivateInsurance} from "./src/components/data-display/activate-insurance"
import {ActivateInsuranceStep4} from "./src/components/data-display/activate-insurance-step-4"
import {ActivateInsuranceComplete} from "./src/components/data-display/activate-insurance-complete"

declare const global: { HermesInternal: null | {} }

const AppTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#d71920',
    background: 'white',
  },
}

export type RootStackParamLList = {
  Main: undefined
  LogViewer: undefined
  OfflineApplications: undefined
}

export type MainStackParamList = {
  SignIn: undefined
  Dashboard: undefined
  AllApplications: undefined
  FormRenderer: {
    form: FormType,
    formData?: { [key: string]: ValueType },
    processor?: string,
    canGoBack?: boolean,
  }
  FormSuccess: {
    creditLimit?: string
  }
  FormError: {
    title: string,
    message: string
  }
  FormPending: {
    creditLimit: string
  }
  Processing: {
    processor: string,
    formData: { [key: string]: any },
    onSuccess: FormOnSuccess,
  }
  ActivateCard: {
    mustUploadDocuments: boolean
    idNumber: string
    commQueueId: number
  },
  ActivateCardComplete: undefined,
  ActivateInsurance: undefined,
  ActivateInsuranceComplete: undefined,
}

const RootStack = createStackNavigator<RootStackParamLList>()
const MainStack = createStackNavigator<MainStackParamList>()

const App = () => {
  return (
    <AppStorageView>
      <SafeAreaView>
        <ContentWrap>
          <NavigationContainer theme={AppTheme}>
            <RootStack.Navigator mode={"modal"}>
              <RootStack.Screen name={"Main"} component={MainStackScreen} options={{ headerShown: false }} />
              <RootStack.Screen name={"LogViewer"} component={LogViewer}/>
              <RootStack.Screen name={"OfflineApplications"} component={OfflineApplications}/>
            </RootStack.Navigator>
          </NavigationContainer>
        </ContentWrap>
      </SafeAreaView>
    </AppStorageView>
  )
}

const MainStackScreen = () => {
    return (
      <MainStack.Navigator initialRouteName={"SignIn"}>
        <MainStack.Screen name={"SignIn"} component={SignIn} options={{headerShown: false}}/>
        <MainStack.Screen name={"Dashboard"} component={Dashboard} options={{headerShown: false}}/>
        <MainStack.Screen name={"AllApplications"} component={AllApplications} options={{title: 'All Applications'}}/>
        <MainStack.Screen name={"FormRenderer"} component={FormRenderer}/>
        <MainStack.Screen name={"FormSuccess"} component={FormComplete} options={{headerShown: false}}/>
        <MainStack.Screen name={"FormError"} component={FormError} options={{headerShown: false}}/>
        <MainStack.Screen name={"FormPending"} component={FormPending} options={{headerShown: false}}/>
        <MainStack.Screen name={"Processing"} component={Processing} options={{headerShown: false}}/>
        <MainStack.Screen name={"ActivateCard"} component={ActivateCard} options={{ title: "Account Activation" }}/>
        <MainStack.Screen name={"ActivateCardComplete"} component={ActivateCardComplete} options={{headerShown: false}}/>
        <MainStack.Screen name={"ActivateInsurance"} component={ActivateInsurance} options={{ title: "Insurance" }}/>
        <MainStack.Screen name={"ActivateInsuranceComplete"} component={ActivateInsuranceComplete} options={{headerShown: false}}/>
      </MainStack.Navigator>
    )
}

const ContentWrap = styled.View`
  width: 100%;
  height: 100%;
`

export default App
