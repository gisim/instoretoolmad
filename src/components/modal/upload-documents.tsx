import React, {useState} from "react"
import styled from "styled-components/native"
import {AppStorageService} from "../../services/app-storage-service"
import {DefaultButton} from "../data-capture/default-button"
import ImagePicker, {ImagePickerOptions} from "react-native-image-picker"
import {Uploader} from "../data-capture/uploader"
import {v4 as uuid} from 'uuid'
import {Alert, Platform, ScrollView, Text} from "react-native"
import {LookUpData} from "../../models/lookup-data"
import {Camera} from "../data-capture/camera"
import {DefaultModal} from "./default-modal"

interface UploadDocumentsProps {
  communicationQueueId: number
  documentUploaded?: (isSuccess: boolean) => void
}

export type UploadRequest = {
  id: string
  type: string
  typeId: number
  communicationQueueId: number
  base64: string|null
}

const ImageOptions: ImagePickerOptions = {
  mediaType: "photo",
  maxWidth: 2048,
  quality: 0.8,
  storageOptions: {
    cameraRoll: false,
    privateDirectory: true,
  },
}

export const UploadDocuments: React.FC<UploadDocumentsProps> = (props) => {
  const supportingDocuments = AppStorageService.lookUp.SupportingDocuments
    .filter(sd => sd.Id !== 1)

  const [uploadRequests, setUploadRequests] = useState<UploadRequest[]>([])
  const [isShowingCamera, setIsShowingCamera] = useState<LookUpData|null>(null)

  const launchCamera = (sd: LookUpData) => {
    switch (Platform.OS) {
      case "ios": {
        launchCameraiOS(sd)
        break
      }
      case "android": {
        launchCameraAndroid(sd)
        break
      }
      default: {
        break
      }
    }
  }

  const launchCameraAndroid = (sd: LookUpData) => {
    setIsShowingCamera(sd)
  }

  const launchCameraiOS = (sd: LookUpData) => {
    ImagePicker.launchCamera(ImageOptions, (response) => {
      if (response.didCancel) { return }
      if (response.error) {
        Alert.alert('Camera Error', `Please ensure you have allowed camera access. If you haven't, please open Settings and allow camera access for this app.`, [
          { text: "Close" },
        ])
        return
      }
      addUploadRequest(sd, response.data)
    })
  }

  const addUploadRequest = (sd: LookUpData, base64: string) => {
    const request: UploadRequest = {
      type: sd.Description,
      id: uuid(),
      base64: base64,
      communicationQueueId: props.communicationQueueId,
      typeId: sd.Id,
    }
    setUploadRequests(currentRequests => {
      const newArray = currentRequests.slice(0)
      newArray.unshift(request)
      return newArray
    })
  }

  const uploaders = supportingDocuments.map(sd => (
    <UploadButton key={`${sd.Id}`}>
      <DefaultButton
        buttonType={"light"}
        title={sd.Description}
        disabled={false}
        onPress={() => launchCamera(sd)}
      />
    </UploadButton>
  ))

  return (
    <Container>
      <Text style={{ marginBottom: 25 }}>
        Please include the following documents where possible.
      </Text>
      <ButtonContainer>
        {uploaders}
      </ButtonContainer>
      <Text style={{ fontWeight: "bold", marginTop: 25 }}>
        Documents Uploaded
      </Text>
      {uploadRequests.length === 0 &&
        <Text style={{ marginTop: 15 }}>
            Select an option above to start uploading. Uploaded documents will appear here.
        </Text>
      }
      <ScrollView style={{ marginTop: 20 }} contentContainerStyle={{ display: "flex", alignItems: "center", justifyContent: "space-between", flexDirection: "row", flexWrap: "wrap" }}>
        {uploadRequests.map(ur => (
          <Uploader
            request={ur}
            key={ur.id}
            documentUploaded={(isSuccess) => {
              if (isSuccess) {
                if (!props.documentUploaded) { return }
                props.documentUploaded(true)
              }
            }}
          />
        ))}
      </ScrollView>

      <DefaultModal
        visible={isShowingCamera !== null}
        title={'Upload Identity Document'}
        headerType={"small"}
        showBottomButtons={true}
      >
        <Camera
          onCancel={() => setIsShowingCamera(null)}
          onUploadComplete={(base64) => {
            if (!isShowingCamera) {return }
            addUploadRequest(isShowingCamera, base64)
            setIsShowingCamera(null)
          }}
        />
      </DefaultModal>
    </Container>
  )
}

const Container = styled.View`
  height: 100%;
  padding: 14px;
  display: flex;
  align-items: flex-start; 
  justify-content: flex-start;
  padding-top: 10px
`

const ButtonContainer = styled.View`
  display: flex;
  align-items: center;
  justify-content: space-between; 
  flex-direction: row;
  flex-wrap: wrap;
`

const UploadButton = styled.View`
  width: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 70px;
`
