import React, {useEffect, useState} from 'react'
import styled from "styled-components/native"
import {PageTitle} from "../data-display/page-title"
import {TopNav} from "../navigation/top-nav"
import {SafeAreaView, Text, View} from "react-native"
import {DefaultCheckBox} from "../data-capture/default-check-box"
import {DefaultButton} from "../data-capture/default-button"
import {Config} from "../../config"

interface NewApplicationTermsProps {
  cancelTapped: () => void,
  agreeTapped: (agreedTerms: boolean, gaveCreditConsent: boolean, isNotUnderDebtReview: boolean) => void
}

const InitialApplicationTermsValue = Config.mode === "develop"

export const NewApplicationTerms: React.FC<NewApplicationTermsProps> = (props) => {
  const [agreeTerms, setAgreeTerms] = useState(InitialApplicationTermsValue)
  const [creditConsent, setCreditConsent] = useState(InitialApplicationTermsValue)
  const [noDebtReview, setNoDebtReview] = useState(InitialApplicationTermsValue)
  const [enabled, setEnabled] = useState(false)

  useEffect(() => {
    setEnabled(agreeTerms && creditConsent)
  }, [agreeTerms, creditConsent, setEnabled])

  const beforeYouApplyPoints = [
    'You are over the age of 18',
    'You have a valid ID or Passport',
    'You are currently employed or earning an income',
    'You are contactable by a cellphone number',
    'You have a postal or residential address',
    'You have obtained permission from your spouse to conclude this agreement, if married in community of property or by way of customary law.',
  ].map(p => <Bullet key={p} text={p}/>)

  const accountFeesPoints = [
    'Initiation fee: R30.00 once off',
    'Monthly service fee up to R15.00*pm',
    'Monthly service fee may increase without notice',
    'The credit facility is only available to applicants living and working within the borders of the Republic of South Africa and is currently not available in neighbouring Countries (Botswana, Lesutho, Mozambique, Swaziland and Zimbabwe).',
  ].map(p => <Bullet key={p} text={p}/>)

  const agreeTapped = () => {
    props.agreeTapped(agreeTerms, creditConsent, noDebtReview)
  }

  return (
    <SafeAreaView style={{marginLeft: 52, marginRight: 52}}>
      <TopNav />
      <Container>
        <PageTitle title={"Application Requirements"} titleType={"large"} textAlign={"left"}/>

        <Heading>Before you apply for an account, you must make sure:</Heading>
        {beforeYouApplyPoints}

        <Heading>Account fees</Heading>
        {accountFeesPoints}

        <View style={{marginTop: 30, marginRight: 52}}>
          <DefaultCheckBox
            text={"I acknowledge that I have read, understood and accepted the Terms and Conditions"}
            isChecked={agreeTerms}
            setChecked={setAgreeTerms}/>
          <DefaultCheckBox
            text={"I give consent that Mr Price Money may contact any credit bureau and South African Fraud Prevention Services to check your credit history and payment profile."}
            isChecked={creditConsent}
            setChecked={setCreditConsent}/>
          <DefaultCheckBox
            text={"I confirm that I am not currently insolvent, under sequestration or administration order, or under or applying for debt review."}
            isChecked={noDebtReview}
            setChecked={setNoDebtReview}/>

          <AnyQueries>Any queries? Call 0800 222 639</AnyQueries>

          <ButtonsContainer>
            <DefaultButton buttonType={"light"} title={"Cancel"} disabled={false} onPress={props.cancelTapped} />
            <View style={{marginLeft: 16 }}>
              <DefaultButton buttonType={"default"} title={"Agree"} disabled={!enabled} onPress={agreeTapped} />
            </View>
          </ButtonsContainer>
        </View>
      </Container>
    </SafeAreaView>
  )
}

const Bullet: React.FC<{ text: string }> = ({text}) => {
  return (
    <View style={{display: "flex", flexDirection: "row", marginLeft: 10, marginTop: 6}}>
      <View style={{width: 15}}>
        <Text>•</Text>
      </View>
      <View>
        <BulletText>{text}</BulletText>
      </View>
    </View>
  )
}

const Container = styled.SafeAreaView`
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  margin: 36px 0px;  
`

const Heading = styled.Text`
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  display: flex;
  align-items: center;
  letter-spacing: 0.444444px;
  color: #353C43;
  margin-top: 36px;  
`

const BulletText = styled.Text`
  font-size: 14px;
  line-height: 20px;
  letter-spacing: 0.25px;
  color: #353C43;
`

const AnyQueries = styled.Text`
  font-weight: 500;
  font-size: 14px;
  color: #353C43;
  margin-top: 24px
`

const ButtonsContainer = styled.View`
  display: flex;
  margin-top: 24px;
  margin-right: -38px;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
`
