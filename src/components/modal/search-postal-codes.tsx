import React, {useState, useEffect} from 'react'
import styled from "styled-components/native"
import {DefaultTextInput} from "../data-capture/default-text-input"
import {useMutation} from 'react-query'
import {ActivityIndicator, Text} from "react-native"
import {database} from '../../services/db-service'
import { Q } from '@nozbe/watermelondb'
import {PostalCode} from "../../models/db/postal-code"

const postalCodeCollection = database.collections.get('postal_codes')

interface SearchPostalCodesModalProps {
  onSelectedPostalCode: (postalCode: PostalCode) => void
}

const searchPostalCodesQuery = async (params: { query: string}) => {
  const query = await postalCodeCollection.query(
    Q.or(
      Q.where('code', parseInt(params.query)),
      Q.where('town', Q.like(`%${Q.sanitizeLikeString(params.query)}%`)),
      Q.where('suburb', Q.like(`%${Q.sanitizeLikeString(params.query)}%`)),
    )
  ).fetch()
  // @ts-ignore
  return query as PostalCode[]
}

export const SearchPostalCodesModal: React.FC<SearchPostalCodesModalProps> = (props) => {
  const [searchPostalCodes, {data, isLoading}] = useMutation(searchPostalCodesQuery)
  const [searchText, setSearchText] = useState('')

  useEffect(() => {
    if (searchText.length < 4) { return }
    searchPostalCodes({query: searchText})
  }, [searchText])

  const blocks: React.ReactNode = data?.sort((a, b) => (a.suburb < b.suburb ? -1 : 1)).map(pc => (
      <PostalCodeBlock onPress={() => props.onSelectedPostalCode(pc)} key={pc.id}>
        <Text numberOfLines={2} ellipsizeMode={"tail"} style={{ fontWeight: 'bold'}}>{pc.suburb}</Text>
        <Text numberOfLines={1} ellipsizeMode={"tail"}>{pc.town}</Text>
        <Text numberOfLines={1} ellipsizeMode={"tail"}>{pc.code}</Text>
      </PostalCodeBlock>
    ))

  return (
    <Container>
      <DefaultTextInput type={"text"} placeholder={"Postal Code"} value={searchText} setValue={setSearchText}/>
      <ScrollView contentContainerStyle={{ display: "flex", alignItems: "center", justifyContent: "space-between", flexDirection: "row", flexWrap: "wrap" }}>
        {blocks}
        {isLoading &&
          <LoadingContainer>
              <ActivityIndicator animating={true} color={'gray'} />
          </LoadingContainer>
        }
      </ScrollView>
    </Container>
  )
}

const Container = styled.View`
  height: 100%;
  display: flex;
  padding: 14px;
`

const LoadingContainer = styled.View`
  display: flex;
  align-items: center;  
  justify-content: center;
`

const ScrollView = styled.ScrollView`  
  margin-top: 10px;  
`

const PostalCodeBlock = styled.TouchableOpacity`
  display: flex;
  padding: 15px;
  align-items: flex-start; 
  justify-content: center;
  width: 175px;
  height: 100px;
  background: #F9F9FB;
  border-width: 1px;
  border-color: #ececee;
  border-radius: 4px;
  margin-top: 14px;
  margin-bottom: 14px;
`
