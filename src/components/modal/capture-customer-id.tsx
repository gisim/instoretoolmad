import React, {useCallback, useEffect, useState} from 'react'
import styled from "styled-components/native"
import {DefaultDropDown} from "../data-capture/default-drop-down"
import {Option} from '../../models/form'
import {DefaultTextInput} from "../data-capture/default-text-input"
import {AppStorageService} from "../../services/app-storage-service"
import {useMutation} from "react-query"
import {validateIdentityNumberQuery} from "../../services/api-service"
import {ActivityIndicator, View} from "react-native"
import {Config} from "../../config"
import {ReadOnlyField} from "../data-capture/read-only-field"
import {ListSelector} from "../data-capture/list-selector"
import {DefaultModal} from "./default-modal"

interface CaptureCustomerIdProps {
  enableActionButtonHandler: (canEnable: boolean) => void
  setCustomerIdNumber: (value: string | null, idType: string) => void
  isLoading: boolean
}

const InitialIdNumber = Config.mode === "develop" ? Config.TemporaryInitialState.IdentityNumber.value : ''

export const CaptureCustomerId: React.FC<CaptureCustomerIdProps> = (props) => {
  const [validateIdentityNumber, {isLoading}] = useMutation(validateIdentityNumberQuery)
  const [idType, setIdType] = useState('8')
  const [idVal, setIdVal] = useState(InitialIdNumber)
  const [error, setError] = useState<string | null>(null)
  const [showModal, setShowModal] = useState(false)

  const options = AppStorageService.lookUp?.IdentityTypes
    .sort((a, b) => (b.Id - a.Id))
    .filter(o => [3, 6, 7, 8].includes(o.Id)) ?? []

  const {enableActionButtonHandler, setCustomerIdNumber} = props

  const setIdValue = useCallback((value: string) => {
    setIdVal(value)

    if (idType !== '8') {
      return
    }
    if (value.length < 13) {
      console.log('id num less than 13')
      return
    }

    validateIdNumber(value)
  }, [idType])

  const validateIdNumber = useCallback((idValue: string) => {
    validateIdentityNumber({idNumber: idValue})
      .then(response => {
        if (!response?.IsIdentityNumberValid ?? false) {
          setError('Invalid ID number.')
        } else {
          setError(null)
        }
      })
  }, [idVal])

  useEffect(() => {
    let isIdNumberValid: boolean
    if (idType === '8') {
      isIdNumberValid = idVal.length === 13
    } else {
      isIdNumberValid = idVal.length > 0
    }

    enableActionButtonHandler(!isLoading && error === null && isIdNumberValid)
  }, [idVal, idType, error, enableActionButtonHandler, isLoading])

  useEffect(() => {
    if (idType !== '8') {
      setError(null)
    } else {
      if (idVal.length > 0) {
        validateIdNumber(idVal)
      }
    }
  }, [idType])

  useEffect(() => {
    if (!error && idVal.length > 0) {
      setCustomerIdNumber(idVal, idType)
    } else {
      setCustomerIdNumber(null, idType)
    }
  }, [idVal])

  return (
    <>
      {props.isLoading &&
      <LoadingContainer>
          <ActivityIndicator size={"large"} color={'gray'} />
      </LoadingContainer>
      }
      {!props.isLoading &&
      <Container>
          <DescriptionText>
              Please provide your ID or Passport number. If you have already started the application process please
              enter the ID or Passport number you used to create the application.
          </DescriptionText>
          <View>
            <ReadOnlyField
                title={"Identity Type"}
                value={options.find(o => `${o.Id}` === idType)?.Description ?? ''}
                onPressed={() => setShowModal(true)}
                errorText={null}
            />
          </View>
          <DefaultTextInput
              type={"text"}
              placeholder={"Identity Number"}
              value={idVal}
              setValue={setIdValue}
              errorText={error}
              style={{ marginBottom: 10 }}
          />
        {isLoading &&
        <Loading>
            <ActivityIndicator animating={isLoading} color={'gray'}/>
            <LoadingText>Validating ID Number…</LoadingText>
        </Loading>
        }
      </Container>
      }

      <DefaultModal
        visible={showModal}
        title={"Select a value"}
        headerType={"small"}
        showBottomButtons={false}
        width={'576px'}
        height={'540px'}
      >
        <ListSelector
          values={options}
          onValueSelected={(value) => {
            setIdType(`${value.Id}`)
            setShowModal(false)
          }}
        />
      </DefaultModal>
    </>
  )
}

const Container = styled.View`
  padding: 15px;
`

const LoadingContainer = styled.View`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%
`


const DescriptionText = styled.Text`
  font-size: 16px;
  line-height: 24px;
  letter-spacing: 0.444444px;
  color: #353C43;
  margin-bottom: 42px;
`

const Loading = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  margin-bottom: 50px;
`

const LoadingText = styled.Text`
  color: grey;
  margin-left: 10px;
`
