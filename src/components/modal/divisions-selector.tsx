import React from 'react'
import {useQuery} from "react-query"
import {getActiveDivisionsQuery} from "../../services/api-service"
import {ActivityIndicator} from "react-native"
import styled from "styled-components/native"
import {ActiveDivision} from "../../models/active-divison"

interface DivisionsSelectorProps {
  divisionSelected: (screen: string) => void
}

export const DivisionsSelector: React.FC<DivisionsSelectorProps> = (props) => {
  const { isLoading, data } = useQuery('active-divisions', getActiveDivisionsQuery)

  if (isLoading) {
    return (
      <LoadingContainer>
        <ActivityIndicator animating={isLoading} color={'gray'} />
      </LoadingContainer>
    )
  }

  const divisionBlocks = data?.map(division => (
    <DivisionBlock
      key={division.DivisionId}
      division={division}
      divisionSelected={props.divisionSelected}
    />
  ))

  return (
    <Container>
      {divisionBlocks}
    </Container>
  )
}
const DivisionBlock: React.FC<{ division: ActiveDivision } & DivisionsSelectorProps> = ({ division, divisionSelected }) => {
  const uri = `data:image/jpeg;base64,${division.DivisionLogoBig}`
  return (
    <DivisionBlockContainer onPress={() => divisionSelected(division.DivisionCode)}>
      <Logo source={{ uri }} />
      {/*<DivisionBlockName>{division.DivisionDesc}</DivisionBlockName>*/}
    </DivisionBlockContainer>
  )
}

const Container = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  padding: 14px;
`

const LoadingContainer = styled.View`
  display: flex;
  align-items: center;  
  justify-content: center;
`

const DivisionBlockContainer = styled.TouchableOpacity`
  display: flex;
  align-items: center; 
  justify-content: center;
  width: 148px;
  height: 148px;
  background: #F9F9FB;
  border-width: 1px;
  border-color: #ececee;
  border-radius: 4px;
  margin-top: 14px;
  margin-bottom: 14px;
`

const Logo = styled.Image`
  width: 90%;
  height: 90%;
  resize-mode: contain;
`

// const DivisionBlockName = styled.Text`
//   font-weight: 500;
//   font-size: 16px;
//   line-height: 42px;
//   text-align: center;
//   color: #010101;
// `
