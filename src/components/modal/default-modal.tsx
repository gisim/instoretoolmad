import React from 'react'
import styled from 'styled-components/native'
import {GestureResponderEvent, View} from 'react-native'

import {DefaultButton} from '../data-capture/default-button'

interface DefaultModalProps {
  visible: boolean
  title: string
  headerType: "small" | "large"
  width?: string
  height?: string
  showBottomButtons: boolean

  cancelButtonTitle?: string
  onCancelButtonPressed?: () => void

  actionButtonTitle?: string
  onActionButtonPressed?: () => void
  isActionButtonEnabled?: boolean

  onBackgroundTapped?: (event: GestureResponderEvent) => boolean
}

interface DefaultModalTitleBarProps {
  title: string
}

interface DefaultModalBottomButtonsProps {
  cancelButtonTitle?: string
  onCancelButtonPressed?: () => void;

  isActionButtonEnabled: boolean
  actionButtonTitle?: string
  onActionButtonPressed?: () => void
}

export const DefaultModal: React.FC<DefaultModalProps> = (props) => {
  let header: React.ReactNode|null = null
  switch (props.headerType) {
    case "small": {
      header = <SmallHeaderBar title={props.title}/>
      break
    }
    case "large": {
      header = <LargeHeaderBar title={props.title}/>
      break
    }
  }

  const width = props.width ?? '100%'
  const height = props.height ?? '100%'

  return (
    <Container
      visible={props.visible}
      animationType={'fade'}
      transparent={true}
    >
      <Content onStartShouldSetResponder={props.onBackgroundTapped}>
        <CentreModal width={width} height={height}>
          {header}
          <ModalItems>{props.children}</ModalItems>
          {props.showBottomButtons &&
          <BottomButtons
              isActionButtonEnabled={props.isActionButtonEnabled ?? true}
              cancelButtonTitle={props.cancelButtonTitle}
              actionButtonTitle={props.actionButtonTitle}
              onCancelButtonPressed={props.onCancelButtonPressed}
              onActionButtonPressed={props.onActionButtonPressed}
          />
          }
        </CentreModal>
      </Content>
    </Container>
  )
}

const SmallHeaderBar: React.FC<DefaultModalTitleBarProps> = (props) => {
  return (
    <SmallTitleBar>
      <SmallTitleText>{props.title}</SmallTitleText>
      <SmallHeaderBorder/>
    </SmallTitleBar>
  )
}

const LargeHeaderBar: React.FC<DefaultModalTitleBarProps> = (props) => {
  return (
    <LargeTitleBar>
      <LargeTitleText>{props.title}</LargeTitleText>
    </LargeTitleBar>
  )
}

export const BottomButtons: React.FC<DefaultModalBottomButtonsProps> = (props) => {
  return (
    <ModalBottomItems>

      {props.cancelButtonTitle && props.onCancelButtonPressed &&
        <DefaultButton
            buttonType={'light'}
            disabled={false}
            title={props.cancelButtonTitle}
            onPress={props.onCancelButtonPressed}
        />
      }

      {props.actionButtonTitle && props.onActionButtonPressed &&
        <>
            <View style={{width: 16}}/>
            <DefaultButton
                buttonType={'default'}
                disabled={!props.isActionButtonEnabled}
                title={props.actionButtonTitle}
                onPress={props.onActionButtonPressed}
            />
        </>
      }
    </ModalBottomItems>
  )
}

const Container = styled.Modal`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`

const Content = styled.View`
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  padding: 128px 24px;
  display: flex;
  align-items: center;
  justify-content: center;thanks
`

const CentreModal = styled.View<{ width: string, height: string }>`  
  width: ${props => props.width};
  height: ${props => props.height};
  background-color: #ffffff;
  border-radius: 8px;
`

const SmallTitleBar = styled.View`
  height: 57px;
  display: flex;
  align-items: center;
  justify-content: center;
`

const SmallTitleText = styled.Text`
  font-weight: 500;
  font-size: 14px;
  letter-spacing: 0.25px;
  color: #353c43;
`

const SmallHeaderBorder = styled.View`
  height: 1px;
  width: 100%;
  background-color: #ececee;
  position: absolute;
  bottom: 0;
`

const LargeTitleBar = styled.View`
  height: 64px;
  display: flex;
  align-items: flex-start;
  justify-content: flex-end;
  padding: 0 38px;
`

const LargeTitleText = styled.Text`
  font-weight: 600;
  font-size: 20px;
  line-height: 28px;
  letter-spacing: 0.5px;
  color: #353C43;
`

const ModalItems = styled.View`
  padding: 0 24px;
  flex: 1;
`

const ModalBottomItems = styled.View`
  flex-direction: row;
  justify-content: flex-end;
  padding: 28px 24px;
`
