import React, {useRef, useState} from 'react'
import {Image, View} from 'react-native'
import {SketchCanvas} from "@terrylinla/react-native-sketch-canvas"
import { BottomButtons } from './default-modal'
import styled from "styled-components/native"

interface CaptureSignatureProps {
  onCancelButtonPressed: () => void
  onActionButtonPressed: (base64: string) => void
}

export const CaptureSignature: React.FC<CaptureSignatureProps> = (props) => {
  const [showSignHere, setShowSignHere] = useState(true)
  const canvas = useRef<SketchCanvas | null>(null)

  const onSavePressed = () => {
    if (canvas.current?.getPaths().length === 0) {
      props.onCancelButtonPressed()
      return
    }

    canvas.current?.getBase64('png', false, false, false, true, (error, image) => {
      if (error) { console.error(error); return }
      if (!image) { return }
      props.onActionButtonPressed(image)
    })
  }

  return (
    <Container>
      <ImageContainer>
        <Image source={require("../../assets/icons/sign-here.png")} width={36} height={101} />
      </ImageContainer>
      {showSignHere &&
        <SignHere>Sign Here</SignHere>
      }
      <View style={{flex: 1, flexDirection: 'row'}}>
        <SketchCanvas
          ref={canvas}
          style={{flex: 1}}
          strokeColor={'black'}
          strokeWidth={7}
          onStrokeStart={() => setShowSignHere(false)}
        />
      </View>
      <Line />
      <BottomButtons
        cancelButtonTitle={"Cancel"}
        actionButtonTitle={"Save"}
        onCancelButtonPressed={props.onCancelButtonPressed}
        onActionButtonPressed={onSavePressed}
        isActionButtonEnabled={true}
      />
    </Container>
  )
}

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`

const SignHere = styled.Text`
  position: absolute;
  bottom: 120px;
  font-size: 27px;
  color: #d1d2d4;
`

const Line = styled.View`
  background: #D1D2D4;
  height: 1px;
  width: 100%;
`

const ImageContainer = styled.View`
  position: absolute;
  left: 54px;
  bottom: 120px;
`
