import React, {useEffect, useLayoutEffect, useState} from 'react'
import {Log} from "../../models/db/log"
import {database} from "../../services/db-service"
import styled from 'styled-components/native'
import {StackNavigationProp} from "@react-navigation/stack"
import {RootStackParamLList} from "../../../App"
import {FlatList, ScrollView, Text, TouchableOpacity, Platform} from "react-native"
import { URL } from 'react-native-url-polyfill'
import {DefaultModal} from "../modal/default-modal"
import {Q} from "@nozbe/watermelondb"

type LogViewerScreenNavigationProps = StackNavigationProp<RootStackParamLList, 'LogViewer'>

interface LogViewerProps {
  navigation: LogViewerScreenNavigationProps
}


export const LogViewer: React.FC<LogViewerProps> = (props) => {
  const [logs, setLogs] = useState<Log[]|null>(null)
  const [showOutput, setShowOutput] = useState<string|null>(null)
  const {navigation} = props

  useEffect(() => {
    if (logs !== null) { return }
    getLogs()
  }, [logs])

  useEffect(() => {
    if (logs !== null) { return }
    const deleteRecords = async () => {
      const logsCollection = database.collections.get('logs')
      const yesterday = new Date((new Date()).getTime() - (24 * 60 * 60 * 1000))
      const allLogs = await logsCollection.query(
        Q.where('created_at', Q.lt(yesterday.getTime()))
      ).fetch()

      await database.action(async () => {
        // @ts-ignore
        allLogs.forEach((log: Log) => {
          log.destroyPermanently()
        })
      })
    }
    deleteRecords()
  }, [logs])

  const getLogs = async () => {
    const logsCollection = database.collections.get('logs')
    const allLogs = await logsCollection.query(
      Q.experimentalSortBy("created_at", Q.desc),
      Q.experimentalTake(100)
    ).fetch()
    setLogs(allLogs as Log[])
  }

  useLayoutEffect(() => {
    navigation.setOptions({
      headerBackTitle: "Close",
    })
  }, [navigation])

  return (
    <>
      <Container>
        <FlatList
          data={logs ?? []}
          renderItem={({ item }) => (
            <TouchableOpacity onPress={() => setShowOutput(item.id)}>
              <LogRow log={item} />
            </TouchableOpacity>
          )}
        />
      </Container>

      <DefaultModal
        visible={showOutput !== null}
        title={"Response"}
        headerType={"small"}
        showBottomButtons={true}
        cancelButtonTitle={"Close"}
        onCancelButtonPressed={() => setShowOutput(null)}
      >
        <ScrollView>
          <Code>
            {showOutput && (logs ?? []).filter(log => log.id === showOutput)[0].data}
          </Code>
        </ScrollView>
      </DefaultModal>
    </>
  )
}

const LogRow: React.FC<{ log: Log }> = (props) => {
  const {log} = props
  let logName: React.ReactNode

  if (log.name.startsWith("http")) {
    const url = new URL(log.name)

    const paths = log.name.split("/")
    const service = `${url.protocol}//${url.hostname}/${paths[3]}`
    const path = log.name.replace(service, "")

    logName = (
      <>
        <Snippet numberOfLines={12} ellipsizeMode={"tail"}>{service}</Snippet>
        <Text style={{ fontWeight: 'bold', marginTop: 10 }}>{path}</Text>
      </>
    )
  } else {
    logName = <Text style={{ fontWeight: 'bold' }}>{log.name}</Text>
  }

  return (
    <RowContainer>
      <DateTime>{log.createdAt.toLocaleString()}</DateTime>
      <LogType>{log.logType}</LogType>
      <SnipperContainer>
        {logName}
      </SnipperContainer>
    </RowContainer>
  )
}

const Container = styled.View`
`

const RowContainer = styled.View`
  display: flex;
  align-items: center;
  flex-direction: row;
  height: 70px;
  border-bottom-width: 1px;
  border-bottom-color: #eceece;
  padding: 0 10px;
`

const DateTime = styled.Text`
  width: 22%
`

const LogType = styled.Text`
  width: 10%
`

const SnipperContainer = styled.View`
`

const Snippet = styled.Text`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const Code = styled.Text`
  font-family: ${Platform.OS === 'ios' ? 'Courier' : 'monospace'}
`
