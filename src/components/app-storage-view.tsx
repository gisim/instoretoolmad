import React, {useEffect, useState} from 'react'
import {AppStorageService} from "../services/app-storage-service"
import {ActivityIndicator, Alert, Text, View} from 'react-native'
import styled from 'styled-components/native'

interface AppStorageViewProps {}

export const AppStorageView: React.FC<AppStorageViewProps> = (props) => {
  const [hasLookUpData, setHasLookUpdata] = useState(false)
  const [refreshCounter, setRefreshCounter] = useState(0)

  useEffect(() => {
    if (hasLookUpData) { return }

    AppStorageService.start()
      .then(() => setHasLookUpdata(true))
      .catch((error) => {
        Alert.alert(
          'App Start-up Error',
          `The app was prevented from starting up correctly. Tap refresh to try again. If this error persists, please contact support.\n\n${error}`,
          [{ text: 'Refresh', onPress: () => setRefreshCounter(counter => ++counter) }]
        )
        console.log('App Storage Service Error: ', error)
      })

    AppStorageService.getLocalLookUpData()
      .then(() => setHasLookUpdata(true))
      .catch(error => console.log('AppStorageView error:', error))

  }, [hasLookUpData, setHasLookUpdata, refreshCounter])

  if (!hasLookUpData) {
    return (
      <Loading/>
    )
  }

  return (
    <View>
      {props.children}
    </View>
  )
}

const Loading: React.FC = () => {
  return (
      <LoadingContainer>
        <ActivityIndicator size={"large"} color={'gray'} />
        <Text style={{ marginTop: 30, textAlign: "center" }}>
          Updating data…
        </Text>
        <Text style={{ marginTop: 30, textAlign: "center", fontWeight: '500' }}>
          Do NOT close the app until complete!
        </Text>
    </LoadingContainer>
  )
}


const LoadingContainer = styled.View`
  display: flex;
  align-items: center;  
  justify-content: center;
  height: 100%;
`
