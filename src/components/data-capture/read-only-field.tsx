import React from 'react'
import styled from 'styled-components/native'
import {Label} from "./default-drop-down"

interface ReadOnlyFieldProps {
  title: string
  value: string
  onPressed: () => void
  errorText?: string | null
}

export const ReadOnlyField: React.FC<ReadOnlyFieldProps> = (props) => {
  const value = props.value.length ? props.value : ""
  const hasError = (props.errorText !== undefined && props.errorText !== null)

  return (
    <>
      <Container onPress={props.onPressed} style={hasError ? {borderColor: '#f44336'} : {borderColor: '#ececee'}}>
        {value.length > 0 && <ValueField>{value}</ValueField>}
        {value.length === 0 && <ValueField>{props.title}</ValueField>}
      </Container>
      {props.value.length > 0 &&
        <Label>{props.title}</Label>
      }
      {hasError && (
        <ErrorLabel>
          {props.errorText}
        </ErrorLabel>
      )}
    </>
  )
}

const Container = styled.TouchableOpacity`
  border-width: 1px;
  width: 100%;
  height: 56px;
  margin-top: 7px;
  margin-bottom: 30px;
  border-radius: 4px;
  display: flex;
  justify-content: center;
  padding: 0px 15px;
`

const ValueField = styled.Text`
  font-size: 16px;
  letter-spacing: 0.444444px;
  color: #9a9da0;
`

const ErrorLabel = styled.Text`  
  text-align: right;
  font-size: 12px;
  letter-spacing: 0.4px;
  color: #F44336;
  bottom: 20px;
`
