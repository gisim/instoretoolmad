import React from 'react'
import {Text} from 'react-native'

import DropDownPicker from 'react-native-dropdown-picker'
import {Option} from "../../models/form"
import {AppStorageService} from "../../services/app-storage-service"
import {LookUpData} from '../../models/lookup-data'
import styled from "styled-components/native"

interface DefaultDropDownProps {
  title: string
  options: Option[] | undefined
  optionsLookUp?: string | undefined
  onItemSelected: (value: string) => void
  value: string | null
}

export const DefaultDropDown: React.FC<DefaultDropDownProps> = (props) => {
  const {lookUp} = AppStorageService
  let options: Option[] | undefined = props.options

  if (props.optionsLookUp) {
    if (props.optionsLookUp in lookUp) {
      // @ts-ignore (needed to force `lookUp` to resolve, which it will since the above line makes sure of that
      options = (lookUp[props.optionsLookUp] as LookUpData[]).map((option: LookUpData) => ({
        title: option.Description,
        value: `${option.Id}`,
      }))
    }
  }

  if (!options) {
    return null
  }

  const items = options.map(option => {
    return {
      label: option.title,
      value: option.value,
      icon: () => {
        return <Text/>
      },
    }
  })

  let defaultValue = null
  if (props.value && props.value.length > 0 && items.map(i => i.value).includes(props.value)) {
    defaultValue = props.value
  }

  return (
    <DropDownWrap>
      <DropDownPicker
        defaultValue={defaultValue}
        placeholder={props.title}
        items={items}
        style={{backgroundColor: '#fff'}}
        containerStyle={{height: 56, marginTop: 7}}
        itemStyle={{justifyContent: 'flex-start'}}
        labelStyle={{color: '#9a9da0', fontSize: 16}}
        dropDownStyle={{backgroundColor: '#fafafa'}}
        onChangeItem={(item) => props.onItemSelected(item.value)}
      />
      {defaultValue &&
        <Label>{props.title}</Label>
      }
    </DropDownWrap>
  )
}

export const Label = styled.Text`
  position: absolute;
  z-index: 99999;
  left: 10px;
  background: white;
  padding: 0px 8px 0 5px;
  font-size: 13px;
  color: #9a9da0;
`

const DropDownWrap = styled.View`
  margin-bottom: 30px;
`
