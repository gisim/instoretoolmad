import React, {useState} from 'react'
import styled from 'styled-components/native'

interface DefaultCheckBoxProps {
  text: string;
  isChecked: boolean;
  setChecked: (isChecked: boolean) => void
}

export const DefaultCheckBox: React.FC<DefaultCheckBoxProps> = (props) => {
  const { isChecked, setChecked } = props

  const toggleChecked = () => {
    setChecked(!isChecked)
  }

  return (
    <CheckBoxContent>
      <CheckBox>
        <CheckBoxTouchArea onPress={toggleChecked}>
          {isChecked && (
            <CheckedIcon source={require('../../assets/icons/tickDark.png')}/>
          )}
        </CheckBoxTouchArea>
      </CheckBox>
      <CheckBoxText>
        {props.text}
      </CheckBoxText>
    </CheckBoxContent>
  )
}

const CheckBoxContent = styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: 10px;    
  margin-bottom: 10px;
`
// margin-left: -10px;

const CheckBox = styled.View`
  width: 24px;
  height: 24px;
  border: 1px solid #D1D2D4;
  border-radius: 4px;
  margin-right: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
`

const CheckBoxTouchArea = styled.TouchableOpacity`
  width: 60px;
  height: 60px;
  display: flex;
  align-items: center;
  justify-content: center;
`

const CheckBoxText = styled.Text`
  font-size: 14px;
  letter-spacing: 0.25px;
  color: #353C43;
`

const CheckedIcon = styled.Image`
  width: 24px;
  height: 24px;
`
