import React, {useEffect, useState} from 'react'
import styled from "styled-components/native"
import {DefaultTextInput} from "./default-text-input"
import {LookUpData} from "../../models/lookup-data"
import {FlatList, Text} from "react-native"
import {AppStorageService} from "../../services/app-storage-service"

interface ListSelectorProps {
  lookUp?: string
  values?: LookUpData[]
  onValueSelected: (value: LookUpData) => void
}

export const ListSelector: React.FC<ListSelectorProps> = (props) => {
  const [searchText, setSearchText] = useState('')
  const [originalItems, setOriginalItems] = useState<LookUpData[]>([])
  const [filteredItems, setFilteredItems] = useState<LookUpData[]>([])
  const { lookUp, values } = props

  useEffect(() => {
    if (values) { return }
    if (filteredItems && filteredItems.length > 0) { return }
    if (!lookUp) { return }
    // @ts-ignore
    const ogItems = AppStorageService.lookUp[lookUp] as LookUpData[]
    setOriginalItems(ogItems)
    setFilteredItems(ogItems)
  }, [filteredItems, setFilteredItems, lookUp, values])

  useEffect(() => {
    if (searchText.length === 0) {
      setFilteredItems(originalItems)
      return
    }
    const newItems = originalItems.filter(item => item.Description.toLowerCase().includes(searchText.toLowerCase()))
    setFilteredItems(newItems)
  }, [searchText, setFilteredItems])

  useEffect(() => {
    if (lookUp) { return }
    if (filteredItems.length > 0) { return }
    if (!values) { return }
    setOriginalItems(values)
    setFilteredItems(values)
  }, [values, filteredItems])


  return (
    <Container>
      <DefaultTextInput type={"text"} placeholder={"Filter"} value={searchText} setValue={setSearchText}/>
      <FlatList
        data={filteredItems}
        keyExtractor={(item: LookUpData) => `${item.Id}`}
        renderItem={({ item }) => {
          return (
            <Row onPress={() => props.onValueSelected(item)}>
              <Text numberOfLines={1} ellipsizeMode={"tail"} style={{ fontWeight: 'normal', fontSize: 16, color: '#7b7c80' }}>
                {item.Description}
              </Text>
            </Row>
          )
        }}
        alwaysBounceVertical={true}
      />
    </Container>
  )
}

const Container = styled.View`
  height: 100%;
  display: flex;
  padding: 14px;
`

const Row = styled.TouchableOpacity`
  width: 100%;
  height: 44px;
  border-bottom-width: 1px;
  border-bottom-color: #ECECEE;
  display: flex;
  align-items: flex-start;
  justify-content: center;
`
