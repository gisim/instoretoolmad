import React from 'react'
import styled from 'styled-components/native'
import Markdown from "react-native-markdown-renderer"

interface DefaultRadioItemProps {
    style: string
    checked: boolean
    label: string
    rightLabel?: boolean
    rightLabelText?: string
    onChange: (checked: boolean) => void
}

interface RadioItemVertical {
    checked: boolean
    label: string
    rightLabel?: boolean
    rightLabelText?: string
    onChange: (checked: boolean) => void
}

interface RadioItemHorizontal {
    checked: boolean;
    label: string;
    onChange: (checked: boolean) => void
}

export const DefaultRadioItem: React.FC<DefaultRadioItemProps> = (props) => {
    return (
        <>
            {props.style === 'vertical' && (
                <VerticalRadioItem>
                    <RadioItemVertical
                        onChange={props.onChange}
                        checked={props.checked}
                        label={props.label}
                        rightLabel={props.rightLabel}
                        rightLabelText={props.rightLabelText}
                    />
                </VerticalRadioItem>
            )}
            {props.style === 'horizontal' && (
                <HorizontalRadioItem>
                    <RadioItemHorizontal
                        onChange={props.onChange}
                        checked={props.checked}
                        label={props.label}
                    />
                </HorizontalRadioItem>
            )}
        </>
    )
}

const RadioItemVertical: React.FC<RadioItemVertical> = (props) => {
    // const [isChecked, setIsChecked] = useState(props.checked)
    const toggleChecked = () => {
        props.onChange(!props.checked)
        // setIsChecked(!isChecked)
    }
    return (
        <VerticalRadioItemContent  onPress={toggleChecked} activeOpacity={0.7}>
            <RadioButton>
                {props.checked && (
                    <RadioButtonIcon />
                )}
            </RadioButton>
            <RadioLabel>
                {props.label}
            </RadioLabel>
            {props.rightLabel && (
                <RadioRightLabel>
                    {props.rightLabelText}
                </RadioRightLabel>
            )}
        </VerticalRadioItemContent>
    )
}

const RadioItemHorizontal: React.FC<RadioItemHorizontal> = (props) => {
    // const [isChecked, setIsChecked] = useState(props.checked)
    const toggleChecked = () => {
        props.onChange(!props.checked)
        // setIsChecked(!isChecked)
    }
    return (
        <HorizontalRadioItemContent onPress={toggleChecked} activeOpacity={0.7}>
            <RadioButton>
                {props.checked && (
                    <RadioButtonIcon />
                )}
            </RadioButton>
            <RadioLabel>
                {props.label}
            </RadioLabel>
        </HorizontalRadioItemContent>
    )
}

const VerticalRadioItem = styled.View`
  width: 100%;
`

const HorizontalRadioItem = styled.View``

const VerticalRadioItemContent = styled.TouchableOpacity`
  width: 100%;
  flex-direction: row;
  align-items: center;
  margin-top: 12px;
`

const HorizontalRadioItemContent = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  margin-right: 60px;
`

const RadioButton = styled.View`
  width: 24px;
  height: 24px;
  border: 1px solid #D1D2D4;
  border-radius: 12px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 8px;
`

const RadioButtonIcon = styled.View`
  width: 12px;
  height: 12px;
  border-radius: 6px;
  background-color: #D71920;
`

const RadioLabel = styled(Markdown)`
  font-size: 14px;
  letter-spacing: 0.25px;
  color: #353C43;
`

const RadioRightLabel = styled.Text`
  position: absolute;
  right: 0;
`
