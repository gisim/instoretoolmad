import React, {useEffect, useState} from "react"
import styled from "styled-components/native"
import {NumberSelector} from "./number-selector"

interface TimePeriodSelectorProps {
  title: string
  onValueChanged: (years: number, months: number) => void
  errorText?: string | null
}

export const TimePeriodSelector: React.FC<TimePeriodSelectorProps> = (props) => {
  const [years, setYears] = useState(0)
  const [months, setMonths] = useState(0)
  const {onValueChanged} = props
  const hasError = (props.errorText !== null)

  useEffect(() => {
    if (years === 0 && months === 0) { return }
    onValueChanged(years, months)
  }, [years, months])

  const increaseMonth = (val: number) => {
    setMonths(m => {
      if (m === 11) {
        setYears(y => ++y)
        return 0
      }
      return val
    })
  }

  return (
    <>
      <Container>
        <NumberSelector
          showErrorBorder={hasError}
          onValueUpdated={setYears}
          value={years}
          valueTitle={'year'}
          valueTitlePlural={'years'}
        />

        <NumberSelector
          showErrorBorder={hasError}
          onValueUpdated={(val) => increaseMonth(val)}
          value={months}
          valueTitle={'month'}
          valueTitlePlural={'months'}
          resetAt={12}
        />

        <Label>{props.title}</Label>
      </Container>
      {hasError && (
        <ErrorLabel>
          {props.errorText}
        </ErrorLabel>
      )}
    </>
  )
}



const Container = styled.View`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
`

const Label = styled.Text`
  position: absolute;
  z-index: 99999;
  left: 10px;
  top: -8px;
  background: white;
  padding: 0px 8px 0 5px;
  font-size: 13px;
  color: #9a9da0;
`

const ErrorLabel = styled.Text`  
  text-align: right;
  font-size: 12px;
  letter-spacing: 0.4px;
  color: #F44336;
  bottom: -10px;
`
