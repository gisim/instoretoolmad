import React from 'react'
import styled from 'styled-components/native'
import {Image, View} from "react-native"
import {DefaultButton} from "./default-button"
import {FinancialServiceType} from "../../models/get-insurance-products-response"

interface InsuranceProductListProps {
  dataList: FinancialServiceType[]
}

interface InsuranceProductListItemProps {
  iconType: string
  title: string
  cost: string
  signature: string
}

export const InsuranceProductList: React.FC<InsuranceProductListProps> = (props) => {
  const itemsList = props.dataList.map(dataItem =>
    <InsuranceProductListItem
      key={dataItem.FinancialServiceTypeId}
      iconType={'star'}
      title={dataItem.FinancialServiceTypeDesc}
      cost={dataItem.FinancialServiceTypeRate}
      signature={''}
    />
  )
  const total = props.dataList.reduce((sum, product) => sum + parseFloat(product.FinancialServiceTypeRate), 0.0)

  return (
    <StyledInsuranceProductList>
      <TopBar>
        <TopText hasRightPadding={false}>
          Insurance Product
        </TopText>
        <RightItemsWrap>
          <TopText hasRightPadding={true} style={{textAlign: 'right'}}>
            Cost
          </TopText>
          <TopText hasRightPadding={false} style={{textAlign: 'center'}}>
            Initial
          </TopText>
        </RightItemsWrap>
      </TopBar>
      <InsuranceList>
        {itemsList}
      </InsuranceList>
      <TotalBar>
        <TotalTopBorder/>
        <View style={{
          height: '100%',
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          paddingRight: 44,
          paddingLeft: 13,
        }}>
          <TotalText>
            Total Cost
          </TotalText>
          <TotalText>
            R {total.toFixed(2)}
          </TotalText>
        </View>
        <TotalBottomBorder />
      </TotalBar>
    </StyledInsuranceProductList>
  )
}

export const InsuranceProductListItem: React.FC<InsuranceProductListItemProps> = (props) => {
  let imageSource
  if (props.iconType === 'card') {
    imageSource = require('../../assets/icons/card.png')
  } else if (props.iconType === 'star') {
    imageSource = require('../../assets/icons/star.png')
  } else {
    imageSource = require('../../assets/icons/star.png')
  }

  const isSigned = props.signed
  return (
    <InsuranceListItem>
      <TopBorder/>
      <ContentWrap>
        <Icon>
          <Image source={imageSource} width={17} height={21}/>
        </Icon>
        <View>
          <ProductTitle>{props.title}</ProductTitle>
        </View>
      </ContentWrap>
      <Cost>R{props.cost}</Cost>
      <ButtonWrap>
        {!isSigned && <DefaultButton buttonType={'light'} title={'SIGN'} disabled={false} onPress={() => null}/>}
        {isSigned && <SignatureArea/>}
      </ButtonWrap>
    </InsuranceListItem>
  )
}

const StyledInsuranceProductList = styled.View`

`

const TopBar = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  padding-bottom: 8px;
`

const TopText = styled.Text<{ hasRightPadding: boolean }>`
  font-weight: bold;
  font-size: 11px;
  letter-spacing: 0.4px;
  color: #9A9DA0;
  width: 200px;
  padding-right: ${props => props.hasRightPadding ? 50 : 0}px;
`

const RightItemsWrap = styled.View`
  flex-direction: row;
`

const InsuranceList = styled.View`

`

const InsuranceListItem = styled.View`
  height: 84px;
  width: 100%;
`

const ContentWrap = styled.View`
  height: 100%;
  flex-direction: row;
  align-items: center;
`

const Cost = styled.Text`
  font-weight: 500;
  font-size: 14px;
  letter-spacing: 0.25px;
  color: #353C43;
  position: absolute;
  left: 499px;
  top: 30px;
  text-align: right;
`

const ButtonWrap = styled.View`
  width: 200px;
  position: absolute;
  right: 0;
  top: 14px;
`

const SignatureArea = styled.View`
  width: 200px;
  height: 56px;
  border: 1px solid #D1D2D4;
  border-radius: 4px;
`

const Icon = styled.View`
  width: 31px;
  height: 31px;
  border: 1px solid #ECECEE;
  border-radius: 16px;
  margin-right: 10px;
  align-items: center;
  justify-content: center;
`

const ProductTitle = styled.Text`
  font-weight: 500;
  font-size: 14px;
  letter-spacing: 0.25px;
  color: #353C43;
`

const ProductDescription = styled.Text`
  font-size: 12px;
  letter-spacing: 0.4px;
  color: #9A9DA0;

`

const TopBorder = styled.View`
  width: 100%;
  height: 1px;
  background-color: #ECECEE;
`

const TotalTopBorder = styled.View`
  width: 100%;
  height: 1px;
  background-color: #D1D2D4;
`

const TotalBottomBorder = styled(TotalTopBorder)`
  position: absolute;
  bottom: 0;
`

const TotalBar = styled.View`
  height: 53px;
  background: #F9F9FB;
`

const TotalText = styled.Text`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  letter-spacing: 0.25px;
  color: #353C43;
`
