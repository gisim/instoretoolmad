import React from 'react'
import styled from 'styled-components/native'
import {ActivityIndicator} from "react-native"

interface DefaultButtonProps {
  buttonType: "default" | "light" | "smallLight" | "smallDefault"
  title: string
  disabled: boolean
  onPress: any
  isLoading?: boolean
  isSuccess?: boolean
  width?: number
}

interface DefaultButtonContentProps {
  buttonType: string
  title: string
  disabled: boolean
  onPress: any
  isLoading?: boolean
}

export const DefaultButton: React.FC<DefaultButtonProps> = (props) => {
  const isSuccess = props.isSuccess ?? false
  return (
    <>
      {props.buttonType === 'default' && (
        <StyledDefaultButton isDisabled={props.disabled} isSuccess={isSuccess}>
          <DefaultButtonContent
            buttonType={'default'}
            title={props.title}
            disabled={props.disabled}
            onPress={props.onPress}
            isLoading={props.isLoading}
          />
        </StyledDefaultButton>
      )}
      {props.buttonType === 'light' && (
        <StyledDefaultButtonLight isDisabled={props.disabled} isSuccess={isSuccess}>
          <DefaultButtonContent
            buttonType={'light'}
            title={props.title}
            disabled={props.disabled}
            onPress={props.onPress}
            isLoading={props.isLoading}
          />
        </StyledDefaultButtonLight>
      )}
      {props.buttonType === 'smallLight' && (
        <StyledDefaultButtonSmallLight isDisabled={props.disabled} width={props.width ?? 60} isSuccess={isSuccess}>
          <DefaultButtonContent
            buttonType={'light'}
            title={props.title}
            disabled={props.disabled}
            onPress={props.onPress}
            isLoading={props.isLoading}
          />
        </StyledDefaultButtonSmallLight>
      )}
      {props.buttonType === 'smallDefault' && (
        <StyledDefaultButtonSmall isDisabled={props.disabled} isSuccess={isSuccess}>
          <DefaultButtonContent
            buttonType={'default'}
            title={props.title}
            disabled={props.disabled}
            onPress={props.onPress}
            isLoading={props.isLoading}
          />
        </StyledDefaultButtonSmall>
      )}
    </>
  )
}

const DefaultButtonContent: React.FC<DefaultButtonContentProps> = (props) => {
  if (props.isLoading) {
    return (
      <StyledTouchArea>
        <ActivityIndicator size={"small"} animating={true} color={'gray'} />
      </StyledTouchArea>
    )
  }

  return (
    <>
      {props.disabled && (
        <StyledTouchArea activeOpacity={0.7} disabled={true}>
          <StyledDisabledButtonText>{props.title}</StyledDisabledButtonText>
        </StyledTouchArea>
      )}
      {!props.disabled && (
        <StyledTouchArea activeOpacity={0.7} onPress={props.onPress}>
          {props.buttonType === 'default' && (
            <StyledButtonText>{props.title}</StyledButtonText>
          )}
          {props.buttonType === 'light' && (
            <StyledLightButtonText>{props.title}</StyledLightButtonText>
          )}
        </StyledTouchArea>
      )}
    </>
  )
}

const StyledDefaultButton = styled.View<{ isDisabled: boolean, isSuccess: boolean }>`
  width: 200px;
  max-width: 200px;
  height: 56px;
  background-color: ${props => props.isSuccess ? '#18a611' : (props.isDisabled ? '#ececee' : '#353c43')};
  box-shadow: ${props => props.isDisabled ? 'none' : '0px 0px 10px rgba(0, 0, 0, 0.0001)'};
  border-radius: 4px;
  overflow: hidden;
`

const StyledDefaultButtonSmall = styled(StyledDefaultButton)`
  width: 60px;
  height: 28px;
`

const StyledDefaultButtonLight = styled(StyledDefaultButton)`
  background-color: #ffffff;
  border: 1px solid #d1d2d4;
`

const StyledDefaultButtonSmallLight = styled(StyledDefaultButtonLight)<{ width: number }>`
  width: ${props => props.width}px;
  height: 28px;
`

const StyledTouchArea = styled.TouchableOpacity`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`

const StyledButtonText = styled.Text`
  font-weight: 500;
  font-size: 14px;
  text-transform: uppercase;
  color: #ffffff;
  letter-spacing: 1.35px;
  text-align: center;
`

const StyledLightButtonText = styled(StyledButtonText)`
  color: #353c43;
`

const StyledDisabledButtonText = styled(StyledButtonText)`
  color: #d1d2d4;
`
