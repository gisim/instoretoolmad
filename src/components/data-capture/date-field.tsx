import React from 'react'
import styled from 'styled-components/native'
import {Label} from "./default-drop-down"

interface DateFieldProps {
  title: string
  value: string
  onPressed: () => void
}

export const DateField: React.FC<DateFieldProps> = (props) => {
  const value = props.value.length ? props.value : props.title

  return (
    <>
      <Container onPress={props.onPressed}>
        <PostalCodeText>{value}</PostalCodeText>
      </Container>
      {props.value.length > 0 &&
      <Label>{props.title}</Label>
      }
    </>
  )
}

const Container = styled.TouchableOpacity`
  border-width: 1px;
  border-color: #ececee;
  width: 100%;
  height: 56px;
  margin-top: 7px;
  border-radius: 4px;
  display: flex;
  justify-content: center;
  padding: 0px 15px;
`

const PostalCodeText = styled.Text`
  font-size: 16px;
  letter-spacing: 0.444444px;
  color: #9a9da0;
`
