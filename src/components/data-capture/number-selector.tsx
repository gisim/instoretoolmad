import React, {useEffect, useState} from 'react'
import styled from "styled-components/native"

interface NumberSelectorProps {
  title?: string
  showErrorBorder?: boolean
  errorText?: string|null
  onValueUpdated: (value: number) => void
  value: number
  valueTitle: string
  valueTitlePlural: string
  resetAt?: number
}

interface ValueIncreaseButtonProps {
  onPress: () => void
}

interface ValueDecreaseButtonProps {
  onPress: () => void
  disabled: boolean
}

export const NumberSelector: React.FC<NumberSelectorProps> = (props) => {
  const [value, setValue] = useState(props.value)
  const hasError = (props.errorText !== null)
  const showErrorBorder: boolean = props.showErrorBorder ?? hasError
  const {onValueUpdated, resetAt} = props

  useEffect(() => {
    onValueUpdated(value)

    if (resetAt && value === resetAt) {
      setValue(0)
    }
  }, [value])

  const increaseValue = () => {
    setValue(v => {
      if (v === 50) {
        return v
      }
      return ++v
    })
  }

  return (
    <Container>
      <ValueSelector style={showErrorBorder ? {borderColor: '#f44336'} : {borderColor: '#ececee'}}>
        <ValueDecreaseButton
          onPress={() => setValue(y => --y)}
          disabled={value === 0}
        />
        <ValueWrap>
          <ValueOutputNumber>
            {props.value}
          </ValueOutputNumber>
          <ValueOutputTitle>
            {props.value === 1 ? props.valueTitle : props.valueTitlePlural}
          </ValueOutputTitle>
        </ValueWrap>
        <ValueIncreaseButton onPress={increaseValue}/>
      </ValueSelector>

      {props.title !== undefined &&
        <Label>{props.title}</Label>
      }

      {hasError && (
        <ErrorLabel>
          {props.errorText}
        </ErrorLabel>
      )}
    </Container>
  )
}

const ValueIncreaseButton: React.FC<ValueIncreaseButtonProps> = (props) => {
  return (
    <ValueInputButton style={{right: 0}} onPress={props.onPress}>
      <ValueInputButtonDividerRight/>
      <ValueInputIncreaseIcon source={require('../../assets/icons/Add.png')}/>
    </ValueInputButton>
  )
}

const ValueDecreaseButton: React.FC<ValueDecreaseButtonProps> = (props) => {
  return (
    <ValueInputButton style={{left: 0}} onPress={props.onPress} disabled={props.disabled}>
      <ValueInputButtonDividerLeft/>
      <ValueInputDecreaseIcon source={require('../../assets/icons/Remove.png')}/>
    </ValueInputButton>
  )
}

const Container = styled.View`
  width: 49%;
  display: flex;    
`

const ErrorLabel = styled.Text` 
  text-align: right;
  font-size: 12px;
  letter-spacing: 0.4px;
  color: #F44336;
  bottom: -5px;
`

const ValueSelector = styled.View`  
  height: 56px;
  border: 1px solid #ECECEE;
  border-radius: 4px;
  flex-direction: row;    
`

const ValueWrap = styled.View`
  flex-direction: row;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 89%;
  margin-left: 56px;
`

const ValueOutputNumber = styled.Text`
  font-size: 16px;
  text-align: center;
  letter-spacing: 0.444444px;
  color: #353C43;
`

const ValueOutputTitle = styled(ValueOutputNumber)`
  color: #9A9DA0;
  margin-left: 7px;
`

const ValueInputButton = styled.TouchableOpacity`
  width: 56px;
  height: 100%;
  position: absolute;
  z-index: 10;
  display: flex;
  align-items: center;
  justify-content: center;
`

const ValueInputButtonDividerLeft = styled.View`
  height: 100%;
  width: 1px;
  background-color: #ECECEE;
  position: absolute;
  right: 0;
`

const ValueInputButtonDividerRight = styled.View`
  height: 100%;
  width: 1px;
  background-color: #ECECEE;
  position: absolute;
  left: 0;
`

const ValueInputIncreaseIcon = styled.Image`
  width: 14px;
  height: 14px;
  display: flex;
  align-items: center;
  justify-content: center;
`

const ValueInputDecreaseIcon = styled(ValueInputIncreaseIcon)`
  height: 2px;
`

const Label = styled.Text`
  position: absolute;
  z-index: 99999;
  left: 10px;
  top: -8px;
  background: white;
  padding: 0px 8px 0 5px;
  font-size: 13px;
  color: #9a9da0;
`
