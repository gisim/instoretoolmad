import React, {useState, useRef, useEffect} from 'react'
import styled from 'styled-components/native'

import {DefaultRadioItem} from "./default-radio-item"
import {LookUpData} from "../../models/lookup-data"
import {AppStorageService} from "../../services/app-storage-service"
import {ScrollView} from "react-native"

interface HorizontalRadioGroup {
  id: string
  checked: boolean
  label: string
}

interface DefaultHorizontalRadioGroupProps {
  title: string
  radioList: HorizontalRadioGroup[]
  radioListLookUp: string | undefined
  onChange?: (group: HorizontalRadioGroup) => void
  value: string|null
}

export const DefaultHorizontalRadioGroup: React.FC<DefaultHorizontalRadioGroupProps> = (props) => {
  const { lookUp }  = AppStorageService
  const { onChange, value } = props
  const [checkedIndex, setCheckedIndex] = useState<number | null>(null)
  const options = useRef<HorizontalRadioGroup[]>(props.radioList)

  useEffect(() => {
    if (checkedIndex) { return }
    if (!value) { return }

    options.current.forEach((option, index) => {
      if (option.label.toLowerCase() === "yes" && value.toLowerCase() === "true") {
        setCheckedIndex(index)
      }

      if (option.label.toLowerCase() === "no" && value.toLowerCase() === "false") {
        setCheckedIndex(index)
      }

      if (option.id !== value) { return }
      setCheckedIndex(index)
    })

  }, [checkedIndex, setCheckedIndex, value])

  const updateCheckedState = (index: number, isChecked: boolean) => {
    if (isChecked && onChange) {
      setCheckedIndex(index)
      onChange(options.current[index])
    }
  }

  if (options.current.length === 0 && props.radioListLookUp && props.radioListLookUp in lookUp) {
    // @ts-ignore (needed to force `lookUp` to resolve, which it will since the above line makes sure of that
    options.current = (lookUp[props.radioListLookUp] as LookUpData[]).map((option: LookUpData) => ({
      id: `${option.Id}`,
      checked: false,
      label: option.Description,
    }))
  }

  if (!options.current) {
    return null
  }

  const radioGroup = options.current.map(((item, index) => {
    return (
      <DefaultRadioItem
        key={item.id}
        style={'horizontal'}
        checked={checkedIndex === index}
        label={item.label}
        onChange={(isChecked) => {
          updateCheckedState(index, isChecked)
        }}
      />
    )
  }))

  return (
    <HorizontalRadioGroupContent>
      <HorizontalGroupTopBar>
        <GroupTitle>
          {props.title}
        </GroupTitle>
      </HorizontalGroupTopBar>
      <RadioGroup>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {radioGroup}
        </ScrollView>
      </RadioGroup>
    </HorizontalRadioGroupContent>
  )
}

const HorizontalRadioGroupContent = styled.View`
  width: 100%;
`

const RadioGroup = styled.View`
  width: 100%;
  flex-direction: row;
`

const HorizontalGroupTopBar = styled.View`
  margin-bottom: 10px;
`

const GroupTitle = styled.Text`
  font-size: 16px;
  line-height: 24px;
  letter-spacing: 0.444444px;
  color: #353C43;
`
