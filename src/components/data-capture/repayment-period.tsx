import React, {useEffect, useState} from 'react'
import styled from "styled-components/native"
import {useMutation, useQuery} from "react-query"
import {getActiveDivisionsQuery, getAvailableAccountOptions} from "../../services/api-service"
import {ActivityIndicator, Alert} from "react-native"
import {ApplicationApplyObjectListDTO} from "../../models/get-available-account-options-response"
import {Column, Row} from "../forms/column"
import {ReadOnlyField} from "./read-only-field"
import {ListSelector} from "./list-selector"
import {DefaultModal} from "../modal/default-modal"
import {LookUpData} from "../../models/lookup-data"

type LookUpModalData = { value: string, items: LookUpData[] }

interface RepaymentPeriodProps {
  idNumber: string | undefined
  detailsSelected: (division: string, repaymentPeriodId: string) => void
  divisionValue: string|null
  repaymentPeriodValue: string|null
  divisionErrorText: string | null
  repaymentPeriodErrorText: string | null
}

export const RepaymentPeriod: React.FC<RepaymentPeriodProps> = (props) => {
  const {isLoading, data} = useQuery('active-divisions', getActiveDivisionsQuery)
  const [getAccountOptions, {isLoading: getAccountOptionsLoading, data: accountOptions}] = useMutation(getAvailableAccountOptions)
  const [division, setDivision] = useState<string | null>(null)
  const [selectedRepaymentPeriodId, setSelectedRepaymentPeriodId] = useState<string | null>(null)
  const [showLookUpSelector, setShowLookUpSelector] = useState<LookUpModalData|null>(null)

  const {idNumber} = props

  useEffect(() => {
    if (!division) {
      return
    }
    if (accountOptions) {
      return
    }
    if (!idNumber) {
      return
    }
    getAccountOptions({idNumber: idNumber})
      .catch(console.error)
  }, [division, accountOptions, idNumber])

  useEffect(() => {
    if (!division) { return }
    setSelectedRepaymentPeriodId(null)
  }, [division])

  useEffect(() => {
    if (!accountOptions) { return }
    if (!accountOptions.error) { return }
    Alert.alert('Error', `An unexpected error occurred. Please reload the form and try again.\n\n${accountOptions.error}`, [
      { text: 'Okay' },
    ])
  }, [accountOptions])

  useEffect(() => {
    if (selectedRepaymentPeriodId) { return }
    if (!accountOptions) { return }
    if (!accountOptions.ApplicationApplyObjectListDTO) { return }
    const repaymentDivision: ApplicationApplyObjectListDTO[] = accountOptions.ApplicationApplyObjectListDTO.filter(div => `${div.DivisionId}` === division)
    if (repaymentDivision && repaymentDivision.length > 0) {
      const twelveMonth = repaymentDivision[0].RepaymentPeriod.find(rd => rd.RepaymentPeriodDescription.trim().toLowerCase() === "12 month")
      if (twelveMonth) {
        setSelectedRepaymentPeriodId(`${twelveMonth.RepaymentPeriodId}`)
        setRepaymentPeriod(`${twelveMonth.RepaymentPeriodId}`)
      } else {
        setSelectedRepaymentPeriodId(null)
      }
    }
  }, [selectedRepaymentPeriodId, accountOptions, setSelectedRepaymentPeriodId])

  if (accountOptions && accountOptions.error) {
    return null
  }

  const setRepaymentPeriod = (period: string) => {
    setSelectedRepaymentPeriodId(period)
    props.detailsSelected(division!, period)
  }

  if (isLoading || !data) {
    return (
      <LoadingContainer>
        <ActivityIndicator animating={isLoading} color={'gray'} />
      </LoadingContainer>
    )
  }

  const divisions: LookUpData[] = data.map(d => ({Description: d.DivisionDesc, Id: d.DivisionId, Code: ""}))

  const repaymentDivision: ApplicationApplyObjectListDTO[] | null = accountOptions?.ApplicationApplyObjectListDTO?.filter(div => `${div.DivisionId}` === division) ?? null
  let periods: LookUpData[] = []
  if (repaymentDivision && repaymentDivision.length > 0) {
    periods = repaymentDivision[0].RepaymentPeriod.map(period => ({
      Description: period.RepaymentPeriodDescription,
      Id: period.RepaymentPeriodId,
      Code: "",
    }))
  }

  let selectedDivision = 'Division'
  let selectedPeriod = 'Repayment Period'

  if (division) {
    selectedDivision = divisions.filter(d => `${d.Id}` === division)[0].Description
  }

  if (selectedRepaymentPeriodId && periods.length > 0) {
    const foundPeriod = periods.filter(d => `${d.Id}` === selectedRepaymentPeriodId)
    if (foundPeriod.length > 0) {
      selectedPeriod = foundPeriod[0].Description
    }
  }

  return (
    <>
      <Row zIndex={10}>
        <Column columns={1} isDropDown={true}>
          <ReadOnlyField
            title={'Division'}
            value={selectedDivision}
            onPressed={() => {
              setShowLookUpSelector({value: 'DivisionId', items: divisions })
            }}
            errorText={props.divisionErrorText}
          />
        </Column>
      </Row>
      <Row zIndex={1}>
        <Column columns={1} isDropDown={true}>
          {division && !getAccountOptionsLoading &&
            <ReadOnlyField
              title={'Repayment Period'}
              value={selectedPeriod}
              onPressed={() => {
                setShowLookUpSelector({value: 'RepaymentPeriodId', items: periods })
              }}
              errorText={props.repaymentPeriodErrorText}
            />
          }
          {division && getAccountOptionsLoading &&
            <LoadingContainer>
              <ActivityIndicator size={"small"} animating={getAccountOptionsLoading} color={'gray'} />
            </LoadingContainer>
          }
        </Column>
      </Row>

      <DefaultModal
        visible={showLookUpSelector !== null}
        title={"Select a value"}
        headerType={"small"}
        showBottomButtons={false}
        height={'500px'}
        width={'576px'}
      >
        <ListSelector
          values={showLookUpSelector?.items ?? []}
          onValueSelected={(value) => {
            switch (showLookUpSelector?.value) {
              case 'DivisionId': {
                setDivision(`${value.Id}`)
                break
              }
              case 'RepaymentPeriodId': {
                setRepaymentPeriod(`${value.Id}`)
                break
              }
            }
            setShowLookUpSelector(null)
          }}
        />
      </DefaultModal>
    </>
  )
}

const LoadingContainer = styled.View`
  display: flex;
  align-items: center;  
  justify-content: center;
  height: 50px;
`
