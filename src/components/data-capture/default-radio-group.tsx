import React, {useEffect, useState} from 'react'
import styled from 'styled-components/native'

import {DefaultRadioItem} from "./default-radio-item"

interface VerticalRadioGroup {
    id: number
    checked: boolean
    label: string
    hasRightLabel: boolean
    rightLabel: string
}

interface VerticalRadioGroup {}

interface DefaultRadioGroupProps {
    style: string
    radioList: VerticalRadioGroup[]
    defaultCheckedIndex? : number
    onChange?: (group: VerticalRadioGroup) => void
}

export const DefaultRadioGroup: React.FC<DefaultRadioGroupProps> = (props) => {
    const [checkedIndex, setCheckedIndex] = useState<number>(props.defaultCheckedIndex ? props.defaultCheckedIndex : -1)

    useEffect(() => {
        if (props.onChange) {
            props.onChange(props.radioList[checkedIndex])
        }
    }, [checkedIndex])

    const updateCheckedState = (index: number, isChecked: boolean) => {
        if (isChecked) {
            setCheckedIndex(index)
        }
    }

    const radioGroup = props.radioList.map(((item, index) => {
        return (
            <DefaultRadioItem
                key={item.id}
                style={props.style}
                checked={checkedIndex === index}
                label={item.label}
                rightLabel={item.hasRightLabel}
                rightLabelText={item.rightLabel}
                onChange={(isChecked) => {
                    updateCheckedState(index, isChecked)
                }}
            />
        )
    }))
    return (
        <>
            {props.style === 'vertical' && (
                <RadioGroup>
                    {radioGroup}
                </RadioGroup>
            )}
            {props.style === 'horizontal' && (
                <RadioGroup>
                    {radioGroup}
                </RadioGroup>
            )}
        </>
    )
}

const RadioGroup = styled.View`
  width: 100%;
`
