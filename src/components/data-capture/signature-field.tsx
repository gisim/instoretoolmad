import React from 'react'
import styled from "styled-components/native"
import {Image} from "react-native"

interface SignatureFieldProps {
  onPress: () => void
  value: string
  errorText?: string | null
}

export const SignatureField: React.FC<SignatureFieldProps> = (props) => {
  const hasImage = (props.value && props.value.length > 0) === true
  const hasError = (props.errorText !== null)

  return (
    <>
      <Container onPress={props.onPress} style={hasError ? {borderColor: '#f44336'} : {borderColor: '#ececee'}}>
        <Title>Signature</Title>
        {!hasImage &&
          <Caption>Tap to sign</Caption>
        }
        {hasImage &&
          <Image source={{uri: `data:image/png;base64,${props.value}`}} style={{width: 320, height: 120, resizeMode: "contain"}}/>
        }
      </Container>
      {hasError && (
        <ErrorLabel>
          {props.errorText}
        </ErrorLabel>
      )}
    </>
  )
}

const Container = styled.TouchableOpacity`
  border-width: 1px;
  width: 100%;
  height: 130px;
  margin-top: 7px;
  border-radius: 4px;
  display: flex;
  align-items: center;
  justify-content: center; 
`

const Caption = styled.Text`
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
  display: flex;
  align-items: center;
  text-align: center;
  letter-spacing: 0.444444px;
  color: #9A9DA0;
`

const Title = styled.Text`
  position: absolute;
  z-index: 99999;
  left: 10px;
  top: -8px;
  background: white;
  padding: 0px 8px 0 5px;
  font-size: 13px;
  color: #9a9da0;
`

const ErrorLabel = styled.Text`  
  text-align: right;
  font-size: 12px;
  letter-spacing: 0.4px;
  color: #F44336;
  bottom: -5px;
`
