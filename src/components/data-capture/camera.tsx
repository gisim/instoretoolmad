import React, {useRef, useState} from 'react'
import {RNCamera} from "react-native-camera"
import {StyleSheet, View} from "react-native"
import {DefaultButton} from "./default-button"
import styled from "styled-components/native"

interface CameraProps {
  onUploadComplete: (base64: string) => void
  onCancel: () => void
}

export const Camera: React.FC<CameraProps> = (props) => {
  const camera = useRef<RNCamera | null>(null)
  const [hasTakenPhoto, setHasTakenPhoto] = useState(false)
  const base64 = useRef<string|null>(null)

  const captureImage = async () => {
    if (!camera.current) { return }
    const options = {
      quality: 0.8,
      base64: true,
      doNotSave: true,
      pauseAfterCapture: true,
      width: 2000,
    }
    const data = await camera.current.takePictureAsync(options)
    if (data.base64) {
      const sanitised = data.base64.replace(/\r?\n|\r/g, "")
      base64.current = sanitised
    }
    setHasTakenPhoto(true)
  }

  const retake = () => {
    if (!camera.current) { return }
    camera.current.resumePreview()
    setHasTakenPhoto(false)
  }

  const captureComplete = () => {
    if (!hasTakenPhoto) { return }
    if (base64.current) {
      props.onUploadComplete(base64.current)
    }
  }

  const cancel = () => {
    props.onCancel()
  }

  return (
    <>
      <View style={styles.container}>
        <RNCamera
          ref={camera}
          type={"back"}
          style={styles.preview}
          captureAudio={false}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Okay',
            buttonNegative: 'Cancel',
          }}
          cameraViewDimensions={{
            width: 400,
            height: 400,
          }}
        />
      </View>
      <Container>
        <DefaultButton buttonType={"light"} title={"Close"} disabled={false} onPress={cancel}/>
        <ButtonsContainer>
          {hasTakenPhoto && <View style={{ marginRight: 10 }}><DefaultButton buttonType={"light"} title={"Retake"} disabled={false} onPress={retake}/></View>}
          {!hasTakenPhoto && <DefaultButton buttonType={"default"} title={"Capture"} disabled={false} onPress={captureImage}/>}
          {hasTakenPhoto && <DefaultButton buttonType={"default"} title={"Upload"} disabled={false} onPress={captureComplete} isSuccess={hasTakenPhoto}/>}
        </ButtonsContainer>
      </Container>
    </>
  )
}

const Container = styled.View`
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  flex-direction: row;
  margin-top: 10px;
`

const ButtonsContainer = styled.View`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  flex-direction: row;
`

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    overflow: 'hidden',
    height: 400,
  },
})

