import styled from "styled-components/native"

export const ErrorLabel = styled.Text`
  top: 100%;
  margin: 0 -16px;
  text-align: right;
  font-size: 12px;
  letter-spacing: 0.4px;
  color: #F44336;
`
