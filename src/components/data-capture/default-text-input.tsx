import React, {useEffect, useState} from 'react'
import styled from 'styled-components/native'
import {KeyboardTypeOptions, Platform, StyleProp, ViewStyle} from "react-native"
import { ErrorLabel } from './error-label'

interface DefaultTextInputProps extends TextInputProps {
  type: "text" | "number" | "password" | "email" | "money";
}

interface TextInputProps {
  placeholder: string
  errorText?: string | null
  keyboardType?: KeyboardTypeOptions
  autoCapitalize?: 'none' | 'sentences' | 'words' | 'characters'
  style?: StyleProp<ViewStyle>
  isDisabled?: boolean
  autoFocus?: boolean

  value: string
  setValue: (value: string) => void
}

export const DefaultTextInput: React.FC<DefaultTextInputProps> = (props) => {
  let keyboardType: KeyboardTypeOptions = props.keyboardType ?? "default"
  let autoCapitalize = props.autoCapitalize ?? 'sentences'

  switch (props.type) {
    case "email": {
      keyboardType = "email-address"
      autoCapitalize = "none"
      break
    }
    case "number": {
      keyboardType = "number-pad"
      break
    }
    case "money": {
      keyboardType = "decimal-pad"
      break
    }
    default: {
      break
    }
  }

  const setValue = (val: string) => {
    switch (props.type) {
      case "money": {
        if (val.length === 0) {
          props.setValue(val)
        }
        if (/^\d+$/.test(val)) {
          props.setValue(val)
        }
        break
        // The below will validate a decimal
        // if (val === "") {
        //   props.setValue(val)
        //   break
        // }
        // // @ts-ignore
        // if (val.endsWith(".") && !val.startsWith(".") && [0, 1].includes(val.match(/\./g).length)) {
        //   props.setValue(val)
        //   break
        // }
        // const parsedVal = val.replace(",", ".")
        // if (/^-{0,1}\d*\.{0,1}\d+$/.test(parsedVal)) {
        //   props.setValue(parseFloat(parsedVal).toFixed(2))
        // } else {
        //   console.log('negative', parseFloat(parsedVal), parsedVal)
        // }
        // break
      }
      default: {
        props.setValue(val)
        break
      }
    }
  }

  switch (props.type) {
    case "email":
    case "number":
    case "money":
    case "text": {
      return <TextTypeInput
        placeholder={props.placeholder}
        errorText={props.errorText}
        value={props.value}
        setValue={setValue}
        keyboardType={keyboardType}
        autoCapitalize={autoCapitalize}
        style={props.style}
        autoFocus={props.autoFocus}
      />
    }
    case "password": {
      return <PasswordTypeInput
        placeholder={props.placeholder}
        errorText={props.errorText}
        value={props.value}
        setValue={props.setValue}
        style={props.style}
        autoFocus={props.autoFocus}
      />
    }
  }

  return null
}

const TextTypeInput: React.FC<TextInputProps> = (props) => {
  const [floatingPlaceholder, setFloatingPlaceholder] = useState(props.value.length > 0)
  const [textColor, setTextColor] = useState(true)
  const isDisabled = props.isDisabled ?? false
  const { value } = props

  const showFloatingPlaceholder = () => {
    setFloatingPlaceholder(true)
    setTextColor(false)
  }

  useEffect(() => {
    if (floatingPlaceholder) { return }
    if (value.length === 0) { return }
    setFloatingPlaceholder(true)
  }, [floatingPlaceholder, setFloatingPlaceholder, value])

  const error = (props.errorText != null)

  return (
    <StyledDefaultTextInput style={[props.style, error ? {borderColor: '#f44336'} : {borderColor: '#ececee'}]}>
      {floatingPlaceholder && (
        <InputFocusedPlaceholder placeholder={props.placeholder}/>
      )}
      <StyledInputArea
        style={{color: textColor ? '#9A9DA0' : '#353C43'}}
        onChangeText={(text) => props.setValue(text)}
        value={props.value}
        placeholder={props.placeholder}
        onFocus={showFloatingPlaceholder}
        keyboardType={props.keyboardType ?? "default"}
        autoCapitalize={props.autoCapitalize}
        clearButtonMode={"while-editing"}
        editable={!isDisabled}
        autoFocus={props.autoFocus ?? false}
      />
      {error && (
        <ErrorLabel>
          {props.errorText}
        </ErrorLabel>
      )}
    </StyledDefaultTextInput>
  )
}

const PasswordTypeInput: React.FC<TextInputProps> = (props) => {
  const [floatingPlaceholder, setFloatingPlaceholder] = useState(false)
  const [textColor, setTextColor] = useState(true)
  const [enableVisibilityToggle, setEnableVisibilityToggle] = useState(true)
  const [passwordVisible, setPasswordVisible] = useState(false)
  const { value } = props

  const showFloatingPlaceholder = () => {
    setFloatingPlaceholder(true)
    setTextColor(false)
    setEnableVisibilityToggle(false)
    setPasswordVisible(true)
  }
  const togglePasswordVisible = () => {
    setPasswordVisible(!passwordVisible)
  }


  useEffect(() => {
    if (floatingPlaceholder) { return }
    if (value.length === 0) { return }
    setFloatingPlaceholder(true)
  }, [floatingPlaceholder, setFloatingPlaceholder, value])

  const error = (props.errorText != null)

  return (
    <StyledDefaultTextInput style={[props.style, error ? {borderColor: '#f44336'} : {borderColor: '#ececee'}]}>
      {floatingPlaceholder && (
        <InputFocusedPlaceholder placeholder={props.placeholder}/>
      )}
      <StyledInputArea
        style={{color: textColor ? '#9A9DA0' : '#353C43'}}
        onChangeText={(text) => props.setValue(text)}
        value={props.value}
        onFocus={showFloatingPlaceholder}
        autoCompleteType={'password'}
        placeholder={props.placeholder}
        secureTextEntry={passwordVisible}
        autoFocus={props.autoFocus ?? false}
      />
      <StyledRightIconTouchArea
        onPress={togglePasswordVisible}
        disabled={enableVisibilityToggle}
      >
        <StyledRightIcon source={require('../../assets/icons/show.png')}/>
      </StyledRightIconTouchArea>
      {error && (
        <ErrorLabel>
          {props.errorText}
        </ErrorLabel>
      )}
    </StyledDefaultTextInput>
  )
}

const InputFocusedPlaceholder: React.FC<{ placeholder: string }> = (props) => {
  return (
    <StyledFocusedPlaceholderWrap>
      <StyledFocusedPlaceholder>{props.placeholder}</StyledFocusedPlaceholder>
    </StyledFocusedPlaceholderWrap>
  )
}

const StyledDefaultTextInput = styled.View`
  height: 56px;
  width: 100%;
  border: 1px solid;
  border-radius: 4px;
  padding: 16px;
  margin-top: 7px;
  margin-bottom: 30px;  
`

const StyledInputArea = styled.TextInput`
  width: 100%;
  height: ${Platform.OS === 'ios' ? '100%' : '40px'};
  font-size: 16px;
  letter-spacing: 0.444444px;  
  margin-top: ${Platform.OS === 'ios' ? '0' : '-6px;'};
`

const StyledFocusedPlaceholderWrap = styled.View`
  position: absolute;
  top: -8px;
  left: 8px;
  height: 16px;
  background-color: #ffffff;
  padding: 0 13px 0 8px;
`

const StyledFocusedPlaceholder = styled.Text`
  font-size: 13px;
  color: #9a9da0;
`

const StyledRightIconTouchArea = styled.TouchableOpacity`
  width: 24px;
  height: 24px;
  position: absolute;
  right: 16px;
  top: 16px;
`

const StyledRightIcon = styled.Image`
  width: 100%;
  height: 100%;
`
