import React, {useEffect, useState} from 'react'
import {UploadRequest} from "../modal/upload-documents"
import styled from "styled-components/native"
import {ActivityIndicator, Image} from "react-native"
import {useMutation} from "react-query"
import {uploadDocumentMutation} from "../../services/api-service"

interface UploaderProps {
  request: UploadRequest
  documentUploaded?: (isSuccess: boolean) => void
}

export const Uploader: React.FC<UploaderProps> = (props) => {
  const [uploadDocument, { isLoading, isSuccess }] = useMutation(uploadDocumentMutation)
  const [isUploaded, setIsUploaded] = useState(false)
  const {request, documentUploaded} = props

  const uploadFile = () => {
    const now = new Date()
    const body = {
      "CommunicationQueueId": request.communicationQueueId,
      "SupportingDocumentType": request.typeId,
      "FileType": "image/jpeg",
      "FileBytes": `data:image/jpg;base64,${request.base64}`,
      "IsActive": true,
      "DateUploaded": now.toISOString(),
      "UploadedBy": "C3PO",
    }

    uploadDocument({ body })
      .then((response) => {
        if (!response) { return }
        if (!documentUploaded) { return }
        documentUploaded(response.ResponseStatus === 0)
        request.base64 = null
        setIsUploaded(true)
      })
  }

  useEffect(() => {
    if (!request.base64) { return }
    if (isUploaded) { return }
    if (isLoading) { return }
    if (isSuccess) { return }
    uploadFile()
  }, [request, uploadDocument])

  useEffect(() => {
    if (isSuccess) {
      setIsUploaded(true)
    }
    if (!request.base64) {
      setIsUploaded(true)
    }
  }, [isSuccess, request])

  return (
    <UploadRecord>
      <DocumentType>{request.type}</DocumentType>
      <TickView isLoading={isLoading} isSuccess={!isLoading && isUploaded}/>
    </UploadRecord>
  )
}

const TickView: React.FC<{ isLoading: boolean, isSuccess: boolean }> = (props) => {
  const { isLoading, isSuccess } = props
  return (
    <TickViewContainer>
      {isSuccess &&
      <Image source={require('../../assets/icons/tick-green.png')}/>
      }
      {isLoading &&
      <ActivityIndicator size={"small"} animating={true} style={{marginRight: 10}} color={'gray'} />
      }
    </TickViewContainer>
  )
}

const UploadRecord = styled.View`
  width: 100%;
  height: 25px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

const DocumentType = styled.Text`
  font-size: 16px;
`

const TickViewContainer = styled.View`
  width: 30px
`
