import React, {useEffect, useRef, useState} from 'react'
import styled from 'styled-components/native'

import {DefaultRadioItem} from "./default-radio-item"
import {AppStorageService} from "../../services/app-storage-service"
import {LookUpData} from "../../models/lookup-data"

interface VerticalRadioGroup {
    id: string
    checked: boolean
    label: string
    hasRightLabel: boolean
    rightLabel: string
}

interface DefaultVerticalRadioGroupProps {
    title: string
    rightTitle?: string
    radioList: VerticalRadioGroup[]
    radioListLookUp: string | undefined
    value: string
    onChange?: (group: VerticalRadioGroup) => void
    canUncheck?: boolean
}

export const DefaultVerticalRadioGroup: React.FC<DefaultVerticalRadioGroupProps> = (props) => {
    const { lookUp }  = AppStorageService
    const { value } = props
    const [checkedIndex, setCheckedIndex] = useState<number>(-1)
    const options = useRef<VerticalRadioGroup[]>(props.radioList)

    if (options.current.length === 0 && props.radioListLookUp && props.radioListLookUp in lookUp) {
        // @ts-ignore (needed to force `lookUp` to resolve, which it will since the above line makes sure of that
        options.current = (lookUp[props.radioListLookUp] as LookUpData[]).map((option: LookUpData) => ({
            id: `${option.Id}`,
            checked: `${option.Id}` === value,
            label: option.Description,
            hasRightLabel: false,
            rightLabel: '',
        }))
    }

    useEffect(() => {
        options.current.forEach((option, index) => {
            if (option.id === value) {
                setCheckedIndex(index)
            }
        })
    }, [value])

    if (!options.current) {
        return null
    }

    const updateCheckedState = (index: number, isChecked: boolean) => {
        if (isChecked && props.onChange) {
            setCheckedIndex(index)
            props.onChange(options.current[index])
        } else {
            if (props.canUncheck && props.onChange) {
                props.onChange(options.current[index])
                setCheckedIndex(-1)
            }
        }
    }

    const rightTitle = (props.rightTitle != null)

    const radioGroup = options.current.map(((item, index) => {
        return (
            <DefaultRadioItem
                key={item.id}
                style={'vertical'}
                checked={checkedIndex === index}
                label={item.label}
                rightLabel={item.hasRightLabel}
                rightLabelText={item.rightLabel}
                onChange={(isChecked) => {
                    updateCheckedState(index, isChecked)
                }}
            />
        )
    }))
    return (
        <>
            <RadioGroup>
                <VerticalTopBar>
                    <RadioGroupTitle>
                        {props.title}
                    </RadioGroupTitle>
                    {rightTitle && (
                        <RadioGroupRightTitle>
                            {props.rightTitle}
                        </RadioGroupRightTitle>
                    )}
                </VerticalTopBar>
                {radioGroup}
            </RadioGroup>
        </>
    )
}

const RadioGroup = styled.View`
  width: 100%;
`

const VerticalTopBar = styled.View`
  flex-direction: row;
  margin-bottom: 12px;
`

const RadioGroupTitle = styled.Text`
  font-weight: 500;
  font-size: 14px;
  letter-spacing: 1.35px;
  text-transform: uppercase;
  color: #353C43;
`

const RadioGroupRightTitle = styled(RadioGroupTitle)`
  position: absolute;
  right: 0;
`
