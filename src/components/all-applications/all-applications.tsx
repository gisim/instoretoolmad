import React from 'react';
import styled from 'styled-components/native';

import {ApplicationCell} from '../data-display/application-cell';
import {StackNavigationProp} from "@react-navigation/stack"
import {MainStackParamList} from "../../../App"


type AllApplicationsNavigationProps = StackNavigationProp<MainStackParamList, 'AllApplications'>

export interface ApplicationType {
  id: number
  icon: string
  idNumber: string
  consumerName: string
  applicationType: string
  state: string
}

const allApplicationsList = [
  {
    id: 0,
    icon: 'application',
    idNumber: '910423 5046 081',
    consumerName: 'Buddy Patterson',
    applicationType: 'Account Application',
    state: 'processing',
  },
  {
    id: 1,
    icon: 'insurance',
    idNumber: '860524 3815 041',
    consumerName: 'Monika Pittman',
    applicationType: 'Insurance',
    state: 'processing',
  },
  {
    id: 2,
    icon: 'application',
    idNumber: '780314 2974 061',
    consumerName: 'Reem Chambers',
    applicationType: 'Account Application',
    state: 'processing',
  },
  {
    id: 3,
    icon: 'mrpMobile',
    idNumber: '950129 6986 062',
    consumerName: 'Alys Glass',
    applicationType: 'Mr Price Mobile',
    state: 'processing',
  },
  {
    id: 4,
    icon: 'mrpMobile',
    idNumber: '840322 7891 071',
    consumerName: 'Alice Bellamy',
    applicationType: 'Mr Price Mobile',
    state: 'error',
  },
  {
    id: 5,
    icon: 'application',
    idNumber: '790614 6478 031',
    consumerName: 'Jemimah Handson',
    applicationType: 'Account Application',
    state: 'approved',
  },
  {
    id: 6,
    icon: 'application',
    idNumber: '861009 6732 032',
    consumerName: 'Abbie Beck',
    applicationType: 'Account Application',
    state: 'denied',
  },
];

interface AllApplicationsProps {
  navigation: AllApplicationsNavigationProps
}

export const AllApplications: React.FC<AllApplicationsProps> = () => {
  return (
    <StyledAllApplicationsList>
      <ApplicationCell applicationList={allApplicationsList} />
    </StyledAllApplicationsList>
  );
};

const StyledAllApplicationsList = styled.ScrollView`
  padding: 0 21px;
`;
