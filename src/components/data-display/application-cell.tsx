import React from 'react'
import styled from 'styled-components/native'
import {TouchableOpacity} from 'react-native'
import {IconCell} from './icon-cell'
import {ApplicationType} from "../all-applications/all-applications"
import {useNavigation} from "@react-navigation/native"
import {FormType} from "../../services/form-service"

interface ApplicationCellProps {
  applicationList: ApplicationType[];
}

interface CellIconProps {
  iconType: string;
}

export const ApplicationCell: React.FC<ApplicationCellProps> = (props) => {
  const navigation = useNavigation()

  const allApplications = props.applicationList.map((application) => {
    return (
      <TouchableOpacity key={application.id} activeOpacity={0.7} onPress={() => navigation.navigate('FormRenderer', { form: FormType.Complete })}>
        <Container>

          <CellIconView>
            <IconCell size={'small'} type={application.icon}/>
          </CellIconView>

          <CellColumn>
            <CellIdLabel>
              {application.idNumber}
            </CellIdLabel>
          </CellColumn>

          <CellColumn>
            <NameLabel>
              {application.consumerName}
            </NameLabel>
          </CellColumn>

          <CellColumn>
            <NameLabel>
              {application.applicationType}
            </NameLabel>
          </CellColumn>

          <StatusIcon>
            <CellIcon iconType={application.state}/>
          </StatusIcon>

        </Container>

        <HorizontalBorder/>
      </TouchableOpacity>
    )
  })
  return <>{allApplications}</>
}

const CellIcon: React.FC<CellIconProps> = (props) => {
  return (
    <>
      {props.iconType === 'processing' && (
        <IconImage
          source={require('../../assets/icons/pending.png')}
        />
      )}
      {props.iconType === 'error' && (
        <IconImage
          source={require('../../assets/icons/error.png')}
        />
      )}
      {props.iconType === 'approved' && (
        <IconImage
          source={require('../../assets/icons/approved.png')}
        />
      )}
      {props.iconType === 'denied' && (
        <IconImage
          source={require('../../assets/icons/denied.png')}
        />
      )}
    </>
  )
}

const HorizontalBorder = styled.View`
  height: 1px;
  width: 100%;
  background-color: #edf2f7;
`

const Container = styled.View`
  display: flex;
  flex-direction: row;
  height: 60px;
  padding: 0;
`

const CellIconView = styled.View`
  display: flex;
  padding-right: 15px;
  align-items: center;
  justify-content: center;
`

const CellColumn = styled.View`
  width: 23.5%;
  display: flex;
  justify-content: center;
`

const CellIdLabel = styled.Text`
  font-size: 14px;
  letter-spacing: 0.25px;
  color: #353c43;
`

const NameLabel = styled(CellIdLabel)`
  font-size: 12px;
  letter-spacing: 0.4px;
  color: #6e6e77;
`

const StatusIcon = styled(CellColumn)`
  align-items: flex-end;
`

const IconImage = styled.Image``
