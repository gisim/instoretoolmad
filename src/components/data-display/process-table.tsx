import React, {useCallback, useEffect, useRef, useState} from 'react'

import {PageTitle} from './page-title'
import {StepCell} from './step-cell'
import {Alert, KeyboardAvoidingView, TouchableOpacity, View} from "react-native"
import {useNavigation} from '@react-navigation/native'
import {FormType, ValueType} from "../../services/form-service"
import {DefaultModal} from "../modal/default-modal"
import {CaptureCustomerId} from "../modal/capture-customer-id"
import {useMutation} from "react-query"
import {
  getCustomerApplicationsQuery,
  getCustomerInfo,
  getFinalOutcomeQuery,
  getInstoreApplicationDataQuery,
} from "../../services/api-service"
import {Config} from "../../config"
import {AppStorageService} from "../../services/app-storage-service"
import {ApplicationDetailList} from "../../models/get-customer-applications-response"
import {FormNameType} from "../../models/form"
import {sanitizeApplicationData, sanitizeCustomerInfo} from "../../services/customer-service"
import {CompleteType} from "../../forms/form-processor"

type ProcessType = 'application' | 'activation'
type ScreenType = "fullscreen" | "modal" | "screen"


interface FormScreenParams {
  screen?: string
  screenType?: ScreenType
  form?: FormType
  formData?: { [key: string]: ValueType }
  processType?: ProcessType
}

interface Process extends FormScreenParams {
  id: number
  title: string
  subtext: string
  icon: string
}

export interface ProcessCell {
  id: number
  title: string
  cells: Process[];
}

interface ProcessTableProps {
  processList: ProcessCell[]
}

type NewApplicationStatus = { status: "New" }
type CompletedApplicationStatus = { status: "Completed", outcome: CompleteType }
type InProgressApplicationStatus = { status: "InProgress", application: ApplicationDetailList }
type ErrorApplicationStatus = { status: "Error" }

type ApplicationStatus =
  | NewApplicationStatus
  | CompletedApplicationStatus
  | InProgressApplicationStatus
  | ErrorApplicationStatus


export const ProcessTable: React.FC<ProcessTableProps> = (props) => {
  const navigation = useNavigation()
  const [getCustomerApplications] = useMutation(getCustomerApplicationsQuery)
  const [getInstoreApplicationData] = useMutation(getInstoreApplicationDataQuery)
  const [getFinalOutcome] = useMutation(getFinalOutcomeQuery)
  const [showIdModal, setShowIdModal] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [isIdModalActionButtonEnabled, setIsIdModalActionButtonEnabled] = useState(false)
  const customerIdNumber = useRef<{ number: string|null, type: string } | null>(null)
  const processType = useRef<ProcessType>('application')

  useEffect(() => {
    if (isLoading) { return }
    setShowIdModal(false)
  }, [isLoading, setShowIdModal])

  const goToScreen = useCallback((process: FormScreenParams) => {
    if (!process.screen) {
      return
    }
    const screenType = process.screenType ?? "fullscreen"

    switch (screenType) {
      case "fullscreen": {
        let processor = 'new-application'
        if (process.form === FormType.Complete) {
          processor = 'complete-application'
        }

        navigation.navigate(process.screen!, {
          form: process.form,
          formData: process.formData,
          processor,
        })
        break
      }
      case "modal": {
        switch (process.screen!) {
          case "CaptureCustomerId": {
            processType.current = process.processType!
            setShowIdModal(true)
            break
          }
        }
        break
      }
      case "screen": {
        navigation.navigate(process.screen!)
        break
      }
    }
  }, [])

  const processCustomerId = () => {
    if (!customerIdNumber.current) { return }
    setIsLoading(true)

    if (processType.current === "activation") {
      getCustomerApplications({ idNumber: customerIdNumber.current.number!, processId: 105 })
        .then((result) => {
          try {
            if (result!.ApplicationDetailList.length > 0) {
              const sortedApplications = result!.ApplicationDetailList.sort((a, b) => {
                return (a.ModifiedDate > b.ModifiedDate) ? -1 : ((a.ModifiedDate < b.ModifiedDate) ? 1 : 0)
              })

              const lastApplication = sortedApplications[0]
              let mustUploadDocuments = false

              if (lastApplication.AccountCode !== undefined && lastApplication.AccountCode !== null) {
                throw 'No available activations exist for this customer.'
              }

              switch (lastApplication.ChannelDescription) {
                case "Instore Application":
                case "WhatsApp":
                case "Omni Formae": {
                  mustUploadDocuments = true
                  break
                }
                default:
                  break
              }

              navigation.navigate('ActivateCard', {
                mustUploadDocuments,
                idNumber: customerIdNumber.current!.number!,
                commQueueId: lastApplication.CommunicationQueueId,
              })
              setIsLoading(false)
            } else {
              setIsLoading(false)
              throw 'This customer has no activations pending.'
            }
          } catch (e) {
            setIsLoading(false)
            setTimeout(() => {
              Alert.alert(`Can't find customer`, e, [{text: 'Okay'}])
            }, 750)
          }
        })

      return
    }


    getCustomerApplications({ idNumber: customerIdNumber.current.number!, processId: 2 })
      .then((result) => {
        let applicationStatus: Promise<ApplicationStatus> = Promise.resolve({ status: "New" })

        if (result!.ApplicationDetailList.length > 0) {
          const sortedApplications = result!.ApplicationDetailList.sort((a, b) => {
            return (a.ModifiedDate > b.ModifiedDate) ? -1 : ((a.ModifiedDate < b.ModifiedDate) ? 1 : 0)
          })

          const lastApplication = sortedApplications[0]

          switch (lastApplication.ProcessStatusId) {
            case 4: {
              applicationStatus = getFinalOutcome({ communicationQueueId: `${lastApplication.CommunicationQueueId}` })
                .then(data => {
                  return { status: "Completed", outcome: {status: "final", creditLimit: data!.CreditLimit}}
                })
              break
            }
            case 5: {
              applicationStatus = getFinalOutcome({ communicationQueueId: `${lastApplication.CommunicationQueueId}` })
                .then(() => {
                  return { status: "Completed", outcome: {
                      status: "error",
                      type: "rejected",
                      error: {
                        title: "Rejected",
                        message: "Your application is unsuccessful due to you not meeting the minimum criteria, however you can apply for a Lay-By account (Subject to In Store availability). To get more information regarding your application, contact our call centre on 0800 222 639."
                      },
                    } }
                })
              break
            }
            case 7: {
              applicationStatus = getFinalOutcome({ communicationQueueId: `${lastApplication.CommunicationQueueId}` })
                .then(data => {
                  return { status: "Completed", outcome: {status: "pending", creditLimit: data!.InitialCreditLimit} }
                })
              break
            }
            case 3:
            case 99:
            case 108: {
              applicationStatus = Promise.resolve({ status: "InProgress", application: result!.ApplicationDetailList[0] })
              break
            }
            case 91:
            case 92:
            case 93:
            case 94:
            case 107: {
              applicationStatus = Promise.resolve({status: "New"})
              break
            }
            default: {
              applicationStatus = Promise.resolve({status: "Error"})
              break
            }
          }
        } else {
          applicationStatus = Promise.resolve({status: "New"})
        }

        return Promise.all([
          applicationStatus,
          getCustomerInfo({ idNumber: customerIdNumber.current!.number! }),
          AppStorageService.getActiveStaffCode(),
        ])
      })
      .then(data => {
        const applicationStatus = data[0] as ApplicationStatus
        const details = data[1] as { [key: string]: any }
        const staffCode = data[2] as string | null
        const customerDetails = sanitizeCustomerInfo(details)

        switch (applicationStatus.status) {
          case "New": {
            setIsLoading(false)
            goToScreen({
              screen: 'FormRenderer',
              screenType: "fullscreen",
              form: FormType.New,
              formData: Config.mode === "develop"
                ? {
                  ...Config.TemporaryInitialState,
                  ...customerDetails,
                  StaffCode: {"type": "string", value: staffCode ?? ''},
                  StoreId: { "type": "int", value: `${AppStorageService.activeStoreId}` },
                  IdentityTypeId: {"type": "int", value: customerIdNumber.current!.type},
                  FirstName2: {"type": "string", value: ""},
                }
                : {
                  ...customerDetails,
                  IdentityNumber: {"type": "string", value: customerIdNumber.current!.number!},
                  IdentityTypeId: {"type": "int", value: customerIdNumber.current!.type},
                  StaffCode: {"type": "string", value: staffCode ?? ''},
                  StoreId: { "type": "int", value: `${AppStorageService.activeStoreId}` },
                  FirstName2: {"type": "string", value: ""},
                },
            })
            break
          }
          case "InProgress": {
            const { CommunicationQueueId, NewApplicationId } = applicationStatus.application
            getInstoreApplicationData({ communicationQueueId: CommunicationQueueId, newApplicationId: NewApplicationId })
              .then((existingData) => {
                setIsLoading(false)
                const newFormData = sanitizeApplicationData(existingData!)

                const formData = {
                  ...customerDetails,
                  ...newFormData,
                  "StaffCode": { type: "string" as FormNameType, value: staffCode ?? ''},
                }

                goToScreen({
                  screen: 'FormRenderer',
                  screenType: "fullscreen",
                  form: FormType.Complete,
                  formData,
                })
              })
            break
          }
          case "Completed": {
            setIsLoading(false)
            switch (applicationStatus.outcome.status) {
              case "pending": {
                navigation.navigate("FormPending", { creditLimit: applicationStatus.outcome.creditLimit })
                break
              }
              case "final": {
                navigation.navigate('FormSuccess', { creditLimit: applicationStatus.outcome.creditLimit })
                break
              }
              case "error": {
                navigation.navigate("FormError", applicationStatus.outcome.error)
                break
              }
            }
            break
          }
          default: {
            setIsLoading(false)
            setTimeout(() => {
              Alert.alert('Contact Call Centre', 'Please contact the call centre on\n0800 222 639 for more information about this application.', [{text: "Okay"}])
            }, 1000)
            break
          }
        }
      })
      .catch((error) => {
        setIsLoading(false)
        console.error(error)
      })
  }

  const applicationCategories = props.processList.map((category) => {
    const applicationTypes = category.cells.map((appType) => (
      <TouchableOpacity
        activeOpacity={1.0}
        onPress={() => goToScreen(appType)}
        key={appType.title}
      >
        <StepCell
          key={appType.id}
          iconSize={'large'}
          iconType={appType.icon}
          title={appType.title}
          subText={appType.subtext}
        />
      </TouchableOpacity>
    ))

    return (
      <View key={category.id}>
        <View style={{marginTop: 44}} />
        <PageTitle
          key={category.id}
          title={category.title}
          titleType={'cellTitle'}
          textAlign={'left'}
        />
        {applicationTypes}
      </View>
    )
  })

  return (
    <>
      {applicationCategories}

      <DefaultModal
        visible={showIdModal}
        title={"Customer ID required"}
        headerType={"large"}
        showBottomButtons={true}
        width={'576px'}
        height={'540px'}
        cancelButtonTitle={"Cancel"}
        actionButtonTitle={"Next"}
        onCancelButtonPressed={() => setShowIdModal(false)}
        onActionButtonPressed={processCustomerId}
        isActionButtonEnabled={!isLoading && isIdModalActionButtonEnabled}
      >
        <KeyboardAvoidingView>
          <CaptureCustomerId
            isLoading={isLoading}
            enableActionButtonHandler={setIsIdModalActionButtonEnabled}
            setCustomerIdNumber={(idNumber, idType) => {
              if (!idNumber) { return }
              customerIdNumber.current = { number: idNumber, type: idType }
            }}
          />
        </KeyboardAvoidingView>
      </DefaultModal>
    </>
  )
}
