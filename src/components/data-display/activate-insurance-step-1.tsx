import React, {useRef, useState} from 'react'
import {PageTitle} from "./page-title"
import styled from "styled-components/native"
import {BarCodeReadEvent, RNCamera} from "react-native-camera"
import {StyleSheet, View} from "react-native"
import {DefaultModal} from "../modal/default-modal"
import {ColumnFill, ColumnWidth} from "../forms/column"
import {DefaultTextInput} from "../data-capture/default-text-input"
import {DefaultButton} from "../data-capture/default-button"

interface ActivateInsuranceStep1Props {
  onStoreCardInput: (input: string) => void
  storeCardError: string|null|undefined
  isLoading: boolean
  cardNumber: string
}

export const ActivateInsuranceStep1: React.FC<ActivateInsuranceStep1Props> = (props) => {
  const [isShowingScanner, setIsShowingScanner] = useState(false)
  const [storeCardNumber, setStoreCardNumber] = useState(props.cardNumber)
  const camera = useRef<RNCamera|null>(null)

  const onScanPressed = () => {
    setIsShowingScanner(true)
  }

  const onBarcodeRead = (event: BarCodeReadEvent) => {
    if (!isShowingScanner) { return }
    if (event.data.length === 0) { return }
    const cardNumber = event.data.slice(0, 10)
    setCardNumber(cardNumber)
  }

  const setCardNumber = (cardNumber: string) => {
    setStoreCardNumber(cardNumber)
    props.onStoreCardInput(cardNumber)
    setIsShowingScanner(false)
  }

  return (
    <>
      <PageTitle titleType={'pageTitle'} title={'Customer Validation'} textAlign={'left'}/>
      <TextWrap>
        Before we can get started we need to confirm who you are. Please provide your store card.
      </TextWrap>

      <Row>
        <ColumnFill top={0} style={{ height: 100 }}>
          <DefaultTextInput
            type={"number"}
            placeholder={"Store Card Number"}
            value={storeCardNumber}
            setValue={setCardNumber}
            errorText={props.storeCardError}
          />
        </ColumnFill>
        <ColumnWidth width={200} top={4}>
          <DefaultButton buttonType={"default"} title={"Scan"} disabled={props.isLoading} onPress={onScanPressed} isLoading={props.isLoading} />
        </ColumnWidth>
      </Row>

      <DefaultModal
        visible={isShowingScanner}
        title={'Scan Barcode'}
        headerType={'small'}
        showBottomButtons={true}
        height={'300px'}
        width={'400px'}
        cancelButtonTitle={'Close'}
        onCancelButtonPressed={() => setIsShowingScanner(false)}
      >
        {isShowingScanner &&
        <View style={styles.container}>
            <RNCamera
                ref={camera}
                type={"back"}
                style={styles.preview}
                onBarCodeRead={(event) => {
                  camera.current?.pausePreview()
                  onBarcodeRead(event)
                }}
                captureAudio={false}
                androidCameraPermissionOptions={{
                  title: 'Permission to use camera',
                  message: 'We need your permission to use your camera',
                  buttonPositive: 'Okay',
                  buttonNegative: 'Cancel',
                }}
            />
        </View>
        }
      </DefaultModal>
    </>
  )
}

const TextWrap = styled.Text`
  margin-top: 16px;
  color: #6E6E77;
`

const Row = styled.View`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;  
  margin-top: 44px;
`

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    overflow: 'hidden',
    height: 50,
  },
})
