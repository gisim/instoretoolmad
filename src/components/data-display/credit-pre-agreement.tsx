import React, {useEffect, useRef, useState} from 'react'
import {StyleSheet, View, Text, ActivityIndicator, Alert} from "react-native"
import {AppStorageService} from "../../services/app-storage-service"
import {User} from "../../models/login-response"
import {SignatureField} from "../data-capture/signature-field"
import {CaptureSignature} from "../modal/capture-signature"
import {DefaultModal} from "../modal/default-modal"
import {DefaultButton} from "../data-capture/default-button"
import ViewShot, {captureRef} from "react-native-view-shot"
import {useMutation} from "react-query"
import {emailPreAgreementMutation, uploadDocumentMutation} from "../../services/api-service"
import styled from "styled-components/native"
import {GetPreAgreementDataResponse} from "../../models/get-pre-agreement-data-response"

export interface PreAgreementInformation {
  "DivisionID": string
  "DivisionName": string
  "StoreCode": string
  "UserCode": string
  "AssociateFullName": string
  "VATRegNumber": string
  "ReferenceNo": string
  "Date": string
  "Till": string
  "NCRREGNumber": string
  "CompanyName": string
  "RegistrationNumber": string
  "ContactNumber": string
  "EmailAddress": string
  "PhysicalAddress": string
  "CustomerName": string
  "IdentityNumber/Passport": string
  "PostalAddress1": string
  "PostalAddress2": string
  "PostalAddress3": string
  "PostalCode": string
  "PhysicalAddress1": string
  "PhysicalAddress2": string
  "PhysicalAddress3": string
  "ResPostalCode": string
  "ContactNumberHome": string
  "ContactNumberWork": string
  "CreditLimit": string
  "Plan": string
  "Installment": string
  "Terms": string
  "AnnualInterestRate": string
  "CreditLimitMultiplier": string
  "InitiationFeeAmount": string
  "ServiceFeeAmount": string
  "CustomerProtectionPlan": string
  "LostCardProtectionPlan": string
  "ClubFees": string
  "A2BCommuterPersonalAccidentPlan": string
  "MedinetCriticalIllnessAndHospitalisationPlan": string
  "FamilyFuneralPlan": string
  "DomesticCareProtectionPlan": string
  "360DegreeProtectionPlan": string
}


interface CreditPreAgreementProps {
  response: GetPreAgreementDataResponse
  commQueueId: number
  onApplicationSubmitted: () => void
}

type SignatureType = "cashier" | "consumer"

export const CreditPreAgreement: React.FC<CreditPreAgreementProps> = (props) => {
  const [uploadDocument, { isLoading: uploadDocumentLoading }] = useMutation(uploadDocumentMutation)
  const [sendEmailMutation, { isLoading: sendEmailLoading }] = useMutation(emailPreAgreementMutation)
  const [user, setUser] = useState<User|null>(null)
  const [showSignatureModal, setShowSignatureModal] = useState<SignatureType|null>(null)
  const [cashierSignature, setCashierSignature] = useState<string|null>(null)
  const [consumerSignature, setConsumerSignature] = useState<string|null>(null)
  const viewShot = useRef<ViewShot|null>(null)
  const Data: PreAgreementInformation = props.response.preAgreementInformation
  const associate = props.response.AssociateDetail

  const isLoading = uploadDocumentLoading || sendEmailLoading

  useEffect(() => {
    if (user) { return }
    AppStorageService.getUser()
      .then((u) => {
        setUser(u)
      })
  }, [user, setUser])

  const captureScreenshot = () => {
    if (!viewShot.current) { return }

    captureRef(viewShot, {
      format: "jpg",
      quality: 1.0,
      result: "base64",
    })
      .then((result) => {
        const sanitised = result.replace(/\r?\n|\r/g, "")
        uploadFile(sanitised)
      })
  }

  const uploadFile = (base64: string) => {
    const now = new Date()
    const body = {
      "CommunicationQueueId": props.commQueueId,
      "SupportingDocumentType": 8,
      "FileType": "image/jpeg",
      "FileBytes": `data:image/jpg;base64,${base64}`,
      "IsActive": true,
      "DateUploaded": now.toISOString(),
      "UploadedBy": "C3PO",
    }

    uploadDocument({ body })
      .then((response) => {
        if (!response) {
          throw `[Failed to upload document]`
        }
        return sendEmailMutation({ communicationQueueId: props.commQueueId })
          .then((response) => {
            if (response!) {
              throw `[Failed to send email]`
            }
          })
          .catch((e) => {
            console.log('ActivateCardStep3', e)
          })
      })
      .then((response) => {
        props.onApplicationSubmitted()
      })
      .catch((e) => {
        console.log('Pre-agreement error: ', e)
        Alert.alert(
          'Error',
          'An error occurred while trying to upload this document. Please ensure you have network connectivity and try again.',
          [{ text: 'Okay' }]
        )
      })
  }

  if (!user) {
    return null
  }

  if (isLoading) {
    return (
      <LoadingContainer>
        <ActivityIndicator animating={isLoading} color={'gray'} />
      </LoadingContainer>
    )
  }

  const canSubmit = cashierSignature && cashierSignature.length > 0
    && consumerSignature && consumerSignature.length > 0

  return (
    <View style={{ backgroundColor: 'white', marginBottom: 100 }}>
      <ViewShot style={styles.container} ref={viewShot}>
        <Text style={styles.sectionTitle}>
          Credit Provider Details
        </Text>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Division Name
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.DivisionName}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Store Code
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.StoreCode}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            User Code
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.UserCode}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Associate Full Name
          </Text>
          <Text style={styles.sectionDataBold}>
            {associate.StaffFirstName} {associate.StaffSurname}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            VAT Reg Number
          </Text>
          <Text style={styles.sectionData}>
            {Data.VATRegNumber}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Reference Number
          </Text>
          <Text style={styles.sectionData}>
            {Data.ReferenceNo}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Date
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.Date}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Till Number
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.Till}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            NCR Reg Number
          </Text>
          <Text style={styles.sectionData}>
            {Data.NCRREGNumber}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Company Name
          </Text>
          <Text style={styles.sectionData}>
            {Data.CompanyName}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Registration Number
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.RegistrationNumber}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Contact Number
          </Text>
          <Text style={styles.sectionData}>
            {Data.ContactNumber}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Physical Address
          </Text>
          <Text style={styles.sectionData}>
            {Data.PhysicalAddress}
          </Text>
        </View>
        <Text style={styles.sectionTitle}>
          Customer Details
        </Text>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Customer Name
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.CustomerName}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            ID/Passport Number
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data["IdentityNumber/Passport"]}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Contact Number (H)
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.ContactNumberHome}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Contact Number (W)
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.ContactNumberWork}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Postal Address
          </Text>
          <Text style={styles.sectionDataBold}>
            {[Data.PostalAddress1, Data.PostalAddress2, Data.PostalAddress3, Data.PostalCode]
              .filter(text => text.length > 0)
              .join("\n")}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Cost of Credit
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.AnnualInterestRate}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Terms
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.Terms}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Payment
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.Installment}
          </Text>
        </View>
        <Text style={styles.sectionTitle}>
          Account Facility Quotation
        </Text>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Credit Limit
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.CreditLimit}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Terms
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.Terms}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Plan
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.Plan}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Installment
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.Installment}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}/>
          <Text style={styles.sectionDataAdditional}>
            Plus interest over time
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Annual Interest
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.AnnualInterestRate}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Credit Multiplier
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.CreditLimitMultiplier}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Initial Fee
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.InitiationFeeAmount}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Monthly Service Fee
          </Text>
          <Text style={styles.sectionDataBold}>
            {Data.ServiceFeeAmount}
          </Text>
        </View>
        <Text style={styles.paragraph}>
          This agreement is concluded at the maximum interest prescribed by the NATIONAL CREDIT ACT, currently 21.00%.
          Accounts in arrears are charged at the maximum prescribed rate. The normal interest rate may vary with changes
          in the repo rate as described in the Terms and Conditions.
        </Text>
        <Text style={styles.sectionTitle}>
          Insurance Options
        </Text>
        <Text style={styles.paragraphVariantTwo}>
          If you have chosen any of the following options, your maximum monthly installments will be increased by the
          amounts reflected below.
        </Text>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Customer Protection Plan
          </Text>
          <Text style={(Data.CustomerProtectionPlan === 'R0.00') ? styles.sectionData : styles.sectionDataBold}>
            {Data.CustomerProtectionPlan}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Lost Card Protection Plan
          </Text>
          <Text style={(Data.LostCardProtectionPlan === 'R0.00') ? styles.sectionData : styles.sectionDataBold}>
            {Data.LostCardProtectionPlan}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Club Fees
          </Text>
          <Text style={(Data.ClubFees === 'R0.00') ? styles.sectionData : styles.sectionDataBold}>
            {Data.ClubFees}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            A2B Commuter Personal Accident Plan
          </Text>
          <Text style={(Data.A2BCommuterPersonalAccidentPlan === 'R0.00') ? styles.sectionData : styles.sectionDataBold}>
            {Data.A2BCommuterPersonalAccidentPlan}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Medinet Critical Illness And Hospitalisation Plan
          </Text>
          <Text
            style={(Data.MedinetCriticalIllnessAndHospitalisationPlan === 'R0.00') ? styles.sectionData : styles.sectionDataBold}>
            {Data.MedinetCriticalIllnessAndHospitalisationPlan}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Family Funeral Plan
          </Text>
          <Text style={(Data.FamilyFuneralPlan === 'R0.00') ? styles.sectionData : styles.sectionDataBold}>
            {Data.FamilyFuneralPlan}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            Domestic Care Protection Plan
          </Text>
          <Text style={(Data.DomesticCareProtectionPlan === 'R0.00') ? styles.sectionData : styles.sectionDataBold}>
            {Data.DomesticCareProtectionPlan}
          </Text>
        </View>
        <View style={styles.sectionWrap}>
          <Text style={styles.sectionItem}>
            360 Degree Protection Plan
          </Text>
          <Text style={(Data["360DegreeProtectionPlan"] === 'R0.00') ? styles.sectionData : styles.sectionDataBold}>
            {Data["360DegreeProtectionPlan"]}
          </Text>
        </View>

        <Text style={styles.sectionTitle}>
          Cashier Signature
        </Text>
        <Text style={styles.paragraphVariantTwo}>
          By signing I, {associate.StaffFirstName} {associate.StaffSurname}, the credit provider or duly authorized representative present,
          bear witness to the acceptance of he pre-agreement quotation by the consumer.
        </Text>
        <View style={{ width: 324 }}>
          <SignatureField
            value={cashierSignature ?? ''}
            onPress={() => setShowSignatureModal("cashier")}
            errorText={null}
          />
        </View>

        <Text style={styles.sectionTitle}>
          Consumer Signature
        </Text>
        <Text style={styles.paragraphVariantTwo}>
          By signing I, {Data.CustomerName}, confirm that the above details are correct and that I accept this pre-agreement quotation.
        </Text>
        <View style={{ width: 324 }}>
          <SignatureField
            value={consumerSignature ?? ''}
            onPress={() => setShowSignatureModal("consumer")}
            errorText={null}
          />
        </View>
    </ViewShot>

      <DefaultButton
        buttonType={"default"}
        title={"Submit"}
        disabled={!canSubmit}
        onPress={captureScreenshot}
      />

      <DefaultModal
        visible={showSignatureModal !== null}
        title={"Signature"}
        headerType={"small"}
        showBottomButtons={false}
        width={"720px"}
        height={"350px"}
      >
        <CaptureSignature
          onCancelButtonPressed={() => setShowSignatureModal(null)}
          onActionButtonPressed={(base64) => {
            const sanitised = base64.replace(/\r?\n|\r/g, "")
            switch (showSignatureModal) {
              case "cashier": {
                setCashierSignature(sanitised)
                break
              }
              case "consumer": {
                setConsumerSignature(sanitised)
                break
              }
            }
            setShowSignatureModal(null)
          }}
        />
      </DefaultModal>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    paddingBottom: 100,
    backgroundColor: 'white',
    padding: 20,
  },
  mainTitle: {
    fontWeight: '600',
    fontSize: 20,
    lineHeight: 28,
    color: '#353C43'
  },
  mainSubtitle: {
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.444444,
    color: '#353C43',
    marginTop: 16,
    marginBottom: -20
  },
  sectionTitle: {
    fontWeight: '600',
    fontSize: 18,
    color: '#353C43',
    marginTop: 52,
    marginBottom: 22,
  },
  sectionWrap: {
    flexDirection: "row",
    marginBottom: 8,
  },
  sectionItem: {
    width: 239,
    fontSize: 14,
    lineHeight: 20,
    letterSpacing: 0.25,
    color: '#353C43',
  },
  sectionData: {
    fontWeight: '600',
    fontSize: 14,
    lineHeight: 20,
    letterSpacing: 0.25,
    color: '#6E6E77',
    maxWidth: 186,
  },
  sectionDataBold: {
    fontWeight: '600',
    fontSize: 14,
    letterSpacing: 0.25,
    color: '#353C43',
    maxWidth: 186,
  },
  sectionDataAdditional: {
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 20,
    letterSpacing: 0.25,
    color: '#6E6E77',
    marginTop: -8,
  },
  paragraph: {
    marginTop: 24,
    fontSize: 14,
    lineHeight: 20,
    letterSpacing: 0.25,
    color: '#353C43',
    maxWidth: 600,
  },
  paragraphVariantTwo: {
    marginTop: 24,
    fontSize: 14,
    lineHeight: 20,
    letterSpacing: 0.25,
    color: '#353C43',
    maxWidth: 600,
    marginBottom: 32,
  }
})

const LoadingContainer = styled.View`
  display: flex;
  align-items: center;  
  justify-content: center;
`
