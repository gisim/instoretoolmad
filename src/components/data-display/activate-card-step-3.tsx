import React from 'react'
import {DefaultButton} from "../data-capture/default-button"
import {useNavigation} from '@react-navigation/native'
import {useMutation} from "react-query"
import {emailPreAgreementMutation} from "../../services/api-service"
import styled from 'styled-components/native'

interface ActivateCardStep3Props {
  commQueueId: number
}

export const ActivateCardStep3: React.FC<ActivateCardStep3Props> = (props) => {
  const [sendEmailMutation, { isLoading }] = useMutation(emailPreAgreementMutation)
  const navigation = useNavigation()

  const takeMeBackPressed = () => {
    navigation.navigate('Dashboard')
  }

  const sendEmail = () => {
    sendEmailMutation({ communicationQueueId: props.commQueueId })
      .then(() => {
        takeMeBackPressed()
      })
      .catch((e) => {
        console.log('ActivateCardStep3', e)
      })
  }

  return (
    <Container>
      <DefaultButton buttonType={"light"} title={"Take Me Back"} disabled={false} onPress={takeMeBackPressed} />
      <DefaultButton buttonType={"default"} title={"Send Email"} disabled={isLoading} isLoading={isLoading} onPress={sendEmail} />
    </Container>
  )
}

const Container = styled.View`
  display: flex;
  align-items: flex-end; 
  justify-content: space-between;
  flex-direction: row;
  margin-top: 40px;
`
