import React, {useRef, useState} from 'react'
import styled from 'styled-components/native'
import {DefaultButton} from '../data-capture/default-button'
import {TextArea} from './text-area'
import {DefaultCheckBox} from "../data-capture/default-check-box"
import {View, Text, ActivityIndicator, Image, Alert, TouchableOpacity, StyleSheet} from "react-native"
import {SignatureField} from "../data-capture/signature-field"
import {DisclaimerBox} from "./disclaimer-box"
import {useMutation, useQuery} from "react-query"
import {
  createFinancialProduct,
  getInsuranceDisclaimer,
  uploadInsuranceDocumentMutation
} from "../../services/api-service"
import {LoadingContainer} from "../shared"
import {FinancialServiceType} from "../../models/get-insurance-products-response"
import {CaptureSignature} from "../modal/capture-signature"
import {DefaultModal} from "../modal/default-modal"
import ViewShot, {captureRef} from "react-native-view-shot"
import {UploadInsuranceDocumentRequest} from "../../models/upload-insurance-document-request"
import {CreateFinancialServiceProductWithBeneficiaryRequest} from "../../models/create-financial-product-request"

interface ActivateInsuranceStep4Props {
  products: FinancialServiceType[]
  onSubmitted: () => void
  customerCategoryId: number
  customerId: number
  accountCode: string
}

interface InsuranceProductListItemProps {
  iconType: string
  title: string
  cost: string
  signature: string
  onPress: () => void
}

type InitialSignature = {
  [key: number]: string
}

export const ActivateInsuranceStep4: React.FC<ActivateInsuranceStep4Props> = (props) => {
  const [uploadDocument, {isLoading: isUploading}] = useMutation(uploadInsuranceDocumentMutation)
  const [createProducts, {isLoading: isCreatingProducts}] = useMutation(createFinancialProduct)
  const [agreedToTerms, setAgreedToTerms] = useState(false)
  const {data: disclaimer, isLoading: disclaimerLoading} = useQuery<string>('disclaimer', getInsuranceDisclaimer)
  const [signature, setSignature] = useState('')
  const [initials, setInitials] = useState<InitialSignature>({})
  const [showSignatureModal, setShowSignatureModal] = useState(false)
  const [showInitialModal, setShowInitialModal] = useState<number | null>(null)
  const viewShot = useRef<ViewShot | null>(null)

  const productList = props.products.map(dataItem =>
    <InsuranceProductListItem
      key={dataItem.FinancialServiceGroupId}
      iconType={'star'}
      title={dataItem.FinancialServiceTypeDesc}
      cost={dataItem.FinancialServiceTypeRate}
      signature={initials[dataItem.FinancialServiceGroupId]}
      onPress={() => setShowInitialModal(dataItem.FinancialServiceGroupId)}
    />
  )

  const total = props.products.reduce((sum, product) => sum + parseFloat(product.FinancialServiceTypeRate), 0.0)

  const hasSignature = signature.length > 0
  const hasAllInitials = Object.keys(initials).length > 0 && Object.keys(initials).length === props.products.length
  const hasAllInitialValues = hasAllInitials ? Object.values(initials).reduce((prev, curr) => prev && curr.length > 0, true) : false
  const saveAndSubmitEnabled = agreedToTerms && hasSignature && hasAllInitials && hasAllInitialValues

  const onSaveAndSubmit = () => {
    if (!viewShot.current) {
      return
    }

    const beneficiaryProducts: CreateFinancialServiceProductWithBeneficiaryRequest[] = props.products.map(product => ({
      AccountCode: props.accountCode.trim(),
      ActualSalePrice: parseFloat(product.FinancialServiceTypeRate),
      FinancialServiceGroupId: product.FinancialServiceGroupId,
      FinancialServiceTypeId: product.FinancialServiceTypeId,
      SalesChannel: "InstoreApp",
      BeneficiaryIndentityNumber: null,
      BeneficiaryFirstName: null,
      BeneficiaryLastName: null,
      IMEINumber: "",
      SaleReferenceNumber: "1234567",
      DeviceCoverProductDesc: "",
    }))

    createProducts(beneficiaryProducts)
      .then((response) => {
        if (response?.ResponseMessage.toLocaleLowerCase() !== "success") {
          console.log(response)
          throw Error('Couldn\'t create financial products')
        }

        return captureRef(viewShot, {
          format: "jpg",
          quality: 1.0,
          result: "base64",
        })
          .then((result) => {
            const sanitised = result.replace(/\r?\n|\r/g, "")

            const body: UploadInsuranceDocumentRequest = {
              CustomerCategoryId: props.customerCategoryId,
              CustomerId: props.customerId,
              DocumentTypeId: 3,
              ImageData: sanitised,
            }

            return uploadDocument(body)
              .then((response) => {
                if (response) {
                  props.onSubmitted()
                } else {
                  Alert.alert(
                    'Submission failed',
                    'Your submission did not go through successfully. Please try again.',
                    [{text: 'Okay'}]
                  )
                }
              })
          })
      })
      .catch((error) => {
        Alert.alert(
          'Submission failed',
          error,
          [{text: 'Okay'}]
        )
      })
  }

  const isLoading = isUploading || isCreatingProducts

  return (
    <View style={{backgroundColor: 'white', marginBottom: 100, marginTop: 10}}>
      <ViewShot style={styles.container} ref={viewShot}>
        <InsuranceWrap>

          <View style={{marginBottom: 28}}>
            <TextArea textAlign={"left"} withTitle={true} title={'Product Summary'} maxWidth={600}>
              Here's a recap of your insurance products and the cost. Please sign in the box and click "save and
              submit".
            </TextArea>
          </View>

          <View style={{marginBottom: 51}}>
            <DefaultCheckBox
              text={'I acknowledge that I have read, understood and accepted the Terms and Conditions'}
              isChecked={agreedToTerms}
              setChecked={() => setAgreedToTerms(terms => !terms)}/>
          </View>

          <View>
            <TopBar>
              <TopText hasRightPadding={false}>Insurance Product</TopText>
              <RightItemsWrap>
                <TopText hasRightPadding={true} style={{textAlign: 'right'}}>Cost</TopText>
                <TopText hasRightPadding={false} style={{textAlign: 'center'}}>Initial</TopText>
              </RightItemsWrap>
            </TopBar>

            <View>
              {productList}
            </View>

            <TotalBar>
              <TotalTopBorder/>
              <View style={{
                height: '100%',
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                paddingRight: 44,
                paddingLeft: 13,
              }}>
                <TotalText>
                  Total Cost
                </TotalText>
                <TotalText>
                  R {total.toFixed(2)}
                </TotalText>
              </View>
              <TotalBottomBorder/>
            </TotalBar>
          </View>

          <BottomItems>
            <SignatureWrap>
              <SignatureField
                onPress={() => setShowSignatureModal(true)}
                value={signature}
                errorText={null}
              />
            </SignatureWrap>
            <ButtonWrap>
              <DefaultButton
                buttonType={"default"}
                title={'save and submit'}
                disabled={!saveAndSubmitEnabled}
                onPress={onSaveAndSubmit}
                isLoading={isLoading}
              />
            </ButtonWrap>
          </BottomItems>

          <DisclaimerBox title={'Disclaimer'}>
            {disclaimerLoading &&
            <LoadingContainer>
                <ActivityIndicator size={"large"} color={"gray"}/>
            </LoadingContainer>}
            {!disclaimerLoading && disclaimer &&
            <Text>{disclaimer}</Text>}
          </DisclaimerBox>

          <DefaultModal
            visible={showSignatureModal}
            title={"Signature"}
            headerType={"small"}
            showBottomButtons={false}
            width={"720px"}
            height={"350px"}
          >
            <CaptureSignature
              onCancelButtonPressed={() => setShowSignatureModal(false)}
              onActionButtonPressed={(base64) => {
                const sanitised = base64.replace(/\r?\n|\r/g, "")
                setSignature(sanitised)
                setShowSignatureModal(false)
              }}
            />
          </DefaultModal>


          <DefaultModal
            visible={showInitialModal !== null}
            title={"Signature"}
            headerType={"small"}
            showBottomButtons={false}
            width={"720px"}
            height={"350px"}
          >
            <CaptureSignature
              onCancelButtonPressed={() => setShowInitialModal(null)}
              onActionButtonPressed={(base64) => {
                const sanitised = base64.replace(/\r?\n|\r/g, "")
                setInitials(current => {
                  const newInitials = {...current}
                  newInitials[showInitialModal!] = sanitised
                  return newInitials
                })
                setShowInitialModal(null)
              }}
            />
          </DefaultModal>
        </InsuranceWrap>
      </ViewShot>
    </View>
  )
}

const InsuranceProductListItem: React.FC<InsuranceProductListItemProps> = (props) => {
  let imageSource
  if (props.iconType === 'card') {
    imageSource = require('../../assets/icons/card.png')
  } else if (props.iconType === 'star') {
    imageSource = require('../../assets/icons/star.png')
  } else {
    imageSource = require('../../assets/icons/star.png')
  }

  const isSigned = props.signature !== undefined && props.signature !== ''

  return (
    <InsuranceListItem>
      <TopBorder/>
      <ContentWrap>
        <Icon>
          <Image source={imageSource} width={17} height={21}/>
        </Icon>
        <View>
          <ProductTitle>{props.title}</ProductTitle>
        </View>
      </ContentWrap>

      <View style={{ position: "absolute", right: 200, paddingRight: 40, display: "flex", justifyContent: "center", height: '100%' }}>
        <Cost>R{props.cost}</Cost>
      </View>

      <ButtonWrap>
        {!isSigned && <DefaultButton buttonType={'light'} title={'SIGN'} disabled={false} onPress={props.onPress}/>}
        {isSigned &&
          <TouchableOpacity onPress={props.onPress}>
              <SignatureArea source={{uri: `data:image/png;base64,${props.signature}`}}/>
          </TouchableOpacity>
        }
      </ButtonWrap>
    </InsuranceListItem>
  )
}

const InsuranceWrap = styled.View`
  padding: 0;
  width: 100%;
`

const SignatureWrap = styled.View`
  width: 324px;
`

const BottomItems = styled.View`
  flex-direction: row;
  margin-top: 52px;
  margin-bottom: 65px;
`

const TopBar = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  padding-bottom: 8px;
`

const TopText = styled.Text<{ hasRightPadding: boolean }>`
  font-weight: bold;
  font-size: 11px;
  letter-spacing: 0.4px;
  color: #9A9DA0;
  width: 200px;
  padding-right: ${props => props.hasRightPadding ? 50 : 0}px;
`

const RightItemsWrap = styled.View`
  flex-direction: row;
`

const InsuranceListItem = styled.View`
  height: 84px;
  width: 100%;
`

const ContentWrap = styled.View`
  height: 100%;
  flex-direction: row;
  align-items: center;
`

const Cost = styled.Text`
  font-weight: 500;
  font-size: 14px;
  letter-spacing: 0.25px;
  color: #353C43;
  text-align: right;
`

const ButtonWrap = styled.View`
  width: 200px;
  position: absolute;
  right: 0;
  top: 14px;
`

const SignatureArea = styled.Image`
  width: 200px;
  height: 56px;
  border: 1px solid #D1D2D4;
  border-radius: 4px;
`

const Icon = styled.View`
  width: 31px;
  height: 31px;
  border: 1px solid #ECECEE;
  border-radius: 16px;
  margin-right: 10px;
  align-items: center;
  justify-content: center;
`

const ProductTitle = styled.Text`
  font-weight: 500;
  font-size: 14px;
  letter-spacing: 0.25px;
  color: #353C43;
`

const TopBorder = styled.View`
  width: 100%;
  height: 1px;
  background-color: #ECECEE;
`

const TotalTopBorder = styled.View`
  width: 100%;
  height: 1px;
  background-color: #D1D2D4;
`

const TotalBottomBorder = styled(TotalTopBorder)`
  position: absolute;
  bottom: 0;
`

const TotalBar = styled.View`
  height: 53px;
  background: #F9F9FB;
`

const TotalText = styled.Text`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  letter-spacing: 0.25px;
  color: #353C43;
`

const styles = StyleSheet.create({
  container: {
    width: '100%',
    paddingBottom: 100,
    backgroundColor: 'white',
    padding: 20,
  },
})
