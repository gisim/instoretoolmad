import React from 'react';
import styled from 'styled-components/native';
import {Image} from 'react-native';

interface IconCellProps {
  size: string;
  type: string;
}

interface IconProps {
  type: string;
}

export const IconCell: React.FC<IconCellProps> = (props) => {
  return (
    <>
      {props.size === 'large' && (
        <StyledIconCellLarge>
          <Icon type={props.type} />
        </StyledIconCellLarge>
      )}
      {props.size === 'small' && (
        <StyledIconCellSmall>
          <Icon type={props.type} />
        </StyledIconCellSmall>
      )}
    </>
  );
};

const Icon: React.FC<IconProps> = (props) => {
  return (
    <>
      {props.type === 'activation' && (
        <StyledIconImage
          source={require('../../assets/icons/accountActivation.png')}
        />
      )}
      {props.type === 'application' && (
        <StyledIconImage
          source={require('../../assets/icons/accountApplication.png')}
        />
      )}
      {props.type === 'allApplication' && (
        <StyledIconImage
          source={require('../../assets/icons/allApplications.png')}
        />
      )}
      {props.type === 'insurance' && (
        <StyledIconImage source={require('../../assets/icons/insurance.png')} />
      )}
      {props.type === 'mrpMobile' && (
        <StyledIconImage source={require('../../assets/icons/mrpMobile.png')} />
      )}
    </>
  );
};

const StyledIconCellLarge = styled.View`
  width: 148px;
  height: 148px;
  border-radius: 4px;
  overflow: hidden;
  background-color: #d71920;
`;

const StyledIconCellSmall = styled(StyledIconCellLarge)`
  width: 28px;
  height: 28px;
`;

const StyledIconImage = styled.Image`
  width: 100%;
  height: 100%;
`;
