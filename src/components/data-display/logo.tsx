import React from 'react';
import styled from 'styled-components/native';

interface LogoProps {
  type: string;
}

export const Logo: React.FC<LogoProps> = (props) => {
  return (
    <>
      {props.type === 'large' && (
        <StyledLogo source={require('../../assets/logo/logo-long.png')} />
      )}
      {props.type === 'small' && (
        <StyledLogo source={require('../../assets/logo/logo-small.png')} />
      )}
    </>
  );
};

const StyledLogo = styled.Image``;
