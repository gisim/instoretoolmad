import React, {useEffect, useMemo, useRef, useState} from 'react'
import styled from 'styled-components/native'
import {StackNavigationProp} from "@react-navigation/stack"
import {MainStackParamList} from "../../../App"
import {RouteProp} from "@react-navigation/native"
import {PageTitle} from "./page-title"
import {StepsSubNav} from "../navigation/steps-sub-nav"
import {ActivityIndicator, KeyboardAvoidingView, Platform, Alert} from "react-native"
import {ActivateCardStep1} from "./activate-card-step-1"
import {useMutation} from "react-query"
import {
  getLinkCardDataMutation,
  getPreAgreementDataMutation,
  processLinkAccountMutation
} from "../../services/api-service"
import {ProcessLinkAccountRequest} from "../../models/process-link-account-request"
import {GetPreAgreementDataRequest} from "../../models/get-pre-agreement-data-request"
import {AppStorageService} from "../../services/app-storage-service"
import {CreditPreAgreement} from "./credit-pre-agreement"
import {GetPreAgreementDataResponse} from "../../models/get-pre-agreement-data-response"
import {FormErrors} from "../shared"

export type ActivateCardScreenNavigationProps = StackNavigationProp<MainStackParamList, 'ActivateCard'>
export type ActivateCardScreenRouteProps = RouteProp<MainStackParamList, 'ActivateCard'>

interface ActivateCardProps {
  navigation: ActivateCardScreenNavigationProps
  route: ActivateCardScreenRouteProps
}

export const ActivateCard: React.FC<ActivateCardProps> = (props) => {
  const {mustUploadDocuments, idNumber, commQueueId} = props.route.params
  const [currentStep, setCurrentStep] = useState(1)
  const [getLinkCardData, { isLoading: getLinkCardLoading }] = useMutation(getLinkCardDataMutation)
  const [processLinkAccount, { isLoading: processLinkAccountLoading }] = useMutation(processLinkAccountMutation)
  const [getPreAgreementData, { isLoading: getPreAgreementLoading }] = useMutation(getPreAgreementDataMutation)
  const storeCardNumber = useRef('2011271229')
  const preAgreement = useRef<GetPreAgreementDataResponse|null>(null)
  const [hasUploadedDocument, setHasUploadedDocument] = useState<boolean|null>(null)
  const [errors, setErrors] = useState<FormErrors>({})
  const [preAgreementEmail, setPreAgreementEmail] = useState<string|null>(null)

  const isLoading = getLinkCardLoading || processLinkAccountLoading || getPreAgreementLoading

  useEffect(() => {
    if (hasUploadedDocument === null) { return }
    checkForErrors()
  }, [hasUploadedDocument])

  const nextStep = () => {
    switch (currentStep) {
      case 1: {
        step1save()
        break
      }
    }
  }

  const prevStep = () => {
    setCurrentStep(cs => --cs)
  }

  const checkForErrors = () => {
    const newErrors: FormErrors = {}

    if (!validateStoreCard()) {
      newErrors.StoreCard = 'Please enter a valid store card number'
    }

    if (!hasUploadedDocument) {
      newErrors.UploadDocument = 'Please upload your Identity Document first'
    }

    setErrors(newErrors)

    return Object.keys(newErrors).length > 0
  }

  const step1save = () => {
    if (checkForErrors()) { return }

    const linkCard = async () => {
      try {
        const linkCardData = await getLinkCardData({idNumber})
        if (!linkCardData) {
          return
        }

        const {StoreId, StoreDesc, DivisionDesc, CreditLimit, ...remainingLinkData} = linkCardData
        const requestData = remainingLinkData as ProcessLinkAccountRequest
        requestData.AccountCode = storeCardNumber.current

        const linkAccount = await processLinkAccount(requestData)
        if (linkAccount?.ResponseStatus !== 0) {
          throw (linkAccount?.ResponseMessage ? `[${linkAccount.ResponseMessage}]` : '')
        }

        if (linkAccount?.CustomerId === 0) {
          throw `[An invalid response was received]`
        }

        const user = await AppStorageService.getUser()
        const staffCode = await AppStorageService.getActiveStaffCode()

        if (!linkAccount) {
          return
        }
        const username = user.UserName.slice(0, 5)

        const request: GetPreAgreementDataRequest = {
          StoreCode: parseInt(linkCardData.StoreCode),
          UserCode: username,
          UserName: username,
          StaffId: parseInt(staffCode!),
          TillId: 1,
          CountryId: linkAccount.CountryId,
          DivisionId: linkAccount.DivisionId,
          ReferenceNumber: linkCardData.eReferenceCode,
          IdentityNumber: idNumber,
        }

        const preAgreementData = await getPreAgreementData(request)
        if (preAgreementData) {
          preAgreement.current = preAgreementData
          setPreAgreementEmail(preAgreementData.preAgreementInformation.EmailAddress ?? null)
          setCurrentStep(cs => ++cs)
        }
      } catch (e) {
        Alert.alert('Error Linking Card', `An error occurred while linking your card. Please verify your card number and try again.\n\n${e}`, [{
          style: "default", text: "Okay",
        }])
        console.log(e)
      }
    }

    linkCard()
  }

  useEffect(() => {
    if (mustUploadDocuments) { return }
    setHasUploadedDocument(true)
  }, [mustUploadDocuments])

  const validateStoreCard = () => {
    if (storeCardNumber.current.length === 0) { return false }
    return !(!/^([\d.]+)$/.test(storeCardNumber.current))
  }

  const header = useMemo(() => {
    let title: string|null = null
    let description: string|null = null

    switch (currentStep) {
      case 2: {
        title = 'Credit Facility Pre-Agreement'
        description = 'Please review and confirm that the information presented below is correct.'
        break
      }
      default:
        break
    }

      return (
        <StepsSubNav
          currentStep={currentStep}
          totalSteps={2}
          title={title}
          description={description}
          onNext={nextStep}
          onBack={prevStep}
          canGoBack={false}
          canGoNext={currentStep === 1}
        />
      )
  }, [currentStep, preAgreementEmail, hasUploadedDocument, errors])

  const stepComponent = useMemo(() => {
    switch (currentStep) {
      case 1: {
        return (
          <ActivateCardStep1
            commQueueId={commQueueId}
            mustUploadDocuments={mustUploadDocuments}
            onStoreCardInput={(input) => {
              storeCardNumber.current = input
            }}
            onDocumentUploaded={() => {
              setHasUploadedDocument(true)
            }}
            storeCardError={errors.StoreCard}
            documentUploadError={hasUploadedDocument ? null : errors.UploadDocument}
            hasUploadedDocument={hasUploadedDocument ?? false}
          />
        )
      }
      case 2: {
        return (
          <CreditPreAgreement
            response={preAgreement.current!}
            commQueueId={commQueueId}
            onApplicationSubmitted={() => {
              props.navigation.navigate('ActivateCardComplete')
            }}
          />
        )
      }
    }
  }, [currentStep, errors, hasUploadedDocument])

  return (
    <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} keyboardVerticalOffset={70}>
      <Container>
        <PageTitle title={'Account Activation'} titleType={'largeRed'} textAlign={'left'} subtitle={''} />
        {header}
        {isLoading && (
          <LoadingContainer>
            <ActivityIndicator animating={isLoading} color={'gray'} />
          </LoadingContainer>
        )}
        {!isLoading && stepComponent}
      </Container>
    </KeyboardAvoidingView>
  )
}

const Container = styled.ScrollView`
  padding: 0px 21px 21px 21px;
`

const LoadingContainer = styled.View`
  display: flex;
  align-items: center;  
  justify-content: center;
`
