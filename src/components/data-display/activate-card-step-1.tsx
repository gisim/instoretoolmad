import React, {useMemo, useRef, useState} from 'react'
import {DefaultTextInput} from "../data-capture/default-text-input"
import styled from 'styled-components/native'
import {DefaultButton} from "../data-capture/default-button"
import {StyleSheet, View} from 'react-native'
import {DefaultModal} from "../modal/default-modal"
import {BarCodeReadEvent, RNCamera} from "react-native-camera"
import {PageTitle} from "./page-title"
import Markdown from "react-native-markdown-renderer"
import {ReadOnlyField} from "../data-capture/read-only-field"
import {Camera} from "../data-capture/camera"
import {useMutation} from "react-query"
import {uploadDocumentMutation} from "../../services/api-service"
import {ColumnFill, ColumnWidth} from "../forms/column"

interface ActivateCardStep1Props {
  onStoreCardInput: (input: string) => void
  onDocumentUploaded: () => void
  storeCardError: string|null|undefined
  documentUploadError: string|null|undefined
  mustUploadDocuments: boolean
  commQueueId: number
  hasUploadedDocument: boolean
}

export const ActivateCardStep1: React.FC<ActivateCardStep1Props> = (props) => {
  const [uploadDocument, { isLoading: isUploading }] = useMutation(uploadDocumentMutation)
  const [storeCardNumber, setStoreCardNumber] = useState('')
  const [isShowingScanner, setIsShowingScanner] = useState(false)
  const [isShowingCamera, setIsShowingCamera] = useState(false)
  const [isUploadComplete, setIsUploadComplete] = useState(false)
  const camera = useRef<RNCamera|null>(null)
  const {mustUploadDocuments, documentUploadError, hasUploadedDocument} = props

  const onScanPressed = () => {
    setIsShowingScanner(true)
  }

  const onBarcodeRead = (event: BarCodeReadEvent) => {
    if (!isShowingScanner) { return }
    if (event.data.length === 0) { return }
    const cardNumber = event.data.slice(0, 10)
    setIdentityNumber(cardNumber)
  }

  const setIdentityNumber = (cardNumber: string) => {
    setStoreCardNumber(cardNumber)
    props.onStoreCardInput(cardNumber)
    setIsShowingScanner(false)
  }

  const uploadIdentityDocument = (base64: string) => {
    const now = new Date()
    const body = {
      "CommunicationQueueId": props.commQueueId,
      "SupportingDocumentType": 5,
      "FileType": "image/jpeg",
      "FileBytes": `data:image/jpg;base64,${base64}`,
      "IsActive": true,
      "DateUploaded": now.toISOString(),
      "UploadedBy": "C3PO",
    }

    uploadDocument({ body })
      .then((response) => {
        if (response) {
          props.onDocumentUploaded()
        } else {
          console.log('ID upload failed')
        }
      })
  }

  const uploadComponent = useMemo(() => {
    if (!mustUploadDocuments) { return null }

    const title = isUploadComplete
      ? (isUploading ? "Uploading…" : "Identity Document has successfully been uploaded")
      : (hasUploadedDocument ? "Identity Document has successfully been uploaded" : "Identity Document not uploaded")

    const buttonTitle = isUploadComplete ? "Re-upload" : "Upload"

    return (
      <View style={{ marginBottom: 45 }}>
        <PageTitle titleType={'pageTitle'} title={'Customer Validation'} textAlign={'left'}/>
        <TextWrap>
          <Markdown>Before we can get started, we need to confirm who you are. Please upload your Identity Document (ID).</Markdown>
        </TextWrap>
        <Row>
          <ColumnFill top={0}>
            <ReadOnlyField title={title} value={""} onPressed={() => setIsShowingCamera(true)} errorText={documentUploadError} />
          </ColumnFill>
          <ColumnWidth width={200} top={4}>
            <DefaultButton buttonType={"default"} title={buttonTitle} isLoading={isUploading} disabled={isUploading} onPress={() => setIsShowingCamera(true)} />
          </ColumnWidth>
        </Row>
      </View>
    )
  }, [mustUploadDocuments, isUploading, documentUploadError])

  return (
    <>
      {uploadComponent}

      <PageTitle titleType={'pageTitle'} title={'Link Account to Card'} textAlign={'left'}/>
      <TextWrap>
        <Markdown>Please scan the barcode of the plastic account card being presented to the customer.</Markdown>
      </TextWrap>

      <Row>
        <ColumnFill top={0}>
          <DefaultTextInput
            type={"number"}
            placeholder={"Store Card Number"}
            value={storeCardNumber}
            setValue={setIdentityNumber}
            errorText={props.storeCardError}
          />
        </ColumnFill>
        <ColumnWidth width={200} top={4}>
          <DefaultButton buttonType={"default"} title={"Scan"} disabled={false} onPress={onScanPressed} />
        </ColumnWidth>
      </Row>

      <DefaultModal
        visible={isShowingScanner}
        title={'Scan Barcode'}
        headerType={'small'}
        showBottomButtons={true}
        height={'300px'}
        width={'400px'}
        cancelButtonTitle={'Close'}
        onCancelButtonPressed={() => setIsShowingScanner(false)}
      >
        {isShowingScanner &&
          <View style={styles.container}>
            <RNCamera
                ref={camera}
                type={"back"}
                style={styles.preview}
                onBarCodeRead={(event) => {
                  camera.current?.pausePreview()
                  onBarcodeRead(event)
                }}
                captureAudio={false}
                androidCameraPermissionOptions={{
                  title: 'Permission to use camera',
                  message: 'We need your permission to use your camera',
                  buttonPositive: 'Okay',
                  buttonNegative: 'Cancel',
                }}
            />
          </View>
        }
      </DefaultModal>

      <DefaultModal
        visible={isShowingCamera}
        title={'Upload Identity Document'}
        headerType={"small"}
        showBottomButtons={true}
      >
        <Camera
          onCancel={() => setIsShowingCamera(false)}
          onUploadComplete={(base64) => {
            uploadIdentityDocument(base64)
            setIsUploadComplete(true)
            setIsShowingCamera(false)
          }}
        />
      </DefaultModal>
    </>
  )
}

const Row = styled.View`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;  
`

const TextWrap = styled.View`
  margin-top: 16px;
`


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    overflow: 'hidden',
    height: 50,
  },
})

