import React from 'react'
import styled from 'styled-components/native'
import {View} from "react-native"

interface PageTitleProps {
  title: string|undefined
  subtitle?: string
  titleType: "pageTitle" | "cellTitle" | "largeRed" | "large"
  textAlign: "auto" | "left" | "center" | "right"
}

export const PageTitle: React.FC<PageTitleProps> = (props) => {
  return (
    <>
      {props.titleType === 'pageTitle' && (
        <>
          <StyledPageTitleContent style={{textAlign: props.textAlign}}>
            {props.title}
          </StyledPageTitleContent>
          {props.subtitle &&
              <View style={{marginTop: 10}}>
                <StyledPageSubtitleContent style={{textAlign: props.textAlign}}>
                  {props.subtitle}
                </StyledPageSubtitleContent>
              </View>
          }
        </>
      )}
      {props.titleType === 'cellTitle' && (
        <StyledCellTitleContent style={{textAlign: props.textAlign}}>
          {props.title}
        </StyledCellTitleContent>
      )}
      {props.titleType === 'largeRed' && (
        <StyledLargeRedTitleContent style={{textAlign: props.textAlign, marginTop: 36}}>
          {props.title}
        </StyledLargeRedTitleContent>
      )}
      {props.titleType === 'large' && (
        <StyledLargeTitleContent style={{textAlign: props.textAlign}}>
          {props.title}
        </StyledLargeTitleContent>
      )}
    </>
  )
}

const StyledPageTitleContent = styled.Text`
  font-weight: bold;
  font-size: 27px;
  line-height: 32px;
  display: flex;
  align-items: center;
  text-align: center;
  letter-spacing: 0.5px;
  color: #353c43;
  text-transform: capitalize;
`

const StyledPageSubtitleContent = styled.Text`
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
  display: flex;
  align-items: center;
  text-align: center;
  letter-spacing: 0.4444px;
  color: #353c43;
`

const StyledCellTitleContent = styled(StyledPageTitleContent)`
  font-weight: 600;
  font-size: 20px;
  line-height: 28px;
  margin-bottom: 28px;
`

const StyledLargeRedTitleContent = styled(StyledPageTitleContent)`
  font-weight: bold;
  font-size: 42px;
  line-height: 52px;
  letter-spacing: 0.5px;
  color: #d71920;
`

const StyledLargeTitleContent = styled(StyledPageTitleContent)`
  font-weight: bold;
  font-size: 42px;
  line-height: 52px;
  letter-spacing: 0.5px;
  color: #353C43;
`
