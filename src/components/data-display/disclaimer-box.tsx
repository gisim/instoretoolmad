import React from 'react'
import styled from 'styled-components/native'

interface DisclaimerBoxProps {
  title: string
}

export const DisclaimerBox: React.FC<DisclaimerBoxProps> = (props) => {
  return (
    <StyledDisclaimerBox>
      <StyledDisclaimerBoxTopBorder/>
      <StyledDisclaimerTitle>
        {props.title}
      </StyledDisclaimerTitle>
      <StyledScrollArea>
        <StyledDisclaimerText>
          {props.children}
        </StyledDisclaimerText>
      </StyledScrollArea>
    </StyledDisclaimerBox>
  )
}

const StyledDisclaimerBox = styled.View`
  width: 100%;
  background-color: #F9F9FB;
  padding: 0 76px 60px 65px;
`

const StyledDisclaimerBoxTopBorder = styled.View`
  width: 100%;
  height: 1px;
  background-color: #ECECEE;
`

const StyledDisclaimerTitle = styled.Text`
  font-weight: bold;
  font-size: 16px;
  letter-spacing: 0.444444px;
  color: #353C43;
  padding: 20px 0 10px 0;
`

const StyledScrollArea = styled.View`
`

const StyledDisclaimerText = styled.Text`
  font-size: 12px;
  line-height: 16px;
  letter-spacing: 0.4px;
  color: #6E6E77;
  margin-top: 10px;
  padding: 0 12px 40px 0;
`
