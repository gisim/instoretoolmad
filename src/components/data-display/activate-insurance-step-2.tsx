import React, {useEffect, useMemo, useState} from 'react'
import {PageTitle} from "./page-title"
import styled from "styled-components/native"
import {useQuery} from "react-query"
import {getExistingInsuranceProductsQuery, getInsuranceProductsQuery} from "../../services/api-service"
import {ActivityIndicator} from "react-native"
import {LoadingContainer} from '../shared'
import {ExpandingContentCell} from "./expanding-content-cell"
import {DefaultVerticalRadioGroup} from "../data-capture/default-vertical-radio-group"
import {FinancialServiceType} from "../../models/get-insurance-products-response"

export type SelectedInsuranceProducts = { [key: number]: FinancialServiceType }

interface ActivateInsuranceStep2Props {
  divisionId: number
  storeCardNumber: string
  onSelectedProducts: (products: SelectedInsuranceProducts) => void
  selectedProducts: SelectedInsuranceProducts
}

export const ActivateInsuranceStep2: React.FC<ActivateInsuranceStep2Props> = (props) => {
  const {data, isLoading} = useQuery(['insurance-products', props.storeCardNumber, props.divisionId], () => getInsuranceProductsQuery({
    AccountCode: props.storeCardNumber,
    DivisionId: props.divisionId,
    CommunicationTypeId: 8,
  }))

  const {data: existingProducts} = useQuery(['existing-insurance-products', props.storeCardNumber, props.divisionId], () => getExistingInsuranceProductsQuery({
    accountCode: props.storeCardNumber,
  }))

  const [selectedProducts, setSelectedProducts] = useState<SelectedInsuranceProducts>(props.selectedProducts)

  useEffect(() => {
    props.onSelectedProducts(selectedProducts)
  }, [selectedProducts])

  const selectProduct = (product: FinancialServiceType) => {
    setSelectedProducts(prevState => {
      if (Object.keys(prevState).includes(`${product.FinancialServiceGroupId}`)) {
        if (prevState[product.FinancialServiceGroupId].FinancialServiceTypeId === product.FinancialServiceTypeId) {
          delete prevState[product.FinancialServiceGroupId]
          return prevState
        }
      }
      const updatedValues: { [key: number]: FinancialServiceType } = {}
      updatedValues[product.FinancialServiceGroupId] = product
      return {...prevState, ...updatedValues}
    })
  }

  const hasEligableProducts = isLoading ? true : (data?.FinancialServiceGroup?.length ?? 0) > 0

  const components = useMemo(() => {
    if (!data) { return null }
    if (!existingProducts) { return null }

    const existingFinancialGroups = existingProducts.map(ep => ep.FinancialServiceGroupId)

    return data.FinancialServiceGroup
      .filter(product => !existingFinancialGroups.includes(product.FinancialServiceGroupId))
      .map(product => {
        let selectedValue = ''
        if (selectedProducts[product.FinancialServiceGroupId] !== undefined) {
          selectedValue = `${selectedProducts[product.FinancialServiceGroupId].FinancialServiceTypeId}`
        }

        return (
          <ExpandingContentCell
            key={product.FinancialServiceGroupId}
            title={product.FinancialServiceGroupDesc}
            subTitle={''}
            description={stripHtml(product.BenefitsWording)}
            iconType={'card'}
          >
            <DefaultVerticalRadioGroup
              title={'Select a Plan'}
              rightTitle={'Premium'}
              radioList={product.FinancialServiceType.map(o => ({
                id: `${o.FinancialServiceTypeId}`,
                checked: false,
                label: o.FinancialServiceTypeDesc,
                hasRightLabel: true,
                rightLabel: `R ${o.FinancialServiceTypeRate}`,
              })) ?? []}
              radioListLookUp={undefined}
              value={selectedValue}
              canUncheck={true}
              onChange={(group) => {
                const selected = product.FinancialServiceType.filter(p => `${p.FinancialServiceTypeId}` === group.id)
                selectProduct({ ...selected[0]!, GroupName: product.FinancialServiceGroupDesc})
              }}
            />
          </ExpandingContentCell>
        )
      })
  }, [data, existingProducts])

  return (
    <>
      <PageTitle titleType={'pageTitle'} title={'Products'} textAlign={'left'}/>
      <TextWrap>
        Mr Price Insurance offers a range of products. For more info on each product, press the arrow tabs below.
      </TextWrap>
      {isLoading && (
        <LoadingContainer style={{marginTop: 50}}>
          <ActivityIndicator animating={isLoading} color={'gray'}/>
        </LoadingContainer>
      )}
      <Container>
        {components}
        {!isLoading && data && !hasEligableProducts &&
          <TextWrap style={{ fontWeight: 'bold' }}>⚠️ Your store account has no eligible products. Please speak to a sales associate for more information.</TextWrap>}
      </Container>
    </>
  )
}

const stripHtml = (html: string) => {
  return html.replace(/<br>/gi, '\n')
}

const TextWrap = styled.Text`
  margin-top: 16px;
  color: #6E6E77;
`

const Container = styled.View`
  margin-top: 42px;
`
