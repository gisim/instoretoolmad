import React from 'react'
import styled from 'styled-components/native'

interface TextAreaProps {
  textAlign: "auto" | "left" | "center" | "right"
  maxWidth?: number
  withTitle: boolean
  title?: string
}

export const TextArea: React.FC<TextAreaProps> = (props) => {
  let maxWidth
  if (props.maxWidth !== undefined && props.maxWidth > 0) {
    maxWidth = props.maxWidth
  } else {
    maxWidth = 'auto'
  }
  return (
    <>
      {props.withTitle && (
        <>
          <StyledTextAreaTitle style={{maxWidth: maxWidth}}>{props.title}</StyledTextAreaTitle>
          <StyledDefaultTextArea
            style={{textAlign: props.textAlign, maxWidth: maxWidth}}>
            {props.children}
          </StyledDefaultTextArea>
        </>
      )}
      {!props.withTitle && (
        <StyledDefaultTextArea
          style={{textAlign: props.textAlign, maxWidth: maxWidth}}>
          {props.children}
        </StyledDefaultTextArea>
      )}
    </>
  )
}

const StyledDefaultTextArea = styled.Text`
  font-size: 14px;
  line-height: 20px;
  letter-spacing: 0.25px;
  color: #6e6e77;
`

const StyledTextAreaTitle = styled.Text`
  font-weight: 600;
  font-size: 16px;
  letter-spacing: 0.444444px;
  color: #6e6e77;
  margin-bottom: 6px;
  width: 100%;
`
