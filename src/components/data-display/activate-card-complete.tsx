import React, {useLayoutEffect} from 'react'
import {Image, View} from "react-native"
import styled from "styled-components/native"
import {DefaultButton} from "../data-capture/default-button"
import {ActivateCardScreenNavigationProps} from "./activate-card"
import {StackNavigationProp} from "@react-navigation/stack"
import {MainStackParamList} from "../../../App"
import {TopNav} from "../navigation/top-nav"

export type ActivateCardCompleteScreenNavigationProps = StackNavigationProp<MainStackParamList, 'ActivateCardComplete'>

interface ActivateCardCompleteProps {
  navigation: ActivateCardScreenNavigationProps
}

export const ActivateCardComplete: React.FC<ActivateCardCompleteProps> = (props) => {
  const {navigation} = props

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: "Activation Complete",
    })
  }, [navigation])

  return (
    <>
      <View style={{ paddingHorizontal: 21 }}>
        <TopNav />
      </View>
      <Container>
        <Image
          source={require("../../assets/icons/formSuccess.png")}
          style={{width: 64, height: 64}} width={64}
          height={64}
        />
        <StatusMessage>Congratulations!</StatusMessage>
        <Header>Your account is now active</Header>
        <BottomView>
            <Title>Get the most out of your account!</Title>
            <Description>{'Enjoy loads of perks when you shop on your\nMr Price Money account.'}</Description>
        </BottomView>
        <UpsellView>
          <Image source={require('../../assets/upsell/activations-final-1.png')} width={327} height={444} />
          <Image source={require('../../assets/upsell/activations-final-2.png')} width={324} height={444} style={{ marginTop: 20, marginLeft: 15 }} />
        </UpsellView>

        <BottomAnchorView>
          <DefaultButton
            buttonType={'default'}
            title={'Take Me Back'}
            disabled={false}
            onPress={() => {
              navigation.navigate('Dashboard')
            }}
          />
        </BottomAnchorView>
      </Container>
    </>
  )
}

const Container = styled.View`
  display: flex;
  align-items: center;
  justify-content: center; 
  padding-top: 52px;
`

const StatusMessage = styled.Text`
  margin-top: 24px;
  margin-bottom: 8px;
  font-weight: 600;
  font-size: 20px;
  line-height: 28px;
  letter-spacing: 0.5px;
  color: #8BC34A;
`

const BottomView = styled.View`
  margin-top: 32px;
  padding-top: 28px;
  border-top-width: 2px;
  border-top-color: #f2f2f2;
  width: 50%;
  align-items: center;
  justify-content: center;
`

const Title = styled.Text`
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  letter-spacing: 0.25px;
  color: #353C43;
  margin-bottom: 12px;
`

const Description = styled.Text`
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  text-align: center;
  letter-spacing: 0.25px;
  color: #353C43;
`

const BottomAnchorView = styled.View`
  margin-top: 40px;
`

const Header = styled.Text`
  font-weight: 500;
  font-size: 14px;
  line-height: 24px;
  text-align: center;
  letter-spacing: 1.35px;
  text-transform: uppercase;
  color: #353C43;
`

const UpsellView = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-top: 30px;
`
