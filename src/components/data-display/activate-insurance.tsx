import React, {useMemo, useState} from 'react'
import {ActivityIndicator, Alert, KeyboardAvoidingView, Platform} from "react-native"
import {PageTitle} from "./page-title"
import {StepsSubNav} from "../navigation/steps-sub-nav"
import {ActivateInsuranceStep1} from "./activate-insurance-step-1"
import {ActivateInsuranceStep2, SelectedInsuranceProducts} from "./activate-insurance-step-2"
import { ActivateInsuranceStep3 } from './activate-insurance-step-3'
import {ActivateInsuranceStep4} from "./activate-insurance-step-4"
import {Container, FormErrors, LoadingContainer} from "../shared"
import {useMutation} from "react-query"
import { getInsuranceAccountQuery } from '../../services/api-service'
import {AccountDTO} from "../../models/insurance-account"
import {StackNavigationProp} from "@react-navigation/stack"
import {MainStackParamList} from "../../../App"
import {RouteProp} from "@react-navigation/native"

export type ActivateInsuranceScreenNavigationProps = StackNavigationProp<MainStackParamList, 'ActivateInsurance'>
export type ActivateInsuranceScreenRouteProps = RouteProp<MainStackParamList, 'ActivateInsurance'>

interface ActivateInsuranceProps {
  navigation: ActivateInsuranceScreenNavigationProps
  route: ActivateInsuranceScreenRouteProps
}

export const ActivateInsurance: React.FC<ActivateInsuranceProps> = (props) => {
  const totalSteps = 4
  const [currentStep, setCurrentStep] = useState(1)
  const [errors, setErrors] = useState<FormErrors>({})
  const [storeCardNumber, setStoreCardNumber] = useState('')
  const [insuranceAccount, setInsuranceAccount] = useState<AccountDTO|null>(null)
  const [hasUploadedDocument, setHasUploadedDocument] = useState(false)
  const [uploadedDocumentType, setUploadedDocumentType] = useState<number|null>(null)
  const [getInsuranceAccount, { isLoading: insuranceAccountLoading }] = useMutation(getInsuranceAccountQuery)
  const [selectedInsuranceProducts, setSelectedInsuranceProducts] = useState<SelectedInsuranceProducts>({})

  const isLoading = insuranceAccountLoading

  const nextStep = () => {
    switch (currentStep) {
      case 1: {
        getStoreAccount()
        break
      }
      case 2: {
        if (Object.keys(selectedInsuranceProducts).length === 0) {
          Alert.alert('Error', 'Please choose at least one insurance product to continue.', [{ text: 'Okay' }])
        } else {
          setCurrentStep(cs => ++cs)
        }
        break
      }
      case 3: {
        if (!hasUploadedDocument) {
          Alert.alert('Error', 'Please upload your documents to continue.', [{ text: 'Okay' }])
        } else {
          setCurrentStep(cs => ++cs)
        }
      }
    }
  }

  const prevStep = () => {
    setCurrentStep(cs => --cs)
  }

  const getStoreAccount = async () => {
    if (storeCardNumber.length === 0) {
      const newErrors: FormErrors = {
        StoreCard: 'Please enter a valid store card',
      }
      setErrors(newErrors)
      return
    }

    const insuranceAccountResponse = await getInsuranceAccount({ storeCard: storeCardNumber })
    if (insuranceAccountResponse?.AccountDTO === null) {
      const newErrors: FormErrors = {
        StoreCard: insuranceAccountResponse.ResponseMessage,
      }
      setErrors(newErrors)
      return
    } else {
      setErrors({})
      if (insuranceAccountResponse?.AccountDTO) {
        setInsuranceAccount(insuranceAccountResponse?.AccountDTO)
        setCurrentStep(cs => ++cs)
      } else {
        const newErrors: FormErrors = {
          StoreCard: 'No Division Found',
        }
        setErrors(newErrors)
      }
    }
  }

  const submissionComplete = () => {
    props.navigation.navigate('ActivateInsuranceComplete')
  }

  const stepComponent = useMemo(() => {
    switch (currentStep) {
      case 1: {
        return (
          <ActivateInsuranceStep1
            onStoreCardInput={(input) => {
              setStoreCardNumber(input)
            }}
            storeCardError={errors.StoreCard}
            isLoading={isLoading}
            cardNumber={storeCardNumber}
          />
        )
      }
      case 2: {
        return (
          <ActivateInsuranceStep2
            divisionId={insuranceAccount!.DivisionId}
            storeCardNumber={insuranceAccount!.AccountCode.trim()}
            onSelectedProducts={setSelectedInsuranceProducts}
            selectedProducts={selectedInsuranceProducts}
          />
        )
      }
      case 3: {
        return (
          <ActivateInsuranceStep3
            hasUploadedDocument={hasUploadedDocument}
            documentUploadError={null}
            onDocumentUploaded={(documentType) => {
              setUploadedDocumentType(documentType)
              setHasUploadedDocument(true)
            }}
            customerCategoryId={insuranceAccount!.CustomerCategoryId}
            customerId={insuranceAccount!.CustomerId}
            idType={uploadedDocumentType}
          />
        )
      }
      case 4: {
        return (
          <ActivateInsuranceStep4
            products={Object.values(selectedInsuranceProducts)}
            onSubmitted={submissionComplete}
            customerCategoryId={insuranceAccount!.CustomerCategoryId}
            customerId={insuranceAccount!.CustomerId}
            accountCode={insuranceAccount!.AccountCode}
          />
        )
      }
    }
  }, [currentStep, errors])

  return (
    <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} keyboardVerticalOffset={70}>
      <Container>
        <PageTitle title={'Insurance'} titleType={'largeRed'} textAlign={'left'} subtitle={''}/>
        <StepsSubNav
          currentStep={currentStep}
          totalSteps={totalSteps}
          title={null}
          description={null}
          onNext={nextStep}
          onBack={prevStep}
          canGoBack={currentStep !== 1}
          canGoNext={currentStep < totalSteps}
        />
        {isLoading && (
          <LoadingContainer>
            <ActivityIndicator animating={isLoading} color={'gray'}/>
          </LoadingContainer>
        )}
        {stepComponent}
      </Container>
    </KeyboardAvoidingView>
  )
}
