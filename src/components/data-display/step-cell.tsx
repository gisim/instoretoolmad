import React from 'react';
import styled from 'styled-components/native';
import {View} from 'react-native';

import {IconCell} from './icon-cell';

interface StepCellProps {
  iconSize: string;
  iconType: string;
  title: string;
  subText: string;
}

export const StepCell: React.FC<StepCellProps> = (props) => {
  return (
    <StyledStepCell>
      <IconCell size={props.iconSize} type={props.iconType} />
      <View style={{marginLeft: 22}}>
        <StyledStepCellTitle>{props.title}</StyledStepCellTitle>
        <StyledStepSubText>{props.subText}</StyledStepSubText>
      </View>
    </StyledStepCell>
  );
};

const StyledStepCell = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 22px;
`;

const StyledStepCellTitle = styled.Text`
  font-weight: 600;
  font-size: 16px;
  letter-spacing: 0.444444px;
  color: #353c43;
  text-transform: capitalize;
`;

const StyledStepSubText = styled(StyledStepCellTitle)`
  font-size: 14px;
  line-height: 20px;
  letter-spacing: 0.25px;
  color: #6e6e77;
  margin-top: 8px;
  text-transform: none;
`;

const StyledAllApplicationCell = styled.View``;
