import React, {useState} from 'react'
import styled from 'styled-components/native'
import {StyleProp, StyleSheet, View, ViewStyle} from "react-native"
import Markdown from 'react-native-markdown-renderer'

interface ExpandingContentCellProps {
    iconType: string
    title: string
    subTitle: string
    description?: string|null
}

interface IconProps {
    type: string
}

export const ExpandingContentCell: React.FC<ExpandingContentCellProps> = (props) => {
    const [expand, setExpand] = useState<boolean>(false)
    const toggleExpand = () => {
        setExpand(!expand)
    }

    const expandedStyle: StyleProp<ViewStyle> = {
        height: 'auto',
        paddingTop: 0,
        paddingRight: 7,
        paddingBottom: 32,
        paddingLeft: 40,
        width: '100%',
        overflow: 'hidden',
    }

    const collapseStyle: StyleProp<ViewStyle> = {
        height: 0,
        padding: 0,
        width: '100%',
        overflow: 'hidden',
    }

    const hasDescription = props.description !== null && props.description!.length > 0

    return (
        <CellWrap>
            <ExpandingContentTouchArea onPress={toggleExpand} activeOpacity={0.7}>
                <Border style={{top: 0}} />
                <Icon type={props.iconType} />
                <View>
                    <ContentTitle>
                        {props.title}
                    </ContentTitle>
                    {props.subTitle !== null && props.subTitle.length > 0 &&
                        <ContentSubTitle>
                            {props.subTitle}
                        </ContentSubTitle>
                    }
                </View>
                {expand && <RightIcon source={require('../../assets/icons/arrowDown.png')}/>}
                {!expand && <RightIcon source={require('../../assets/icons/arrowUp.png')}/>}
                {!expand && <Border style={{bottom: 0}}/>}
            </ExpandingContentTouchArea>
            <ExpandingContentArea style={expand ? expandedStyle : collapseStyle}>
                {hasDescription &&
                  <DescriptionText>
                    <Markdown style={styles}>
                        {props.description}
                    </Markdown>
                  </DescriptionText>
                }
                {props.children}
                {expand && <Border style={{bottom: 0}}/>}
            </ExpandingContentArea>
        </CellWrap>
    )
}

const Icon: React.FC<IconProps> = (props) => {
    return (
        <LeftIcon>
            {props.type === 'customer' && (
                <IconImage source={require('../../assets/icons/customer.png')} />
            )}
            {props.type === 'card' && (
                <IconImage source={require('../../assets/icons/card.png')} />
            )}
            {props.type === 'star' && (
                <IconImage source={require('../../assets/icons/star.png')} />
            )}
        </LeftIcon>
    )
}

const CellWrap = styled.View`
  width: 100%;
  margin-top: -1px;
`

const ExpandingContentTouchArea = styled.TouchableOpacity`
  height: 61px;
  width: 100%;
  flex-direction: row;
  align-items: center;
`

const ExpandingContentArea = styled.View`
  width: 100%;
  overflow: hidden;
`

const Border = styled.View`
  width: 100%;
  height: 1px;
  background-color: #F2F2F2;
  position: absolute;
`

const LeftIcon = styled.View`
  width: 30px;
  height: 30px;
  border: 1px solid #ECECEE;
  border-radius: 15px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 10px;
`

const RightIcon = styled.Image`
  width: 14px;
  height: 8px;
  position: absolute;
  right: 0;
`

const IconImage = styled.Image`
  width: 30px;
  height: 30px;
`

const ContentTitle = styled.Text`
  font-weight: 500;
  font-size: 14px;
  letter-spacing: 0.25px;
  color: #353C43;

`

const ContentSubTitle = styled.Text`
  font-size: 12px;
  line-height: 16px;
  letter-spacing: 0.4px;
  color: #9A9DA0;
`

const DescriptionText = styled.View`
    font-weight: normal;
    font-size: 14px;
    line-height: 20px;
    letter-spacing: 0.25px;
    color: #6E6E77;
    padding-bottom: 40px;
`

const styles = StyleSheet.create({
    text: {
        color: '#6E6E77',
    },
})
