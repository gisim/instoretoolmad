import React, {useState} from 'react'
import {PageTitle} from "./page-title"
import {LoadingContainer} from "../shared"
import {ActivityIndicator, View} from "react-native"
import styled from "styled-components/native"
import {ColumnFill, ColumnWidth} from "../forms/column"
import {ReadOnlyField} from "../data-capture/read-only-field"
import {DefaultButton} from "../data-capture/default-button"
import {useMutation} from "react-query"
import {uploadInsuranceDocumentMutation} from "../../services/api-service"
import {Camera} from "../data-capture/camera"
import {DefaultModal} from "../modal/default-modal"
import {UploadInsuranceDocumentRequest} from "../../models/upload-insurance-document-request"
import {ListSelector} from "../data-capture/list-selector"
import {LookUpData} from "../../models/lookup-data"

interface ActivateInsuranceStep3Props {
  hasUploadedDocument: boolean
  documentUploadError: string | null | undefined
  onDocumentUploaded: (idType: number) => void
  customerCategoryId: number
  customerId: number
  idType: number|null
}

export const ActivateInsuranceStep3: React.FC<ActivateInsuranceStep3Props> = (props) => {
  const [uploadDocument, {isLoading: isUploading}] = useMutation(uploadInsuranceDocumentMutation)
  const [isShowingCamera, setIsShowingCamera] = useState(false)
  const [isUploadComplete, setIsUploadComplete] = useState(false)
  const [idType, setIdType] = useState<number>(props.idType ?? 1)
  const {hasUploadedDocument, documentUploadError} = props
  const [showModal, setShowModal] = useState(false)

  const title = isUploadComplete
    ? (isUploading ? "Uploading…" : "Identity Document has successfully been uploaded")
    : (hasUploadedDocument ? "Identity Document has successfully been uploaded" : "Identity Document not uploaded")

  const options: LookUpData[] = [
    { Description: "South African ID document", Id: 1, Code: null },
    { Description: "Drivers Licence", Id: 2, Code: null },
  ]

  const uploadIdentityDocument = (base64: string) => {
    const body: UploadInsuranceDocumentRequest = {
      CustomerCategoryId: props.customerCategoryId,
      CustomerId: props.customerId,
      DocumentTypeId: idType,
      ImageData: base64,
    }

    uploadDocument(body)
      .then((response) => {
        if (response) {
          props.onDocumentUploaded(idType)
        } else {
          console.log('ID upload failed')
        }
      })
  }

  return (
    <>
      <PageTitle titleType={'pageTitle'} title={'Document Upload'} textAlign={'left'}/>

      {isUploading && (
        <LoadingContainer style={{marginTop: 50}}>
          <ActivityIndicator animating={isUploading} color={'gray'}/>
        </LoadingContainer>
      )}

      <TextWrap>
        Nearly there! We just need to confirm who you are before we continue. Please upload your ID document or driver's
        license.
      </TextWrap>

      <View style={{ marginTop: 32 }}>
        <ReadOnlyField
          title={"Document Type"}
          value={options.find(o => o.Id === idType)?.Description ?? ''}
          onPressed={() => {
            if (isUploadComplete || hasUploadedDocument) { return }
            setShowModal(true)
          }}
          errorText={null}
        />
      </View>

      <Row>
        <ColumnFill top={0}>
          <ReadOnlyField
            title={title} value={""}
            onPressed={() => {
              if (isUploadComplete || hasUploadedDocument) { return }
              setIsShowingCamera(true)
            }}
            errorText={documentUploadError}
          />
        </ColumnFill>
        <ColumnWidth width={200} top={4}>
          <DefaultButton
            buttonType={"default"}
            title={"Upload"}
            isLoading={isUploading}
            disabled={isUploading || isUploadComplete || hasUploadedDocument}
            onPress={() => {
              if (isUploadComplete || hasUploadedDocument) { return }
              setIsShowingCamera(true)
            }}
          />
        </ColumnWidth>
      </Row>

      <DefaultModal
        visible={isShowingCamera}
        title={'Upload Identity Document'}
        headerType={"small"}
        showBottomButtons={true}
      >
        <Camera
          onCancel={() => setIsShowingCamera(false)}
          onUploadComplete={(base64) => {
            uploadIdentityDocument(base64)
            setIsUploadComplete(true)
            setIsShowingCamera(false)
          }}
        />
      </DefaultModal>

      <DefaultModal
        visible={showModal}
        title={"Select a value"}
        headerType={"small"}
        showBottomButtons={false}
        width={'576px'}
        height={'540px'}
      >
        <ListSelector
          values={options}
          onValueSelected={(value) => {
            setIdType(value.Id)
            setShowModal(false)
          }}
        />
      </DefaultModal>
    </>
  )
}

const TextWrap = styled.Text`
  margin-top: 16px;
  color: #6E6E77;
`

const Row = styled.View`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
`
