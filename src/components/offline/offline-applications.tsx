import React, {useLayoutEffect} from 'react'
import {RootStackParamLList} from "../../../App"
import {StackNavigationProp} from '@react-navigation/stack'
import styled from 'styled-components/native'
import {OfflineApplication} from "../../models/db/application"
import {ActivityIndicator, Alert, Button, FlatList, Text, View} from "react-native"
import {useMutation, useQuery} from "react-query"
import {getApplicationsQuery, uploadOfflineApplicationsMutation} from "../../services/offline-service"
import {Q} from "@nozbe/watermelondb"

type OfflineApplicationsScreenNavigationProps = StackNavigationProp<RootStackParamLList, 'OfflineApplications'>

interface OfflineApplicationsProps {
  navigation: OfflineApplicationsScreenNavigationProps
}

export const OfflineApplications: React.FC<OfflineApplicationsProps> = (props) => {
  const {data: applications, refetch} = useQuery(['offline-applications', Q.desc], getApplicationsQuery)
  const [uploadOfflineApplications, { isLoading }] = useMutation(uploadOfflineApplicationsMutation)
  const {navigation} = props

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: "Offline Applications",
      headerRight: () => {
        if (isLoading) {
          return <ActivityIndicator size={"small"} animating={true} color={"gray"} />
        }
        return <Button title={"Retry"} onPress={forceRetry}/>
      },
    })
  }, [navigation, isLoading])

  const forceRetry = () => {
    uploadOfflineApplications()
      .then((data) => {
        if (data === null) {
          Alert.alert('Device is offline', 'Please ensure you are connected to the network to continue.', [{ text: "Close", style: "default" }])
        } else {
          Alert.alert('All done!', 'All pending applications have been uploaded.', [{ text: "Close", style: "default" }])
        }
        refetch()
      })
  }

  if ((applications?.length ?? 0) === 0) {
    return (
      <Container>
        <Text style={{marginTop: 100, textAlign: "center"}}>
          There are no offline applications.
        </Text>
      </Container>
    )
  }

  return (
    <Container>
      <FlatList
        data={applications}
        renderItem={({item}) => <Row application={item}/>}
      />
    </Container>
  )
}

const Row: React.FC<{ application: OfflineApplication }> = (props) => {
  const a = props.application

  return (
    <RowContainer>
      <Text style={{ width: '20%' }}>{a.createdAt.toLocaleString()}</Text>
      <View style={{ width: '30%' }}>
        <Text style={{ fontWeight: "500" }}>Comm Queue ID: {a.commQueueId}</Text>
        <Text style={{ fontSize: 10 }}>Request: {a.endpoint.split("/").slice(-1)[0]}</Text>
      </View>
      <Text style={{ width: '10%' }}>{(a.data.length / 1024).toFixed(0)} KB</Text>
      <Text>Status: Pending Upload</Text>
    </RowContainer>
  )
}


const Container = styled.View`  
`

const RowContainer = styled.View`
  height: 50px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  padding: 0 15px;  
  border-bottom-width: 1px;
  border-bottom-color: #eceeee;
`
