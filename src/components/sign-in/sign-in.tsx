import React, {useEffect, useState} from 'react'
import styled from 'styled-components/native'
import {ActivityIndicator, Alert, Platform} from 'react-native'
import {PageTitle} from '../data-display/page-title'
import {Logo} from '../data-display/logo'
import {DefaultButton} from '../data-capture/default-button'
import {TextArea} from '../data-display/text-area'
import {DefaultTextInput} from '../data-capture/default-text-input'
import {StackNavigationProp} from "@react-navigation/stack"
import {MainStackParamList} from "../../../App"
import {useMutation} from "react-query"
import {getActiveStoresQuery, getClaimsQuery, loginMutation} from "../../services/api-service"
import {ActiveStore} from "../../models/active-store"
import {AppStorageService} from "../../services/app-storage-service"
import {Config} from "../../config"

type SignInScreenNavigationProps = StackNavigationProp<MainStackParamList, 'SignIn'>
const InitialUserDetails = Config.mode === "develop" ? { username: 'mad9000', password: 'Mad*9000', staffCode: '1010307' } : { username: '', password: '', staffCode: '' }

interface SignInProps {
  navigation: SignInScreenNavigationProps
}

export const SignIn: React.FC<SignInProps> = (props) => {
  const [login, { isLoading: loginLoading }] = useMutation(loginMutation)
  const [getClaims, { isLoading: getClaimsLoading }] = useMutation(getClaimsQuery)
  const [getStores, { data: activeStores, isLoading: getStoresLoading, isError: getActiveStoresError, error }] = useMutation(getActiveStoresQuery)

  const [username, setUsername] = useState(InitialUserDetails.username)
  const [password, setPassword] = useState(InitialUserDetails.password)
  const [staffCode, setStaffCode] = useState(InitialUserDetails.staffCode)
  const [selectedStore, setSelectedStore] = useState<ActiveStore|null>(null)
  const [storeCodeInput, setStoreCodeInput] = useState('')
  const [signInButtonActive, setSignInButtonActive] = useState(false)

  const isLoading = loginLoading || getClaimsLoading

  useEffect(() => {
    if (storeCodeInput.length < 3) { return }
    getStores({ query: storeCodeInput })
      .catch(console.error)
  }, [storeCodeInput])

  useEffect(() => {
    if (storeCodeInput.length > 0) { return }
    if ((activeStores?.length ?? 0) > 0) { return }
    AppStorageService.getLastStoreCodeInput()
      .then(lastStoreCodeInput => setStoreCodeInput(lastStoreCodeInput ?? ''))
  }, [storeCodeInput])

  useEffect(() => {
    if (username.length === 0) { setSignInButtonActive(false); return }
    if (password.length === 0) { setSignInButtonActive(false); return }
    if (staffCode.length < 7) { setSignInButtonActive(false); return }
    if (!selectedStore) { setSignInButtonActive(false); return }
    setSignInButtonActive(true)
  }, [username, password, staffCode, selectedStore])

  useEffect(() => {
    if (!getActiveStoresError) { return }
    Alert.alert(
      'Error',
      `The app couldn't connect to the server to retrieve stores. Please try again later or contact support.\n\n${error}`,
      [{ text: "Okay" }]
    )
  }, [getActiveStoresError])

  const onSignInTapped = () => {
    login({ username, password })
      .then(response => {
        if (response?.ResponseStatus !== 0) {
          throw "Invalid username or password. Please try again."
        } else {
          return response!
        }
      })
      .then(response => getClaims({userId: response.User.Id}))
      .then(claims => {
        if ((claims?.UserClaim?.length ?? 0) > 0) {
          Promise.all([
            AppStorageService.setActiveStaffCode(staffCode),
            AppStorageService.setActiveStore(selectedStore!),
            AppStorageService.setLastStoreCodeInput(storeCodeInput),
          ])
          .then(() => {
            props.navigation.navigate("Dashboard")
          })
        } else {
          Alert.alert('An error occurred', 'We couldn\'t log you in. Please contact support', [
            { text: 'Close' },
          ])
        }
      })
      .catch(error => {
        Alert.alert('An error occurred', error, [
          { text: 'Close' },
        ])
      })
  }

  return (
    <Container behavior={Platform.OS === "ios" ? "padding" : "height"}>
        <SignInItemWrap>
          <PageTitle
            titleType={'pageTitle'}
            title={'In-store Tool'}
            textAlign={'center'}
          />
        </SignInItemWrap>
        <SignInItemWrap style={{marginTop: 16}}>
          <Logo type={'large'}/>
        </SignInItemWrap>

        <SignInItemWrap style={{marginTop: 42}}>
          <TextArea withTitle={false} textAlign={'center'} maxWidth={400}>
            Please log in to proceed with assisting customers in their Mr Price
            Money account activations process.
          </TextArea>
        </SignInItemWrap>


        <SignInItemWrap style={{marginTop: 22}}>
          <DefaultTextInput type={'text'} placeholder={'Username'} value={username} setValue={setUsername} autoCapitalize={'none'} />
        </SignInItemWrap>

        <SignInItemWrap>
          <DefaultTextInput type={'password'} placeholder={'Password'} value={password} setValue={setPassword} />
        </SignInItemWrap>

        <SignInItemWrap>
          <DefaultTextInput
            type={'text'}
            placeholder={'Staff code'}
            value={staffCode}
            setValue={setStaffCode}
            errorText={(staffCode.length > 0 && staffCode.length < 7) ? 'Staff code must be minimum 7 digits' : null}
          />
        </SignInItemWrap>

        <SignInItemWrap>
          <DefaultTextInput type={'text'} placeholder={'Store'} value={storeCodeInput} setValue={setStoreCodeInput} style={{ marginBottom: 0 }} />
          <StoreCodeScroller directionalLockEnabled={true} horizontal={true}>
            {getStoresLoading &&
              <ActivityIndicator size={"large"} animating={getStoresLoading} color={'gray'} />
            }
            {!getActiveStoresError && activeStores?.map(store => (
              <StoreButton isSelected={store.StoreId === selectedStore?.StoreId && store.DivisionId === selectedStore?.DivisionId} onPress={() => setSelectedStore(store)} activeOpacity={1.0} key={`${store.DivisionId}${store.StoreId}`}>
                <StoreName>{store.StoreDesc} ({store.StoreCode.trimRight()})</StoreName>
                <StoreDivision>{store.DivisionDesc} </StoreDivision>
              </StoreButton>
            ))}
          </StoreCodeScroller>
        </SignInItemWrap>

        <SignInItemWrap style={{marginTop: 48}}>
          <DefaultButton
            buttonType={'default'}
            title={'sign in'}
            disabled={!signInButtonActive}
            onPress={onSignInTapped}
            isLoading={isLoading}
          />
        </SignInItemWrap>

        <SignInItemWrap style={{marginTop: 50}}>
          <TextArea withTitle={false} textAlign={'center'} maxWidth={438}>
            If you don’t have log in details, or have forgotten your log in
            details, please contact mrpSupport on 0800 123 456
          </TextArea>
        </SignInItemWrap>
    </Container>
  )
}

const Container = styled.KeyboardAvoidingView`
  display: flex;
  height: 100%;
  align-items: center;
  justify-content: center;
`

const SignInItemWrap = styled.View`
  width: 438px;
  display: flex;
  align-items: center;
`

const StoreCodeScroller = styled.ScrollView`
  display: flex;
  flex-direction: row;
  margin-top: 5px;
  width: 100%;
  height: 62px;
`

const StoreButton = styled.TouchableOpacity<{ isSelected: boolean }>`
  border-radius: 4px;
  border-width: 1px;
  border-color: #ececee;
  display: flex;
  align-items: flex-start;
  justify-content: center;
  padding: 10px;
  margin-right: 5px;
  background: ${props => props.isSelected ? '#ececee' : 'white'}
`

const StoreName = styled.Text`
  font-weight: bold;
  font-size: 14px;
  line-height: 20px;
  text-align: center;
  letter-spacing: 0.25px;
  color: #6E6E77;
`

const StoreDivision = styled.Text`
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  text-align: center;
  letter-spacing: 0.25px;
  color: #6E6E77;
`
