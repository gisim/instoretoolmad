import React, {useEffect} from 'react'
import {ActivityIndicator, Alert, Image, ScrollView, TouchableOpacity} from 'react-native'
import {ProcessCell, ProcessTable} from '../data-display/process-table'
import {TopNav} from "../navigation/top-nav"
import {StackNavigationProp} from "@react-navigation/stack"
import {MainStackParamList} from "../../../App"
import {FormType} from '../../services/form-service'
import {AppStorageService} from "../../services/app-storage-service"
import {VersionNumber} from "../navigation/version-number"
import NetInfo from '@react-native-community/netinfo'
import {useMutation, useQuery} from "react-query"
import {
  getApplicationsCountQuery,
  uploadOfflineApplicationsMutation
} from "../../services/offline-service"
import styled from "styled-components/native"
import {Config} from "../../config"

const processes: ProcessCell[] = [
  {
    id: 0,
    title: 'start the process',
    cells: [
      {
        id: 0,
        title: 'account application',
        subtext: 'Help a customer apply for a Mr Price Money account.',
        icon: 'application',
        screen: 'CaptureCustomerId',
        screenType: 'modal',
        form: FormType.New,
        processType: 'application',
      },
    ],
  },
  {
    id: 1,
    title: 'activations',
    cells: [
      {
        id: 0,
        title: 'account activation',
        subtext: 'Help a customer activate their Mr Price Money account.',
        icon: 'activation',
        screen: 'CaptureCustomerId',
        screenType: 'modal',
        processType: 'activation',
      },
      {
        id: 1,
        title: 'insurance',
        subtext: 'Help a customer apply for cover with Mr Price Insurance.',
        icon: 'insurance',
        screen: 'ActivateInsurance',
      },
    ],
  },
  {
    id: 2,
    title: 'coming soon',
    cells: [
      {
        id: 2,
        title: 'mr price mobile',
        subtext:
          'Help a customer get connected with a prepaid or sim top up package.',
        icon: 'mrpMobile',
      },
    ],
  },
  // {
  //   id: 2,
  //   title: 'view all',
  //   cells: [
  //     {
  //       id: 0,
  //       title: 'all applications',
  //       subtext: 'Search all applications by customer ID number.',
  //       icon: 'allApplication',
  //       screen: 'AllApplications',
  //       screenType: 'fullscreen',
  //     },
  //   ],
  // },
]

type DashboardScreenNavigationProps = StackNavigationProp<MainStackParamList, 'Dashboard'>

interface DashboardProps {
  navigation: DashboardScreenNavigationProps
}

export const Dashboard: React.FC<DashboardProps> = ({navigation}) => {
  const {data: applicationsCount, refetch} = useQuery('offline-applications-count', getApplicationsCountQuery)

  const [uploadOfflineApplications, {isLoading: uploadingOfflineApplications}] = useMutation(uploadOfflineApplicationsMutation)

  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      console.log(`Connected via`, state.type, `Connected:`, state.isConnected, `Reachable:`, state.isInternetReachable)
      if (state.isConnected && state.isInternetReachable) {
        uploadOfflineApplications()
          .then(() => {
            console.log('Fetching new applications')
          })
          .then(() => refetch())
      }
    })

    return () => {
      unsubscribe()
    }
  }, [applicationsCount, refetch])

  useEffect(() => {
    const interval = setInterval(() => {
      refetch()
    }, 5000)

    return () => clearInterval(interval)
  })

  const resetSession = () => {
    AppStorageService.reset()
      .then(() => {
        navigation.popToTop()
      })
  }

  const onLogOutPressed = () => {
    Alert.alert(
      'Log out?',
      'Are you sure you want to log out?',
      [
        {text: "Log out", style: "destructive", onPress: () => resetSession()},
        {text: "Cancel", style: "cancel"},
      ]
    )
  }

  const launchDebugger = () => {
    // @ts-ignore
    navigation.navigate("LogViewer")
  }

  const launchOfflineViewer = () => {
    // @ts-ignore
    navigation.navigate("OfflineApplications")
  }

  return (
    <DashboardContainer>
      <TopNav/>
      <ScrollView>
        <ProcessTable processList={processes}/>

        <DashboardFooter>
          <FooterItem
            onPress={onLogOutPressed}
            title={'log out'}
          />
          <VersionNumber/>
        </DashboardFooter>

        <OtherButtons>
          {Config.debug &&
          <TouchableOpacity onPress={launchDebugger}>
              <Image source={require("../../assets/icons/debug.png")} style={{width: 30, height: 30}}/>
          </TouchableOpacity>
          }

          <TouchableOpacity onPress={launchOfflineViewer}>
            {uploadingOfflineApplications &&
            <ActivityIndicator size={"large"} animating={true}/>}
            {!uploadingOfflineApplications && (applicationsCount ?? 0) > 0 &&
            <Image source={require("../../assets/icons/offline.png")} style={{width: 30, height: 30}}/>}
          </TouchableOpacity>
        </OtherButtons>
      </ScrollView>
    </DashboardContainer>
  )
}

interface FooterItemProps {
  onPress: () => void
  title: string
}

const FooterItem: React.FC<FooterItemProps> = (props) => {
  return (
    <FooterCellTouchArea onPress={props.onPress}>
      <FooterCellDivider/>
      <FooterCellTitle>{props.title}</FooterCellTitle>
    </FooterCellTouchArea>
  )
}

const DashboardContainer = styled.View`
  padding: 0 21px;
`

const DashboardFooter = styled.View`
  display: flex;
  align-items: flex-start;
  margin-top: 64px;
`

const FooterCellDivider = styled.View`
  width: 100%;
  height: 1px;
  background-color: #F2F2F2;
  position: absolute;
  top: 0;
`

const FooterCellTouchArea = styled.TouchableOpacity`
  width: 100%;
  height: 72px;
  display: flex;
  justify-content: center;
`

const FooterCellTitle = styled.Text`
  font-size: 16px;
  line-height: 24px;
  letter-spacing: 0.444444px;
  color: #6E6E6D;
  text-transform: capitalize;
`

const OtherButtons = styled.View`
  width: 10%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 100px;
`
