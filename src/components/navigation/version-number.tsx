import React from "react"
import styled from "styled-components/native"
import DeviceInfo from 'react-native-device-info'

export const VersionNumber: React.FC = () => {
    const versionNumberString = `v${DeviceInfo.getVersion()}.${DeviceInfo.getBuildNumber()}`

    return (
        <ConstantVersionNumber>
            {versionNumberString}
        </ConstantVersionNumber>
    )
}

const ConstantVersionNumber = styled.Text`
  position: absolute;
  bottom: 26px;
  right: 0;
  background-color: #FFFFFF;
  font-size: 12px;
  letter-spacing: 0.4px;
  color: #9A9DA0;
`
