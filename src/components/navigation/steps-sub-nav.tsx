import React from 'react'
import styled from 'styled-components/native'
import Markdown from 'react-native-markdown-renderer'

import {PageTitle} from '../data-display/page-title'
import {TextArea} from '../data-display/text-area'
import {DefaultButton} from '../data-capture/default-button'

interface StepsSubNavProps {
  currentStep: number
  totalSteps: number
  title: string | undefined | null
  description: string | undefined | null
  onNext: () => void
  onBack: () => void
  canGoBack?: boolean
  canGoNext?: boolean
}

interface StepsTrackerBarProps {
  currentStep: number
  totalSteps: number
}

export const StepsSubNav: React.FC<StepsSubNavProps> = (props) => {
  const hasMultipleSteps = props.totalSteps > 1
  const canGoBack = props.canGoBack ?? true
  const canGoNext = props.canGoNext ?? true

  return (
    <StepsSubNavWrap>
      {hasMultipleSteps &&
        <StepsTrackerBar currentStep={props.currentStep} totalSteps={props.totalSteps}/>
      }
      <NavContent>
        {hasMultipleSteps &&
          <StepsText>
              Step {props.currentStep} of {props.totalSteps}
          </StepsText>
        }
        {props.title && <PageTitle titleType={'pageTitle'} title={props.title} textAlign={'left'}/>}
        {props.description &&
          <TextWrap>
            <Markdown>{props.description}</Markdown>
          </TextWrap>
        }
        <RightButtonArea>
          {props.currentStep > 1 && canGoBack && (
            <BackButton>
              <DefaultButton buttonType={'smallLight'} title={'back'} disabled={false} onPress={props.onBack}/>
            </BackButton>
          )}
          {props.totalSteps !== props.currentStep && canGoNext && (
            <NextButton>
              <DefaultButton buttonType={'smallDefault'} title={'next'} disabled={false} onPress={props.onNext}/>
            </NextButton>
          )}
        </RightButtonArea>
      </NavContent>
    </StepsSubNavWrap>
  )
}

const StepsTrackerBar: React.FC<StepsTrackerBarProps> = (props) => {
  const currentStepPercent = 100 / (props.totalSteps) * (props.currentStep)
  const barProgressPercent = currentStepPercent + '%'
  return (
    <StyledTopTrackerBar>
      <StyledTopTrackerBarFill style={{width: barProgressPercent}}/>
    </StyledTopTrackerBar>
  )
}

const StepsSubNavWrap = styled.View`
  margin: 0 -52px;
`

const NavContent = styled.View`
  width: 100%;
  padding: 29px 52px 0 52px;
`

const StyledTopTrackerBar = styled.View`
  height: 3px;
  background-color: #f2f2f2;
`

const StyledTopTrackerBarFill = styled.View`
  height: 100%;
  background-color: #d71920;
`

const StepsText = styled.Text`
  font-weight: 500;
  font-size: 14px;
  letter-spacing: 0.25px;
  color: #6E6E77;
  margin-bottom: 10px;
`

const TextWrap = styled.View`
  margin-top: 16px;
`

const RightButtonArea = styled.View`
  position: absolute;
  width: 136px;
  top: 29px;
  right: 52px;
  flex-direction: row;
`

const BackButton = styled.View`
  position: absolute;
  left: 0;
`

const NextButton = styled.View`
  position: absolute;
  right: 0;
`
