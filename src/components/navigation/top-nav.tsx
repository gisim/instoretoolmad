import React, { useEffect, useState } from 'react'
import styled from 'styled-components/native'
import { ActiveStore } from '../../models/active-store'
import { User } from '../../models/login-response'
import { AppStorageService } from '../../services/app-storage-service'
import { Logo } from '../data-display/logo'

interface TopNavProps {}

export const TopNav: React.FC<TopNavProps> = () => {
  const [userText, setUserText] = useState<string|null>(null)

  useEffect(() => {
    if (userText) { return }
    Promise.all([AppStorageService.getActiveStore(), AppStorageService.getUser()])
      .then((data: any[]) => {
        const store: ActiveStore = data[0]
        const user: User = data[1]

        setUserText(`${user.UserName}, ${store.StoreDesc}`)
      })
  }, [userText, setUserText])

  return (
    <StyledTopNav style={{ marginBottom: 0 }}>
      <StyledTopLogoWrap>
        <Logo type={'small'} />
      </StyledTopLogoWrap>
      {userText !== null && <StyledNavUser>{userText}</StyledNavUser>}
      <StyledNavBottomBorder />      
    </StyledTopNav>
  )
}

const StyledTopNav = styled.View`
  height: 54px;
  background-color: #f9f9fb;
  flex-direction: row;
  align-items: center;
  margin: 0 -52px;
`

const StyledTopLogoWrap = styled.View`
  width: 210px;
  left: 52px;
`

const StyledNavBottomBorder = styled.View`
  width: 200%;
  height: 1px;
  background-color: #ececee;
  position: absolute;
  bottom: 0;
`

const StyledNavUser = styled.Text`
  position: absolute;
  right: 52px;
`
