import React, { useLayoutEffect } from 'react'
import {Image} from "react-native"
import styled from "styled-components/native"
import {StackNavigationProp} from "@react-navigation/stack"
import {MainStackParamList} from "../../../App"
import {DefaultButton} from "../data-capture/default-button"
import { RouteProp } from '@react-navigation/native'

type FormErrorScreenNavigationProps = StackNavigationProp<MainStackParamList, 'FormError'>
type FormErrorScreenRouteProps = RouteProp<MainStackParamList, 'FormError'>

interface FormErrorProps {
  navigation: FormErrorScreenNavigationProps
  route: FormErrorScreenRouteProps
}

export const FormError: React.FC<FormErrorProps> = (props) => {
  const {navigation} = props
  const { title, message } = props.route.params

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: "Declined",
    })
  }, [navigation])

  return (
    <>
      <Container>
        <DeclinedBar>
          <DeclinedBarText>{title}</DeclinedBarText>
        </DeclinedBar>
        <Image source={require("../../assets/icons/declined.png")} style={{width: 92, height: 92}} width={92} height={92}/>
        <BottomView>
          <Title>{title}</Title>
          <Description>{message}</Description>
        </BottomView>
      </Container>
      <BottomAnchorView>
        <DefaultButton
          buttonType={'default'}
          title={'Take Me Back'}
          disabled={false}
          onPress={() => {
            navigation.navigate('Dashboard')
          }}
        />
      </BottomAnchorView>
    </>
  )
}

const Container = styled.View`
  display: flex;
  align-items: center;
  justify-content: center; 
  padding-top: 52px;
`

const BottomView = styled.View`
  margin-top: 32px;
  padding-top: 28px;
  width: 50%;
  align-items: center;
  justify-content: center;
`

const Title = styled.Text`
  font-weight: 600;
  font-size: 20px;
  line-height: 20px;
  letter-spacing: 0.25px;
  color: #353C43;
  margin-bottom: 12px;
  text-align: center;
`

const Description = styled.Text`
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  text-align: center;
  letter-spacing: 0.25px;
  color: #353C43;
`

const BottomAnchorView = styled.View`
  position: absolute;
  bottom: 20px;
  right: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
`

const DeclinedBar = styled.View`
  margin-top: 20px;
  margin-bottom: 92px;
  background: #F44336;
  height: 48px;
  width: 90%;
  border-radius: 4px;
  display: flex;
  align-items: center;
  justify-content: center;
`

const DeclinedBarText = styled.Text`
  color: white;
  font-weight: 500;
  font-size: 18px;
  letter-spacing: 1px;
`
