import React, {useLayoutEffect} from 'react'
import {Image} from "react-native"
import styled from "styled-components/native"
import {StackNavigationProp} from "@react-navigation/stack"
import {MainStackParamList} from "../../../App"
import {DefaultButton} from "../data-capture/default-button"
import {RouteProp} from "@react-navigation/native"

type FormCompleteScreenNavigationProps = StackNavigationProp<MainStackParamList, 'FormSuccess'>
type FormCompleteScreenRouteProps = RouteProp<MainStackParamList, 'FormSuccess'>

interface FormCompleteProps {
  navigation: FormCompleteScreenNavigationProps
  route: FormCompleteScreenRouteProps
}

export const FormComplete: React.FC<FormCompleteProps> = (props) => {
  const {navigation} = props
  const { creditLimit } = props.route.params || ""

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: "Complete",
    })
  }, [navigation])

  return (
    <>
      <Container>
        <Image
          source={require("../../assets/icons/formSuccess.png")}
          style={{width: 64, height: 64}} width={64}
          height={64}
        />
        <StatusMessage>You're done!</StatusMessage>
        {creditLimit && creditLimit.length > 0 &&
            <>
              <RefNumber>Congratulations, you have been{'\n'}approved for R{creditLimit}!</RefNumber>
              <BottomView>
                  <Description>You can activate your store card now or take the reference number above and your ID to your
                      nearest
                      store to activate when ready.</Description>
              </BottomView>
            </>
        }
        {!creditLimit &&
            <>
              <BottomView>
                <Title>Your account has been approved!</Title>
                <Description>You can activate your store card now or take the reference number above and your ID to your
                    nearest
                    store to activate when ready.</Description>
              </BottomView>
            </>
        }
      </Container>
      <BottomAnchorView>
        <DefaultButton
          buttonType={'default'}
          title={'Take Me Back'}
          disabled={false}
          onPress={() => {
            navigation.navigate('Dashboard')
          }}
        />
      </BottomAnchorView>
    </>
  )
}

const Container = styled.View`
  display: flex;
  align-items: center;
  justify-content: center; 
  padding-top: 52px;
`

const StatusMessage = styled.Text`
  margin-top: 24px;
  margin-bottom: 8px;
  font-weight: 600;
  font-size: 20px;
  line-height: 28px;
  letter-spacing: 0.5px;
  color: #8BC34A;
`

const RefNumber = styled.Text`
  font-weight: 500;
  font-size: 14px;
  line-height: 19px;
  text-align: center;
  letter-spacing: 1.35px;
  text-transform: uppercase;  color: #353C43;
`

const BottomView = styled.View`
  margin-top: 32px;
  padding-top: 28px;
  border-top-width: 2px;
  border-top-color: #f2f2f2;
  width: 50%;
  align-items: center;
  justify-content: center;
`

const Title = styled.Text`
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  letter-spacing: 0.25px;
  color: #353C43;
  margin-bottom: 12px;
`

const Description = styled.Text`
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  text-align: center;
  letter-spacing: 0.25px;
  color: #353C43;
`

const BottomAnchorView = styled.View`
  position: absolute;
  bottom: 20px;
  right: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
`
