import React, {useEffect} from 'react'
import styled from "styled-components/native"
import {TopNav} from "../navigation/top-nav"
import {StackNavigationProp} from "@react-navigation/stack"
import {MainStackParamList} from "../../../App"
import {RouteProp} from "@react-navigation/native"
import {useProcessor} from "../../forms/form-processor"
import {FormType} from "../../services/form-service"
import {ActivityIndicator, Alert} from "react-native"
import {getCustomerInfo, getInstoreApplicationDataQuery} from "../../services/api-service"
import {sanitizeApplicationData, sanitizeCustomerInfo} from "../../services/customer-service"
import {useMutation} from "react-query"

type ProcessingScreenNavigationProps = StackNavigationProp<MainStackParamList, 'Processing'>
type ProcessingScreenRouteProps = RouteProp<MainStackParamList, 'Processing'>

interface ProcessingProps {
  navigation: ProcessingScreenNavigationProps
  route: ProcessingScreenRouteProps
}

export const Processing: React.FC<ProcessingProps> = (props) => {
  const {processor, formData, onSuccess} = props.route.params
  const {navigation} = props
  const { message, complete, submit: retry } = useProcessor(processor, "submit", formData)
  const [getInstoreApplicationData] = useMutation(getInstoreApplicationDataQuery)

  useEffect(() => {
    if (!complete) { return }

    if (complete.status === "complete") {
      switch (onSuccess.type) {
        case "form": {
          let commQueueId = "0"
          let newAppId = "0"

          if (complete.formData && complete.formData["CommunicationQueueId"]?.value) {
            commQueueId = complete.formData["CommunicationQueueId"]?.value
          }

          if (complete.formData && complete.formData["NewApplicationId"]?.value) {
            newAppId = complete.formData["NewApplicationId"]?.value
          }

          Promise.all([
            getCustomerInfo({ idNumber: formData.IdentityNumber.trim() }),
            getInstoreApplicationData({
              communicationQueueId: commQueueId,
              newApplicationId: newAppId,
            }),
          ])
            .then((response) => {
              const details = response[0] as { [key: string]: any }
              const applicationInfo = response[1] as { [key: string]: any }
              const existingApplication = sanitizeApplicationData(applicationInfo)

              const customerDetails = sanitizeCustomerInfo(details)
              navigation.push('FormRenderer', {
                form: onSuccess.name as FormType,
                formData: {
                  ...complete.formData,
                  ...existingApplication,
                  ...customerDetails,
                },
                processor: 'complete-application',
                canGoBack: false,
              })
            })
          break
        }
        case "success": {
          navigation.navigate('FormSuccess', {})
          break
        }
      }
    }
    else if (complete.status === "pending") {
      navigation.navigate("FormPending", { creditLimit: complete.creditLimit })
    }
    else if (complete.status === "final") {
      navigation.navigate('FormSuccess', { creditLimit: complete.creditLimit })
    }
    else if (complete.status === "error") {
      switch (complete.type) {
        case "rejected": {
          navigation.navigate("FormError", complete.error)
          break
        }
        case "technical": {
          if (!retry) {
            navigation.navigate("FormError", complete.error)
          } else {
            Alert.alert(
              'Error',
              'A technical error occurred. Would you like to retry?',
              [
                {text: "Retry", style: "default", onPress: () => retry({})},
                {text: "Cancel", style: "cancel", onPress: () => navigation.navigate("FormError", complete.error)},
              ]
            )
          }
          break
        }
      }
    }

  }, [complete, navigation, onSuccess])

  return (
    <Container>
      <TopNavContainer>
        <TopNav />
      </TopNavContainer>
      <Background source={require('../../assets/upsell/upsell-1.png')} resizeMethod={"scale"} resizeMode={"cover"} />
      <Carousel>
        <ActivityIndicator size={"small"} animating={true} color={"grey"} />
        <Message>{message}…</Message>
      </Carousel>
    </Container>
  )
}

const Container = styled.View`
  flex: 1;
  flex-direction: column;
`

const TopNavContainer = styled.View`
  padding: 0px 52px;
`

const Background = styled.Image`
  width: 100%;
  height: 85%;
  margin: 10px;
  border-radius: 4px;
`

const Carousel = styled.View`
  border-top-width: 1px;
  border-top-color: #ECECEE;
  background-color: #F9F9FB;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  flex: 1;
`

const Message = styled.Text`
  font-size: 16px;
  line-height: 20px;
  text-align: center;
  letter-spacing: 0.25px;
  color: #353C43;
  margin-left: 15px;
`
