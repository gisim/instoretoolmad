import React, { useLayoutEffect } from 'react'
import {Image} from "react-native"
import styled from "styled-components/native"
import {StackNavigationProp} from "@react-navigation/stack"
import {MainStackParamList} from "../../../App"
import {DefaultButton} from "../data-capture/default-button"
import { RouteProp } from '@react-navigation/native'
import {TopNav} from "../navigation/top-nav"

type FormPendingScreenNavigationProps = StackNavigationProp<MainStackParamList, 'FormPending'>
type FormPendingScreenRouteProps = RouteProp<MainStackParamList, 'FormPending'>

interface FormPendingProps {
  navigation: FormPendingScreenNavigationProps
  route: FormPendingScreenRouteProps
}

export const FormPending: React.FC<FormPendingProps> = (props) => {
  const {navigation} = props
  const { creditLimit } = props.route.params || ""

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: "Pending",
    })
  }, [navigation])

  const header = `You have been conditionally\napproved for R${creditLimit ?? "xxxx.yy"}!`

  const paragraph1 = `However your application is currently placed on hold with certain conditions and employment verification checks required:`

  const paragraph2 = "We require you to please provide us with your latest payslip or bank statements (This will be compulsory if you are currently self-employed) or alternatively, we will be contacting your current employer to confirm your employment status.\n\nIf you are a foreign national, you will be required to provide us with a copy of your work permit and passport."

  const terms = "Please note that the above offer is subject to affordability and that T&Cs will apply."

  return (
    <>
      <Container>
        <Image source={require("../../assets/icons/formSuccess.png")} style={{width: 64, height: 64}} width={64} height={64}/>
        <StatusMessage>Things are Happening!</StatusMessage>
        <Header>{header}</Header>
        <BottomView>
          <Paragraph1>{paragraph1}</Paragraph1>
          <Paragraph2>{paragraph2}</Paragraph2>
          <Terms>{terms}</Terms>
        </BottomView>
      </Container>
      <BottomAnchorView>
        <DefaultButton
          buttonType={'default'}
          title={'Take Me Back'}
          disabled={false}
          onPress={() => {
            navigation.navigate('Dashboard')
          }}
        />
      </BottomAnchorView>
    </>
  )
}

const Container = styled.View`
  display: flex;
  align-items: center;
  justify-content: center; 
  padding-top: 52px;
`

const BottomView = styled.View`
  margin-top: 32px;
  padding-top: 28px;
  width: 50%;
  align-items: center;
  justify-content: center;
  border-top-width: 2px;
  border-top-color: #f2f2f2;
`

const StatusMessage = styled.Text`
  margin-top: 24px;
  margin-bottom: 8px;
  font-weight: 600;
  font-size: 20px;
  line-height: 28px;
  letter-spacing: 0.5px;
  color: #8BC34A;
`

const Header = styled.Text`
  font-weight: 500;
  font-size: 14px;
  line-height: 24px;
  text-align: center;
  letter-spacing: 1.35px;
  text-transform: uppercase;
  color: #353C43;
`

const Paragraph1 = styled.Text`
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  text-align: justify;
  letter-spacing: 0.25px;
  color: #353C43;
`

const Paragraph2 = styled.Text`
  margin-top: 28px
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  text-align: justify;
  letter-spacing: 0.25px;
  color: #353C43;
`

const BottomAnchorView = styled.View`
  position: absolute;
  bottom: 20px;
  right: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
`

const Terms = styled.Text`
  margin-top: 28px
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  text-align: center;
  letter-spacing: 0.25px;
  color: #9A9DA0;
`
