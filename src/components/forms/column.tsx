import styled from "styled-components/native"
import {Platform} from "react-native"

export const Row = styled.View<{ zIndex: number }>`
  display: flex;
  flex-direction: row;  
  z-index: ${props => props.zIndex};
`

export const Column = styled.View<{ columns: number, isDropDown: boolean }>`
  width: ${props => 100 / props.columns}%;
  z-index: ${props => Platform.OS === 'ios' ? (props.isDropDown ? 1000 : 1) : 1};
  margin-right: ${props => props.columns > 1 ? '8px' : '0'};
  padding-right: ${props => props.columns > 1 ? '8px' : '0'};
  display: flex;
  justify-content: space-between;
`


export const ColumnFill = styled.View<{ top: number }>`
  margin-top: ${props => props.top}px;  
  margin-right: 8px;
  flex-grow: 1;
`

export const ColumnWidth = styled.View<{ width: number, top: number }>`
  width: ${props => props.width}px;  
  margin-top: ${props => props.top}px;  
`
