import React, {useEffect, useLayoutEffect, useRef, useState} from 'react'
import {Alert, KeyboardAvoidingView, Platform, ScrollView, View} from 'react-native'
import styled from 'styled-components/native'
import {PageTitle} from '../data-display/page-title'
import {DefaultTextInput} from '../data-capture/default-text-input'
import {RouteProp} from '@react-navigation/native'
import {StackNavigationProp} from "@react-navigation/stack"
import {MainStackParamList} from "../../../App"
import {StepsSubNav} from "../navigation/steps-sub-nav"
import {Component, Option} from "../../models/form"
import {hasErrors, useForm} from "../../services/form-service"
import {DefaultCheckBox} from "../data-capture/default-check-box"
import {DefaultHorizontalRadioGroup} from "../data-capture/default-horizontal-radio-group"
import {ExpandingContentCell} from "../data-display/expanding-content-cell"
import {DefaultVerticalRadioGroup} from "../data-capture/default-vertical-radio-group"
import {DefaultButton} from "../data-capture/default-button"
import {RepaymentPeriod} from "../data-capture/repayment-period"
import {Column, Row} from './column'
import {NewApplicationTerms} from "../modal/new-application-terms"
import {useProcessor} from "../../forms/form-processor"
import {AppStorageService} from "../../services/app-storage-service"
import {LookUpData} from "../../models/lookup-data"
import {ReadOnlyField} from "../data-capture/read-only-field"
import {SearchPostalCodesModal} from "../modal/search-postal-codes"
import {DefaultModal} from "../modal/default-modal"
import {CaptureSignature} from "../modal/capture-signature"
import {SignatureField} from "../data-capture/signature-field"
import {UploadDocuments} from "../modal/upload-documents"
import DateTimePickerModal from "react-native-modal-datetime-picker"
import {DateField} from "../data-capture/date-field"
import {ListSelector} from "../data-capture/list-selector"
import {TimePeriodSelector} from "../data-capture/time-period-selector"
import {logValidation} from "../../services/debugger-service"
import {NumberSelector} from "../data-capture/number-selector"
import NetInfo from "@react-native-community/netinfo"
import { v4 as uuid } from 'uuid'

type AccountApplicationScreenNavigationProps = StackNavigationProp<MainStackParamList, 'FormRenderer'>
type AccountApplicationScreenRouteProps = RouteProp<MainStackParamList, 'FormRenderer'>
type LookUpModalData = { value: string, name: string }

interface AccountApplicationProps {
  navigation: AccountApplicationScreenNavigationProps
  route: AccountApplicationScreenRouteProps
}

export const FormRenderer: React.FC<AccountApplicationProps> = (props) => {
  const {lookUp} = AppStorageService
  const {navigation} = props
  const {processor, formData, canGoBack} = props.route.params
  const { submit: submitSaveAndContinue } = useProcessor(processor, "saveAndContinue", formData)

  const [showModal, setShowModal] = useState(false)
  const [hasSeenModal, setHasSeenModal] = useState(false)
  const [currentStep, setCurrentStep] = useState(1)
  const [showPostalCodeModal, setShowPostalCodeModal] = useState<string|null>(null)
  const [showSignatureModal, setShowSignatureModal] = useState(false)
  const [showDocumentUploader, setShowDocumentUploader] = useState<string|null>(null)
  const [showDatePicker, setShowDatePicker] = useState<string|null>(null)
  const [showLookUpSelector, setShowLookUpSelector] = useState<LookUpModalData|null>(null)
  const [showErrors, setShowErrors] = useState(false)
  const [isOnline, setIsOnline] = useState(false)

  const {formValues, componentAtPath, valueForPath, setValueForComponent, form, submittableForm, errors} = useForm(props.route.params.form)
  const termsModal = useRef<React.ReactNode|null>(null)

  let canFormGoBack = true
  if (typeof canGoBack !== 'undefined') {
    canFormGoBack = canGoBack
  }

  useEffect(() => {
    if (!form.initialModal) { return }
    if (hasSeenModal) { return }
    switch (form.initialModal) {
      case "new-application-terms": {
        termsModal.current = (
          <NewApplicationTerms
            cancelTapped={() => {
              setShowModal(false)
              navigation.pop()
            }}
            agreeTapped={(agreedTerms, gaveCreditConsent, isNotUnderDebtReview) => {
              setShowModal(false)

              if (!agreedTerms) {
                navigation.pop()
                return
              }

              setValueForComponent({ name: "IsCreditConsentGiven", type: "bool" }, gaveCreditConsent ? "true" : "false")
              setValueForComponent({ name: "IsUnderDebtReview", type: "bool" }, isNotUnderDebtReview ? "false" : "true")
            }}
          />)
        break
      }
    }

    if (termsModal) {
      setShowModal(true)
      setHasSeenModal(true)
    }
  }, [hasSeenModal, setHasSeenModal, setShowModal, form, navigation])

  useEffect(() => {
    navigation.addListener('beforeRemove', (e) => {
      // @ts-ignore
      if (e.data.action.payload && e.data.action.payload.name === "Dashboard") { return }
      // @ts-ignore
      if (e.data.action.payload && e.data.action.payload.name === "Processing") { return }
      if (canFormGoBack) { return }
      e.preventDefault()
    })
  })

  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      if (state.isConnected && state.isInternetReachable) {
        setIsOnline(true)
      } else {
        setIsOnline(false)
      }
    })

    return () => {
      unsubscribe()
    }
  }, [setIsOnline])

  useEffect(() => {
    if (formData) {
      const formDataKeys = Object.keys(formData)
      formDataKeys.forEach(key => {
        setValueForComponent({name: key, type: formData[key].type}, formData[key].value)
      })
    }
  }, [formData])

  useEffect(() => {
    setShowErrors(false)
  }, [currentStep, setShowErrors])

  useLayoutEffect(() => {
    const options: { [key: string]: any } = {
      headerTitle: form.title ?? "Application",
    }

    if (!canFormGoBack) {
      options.headerLeft = null
    }

    navigation.setOptions(options)
  }, [navigation, form])


  const validateForm = (): boolean => {
    if (hasErrors(currentStep, errors)) {
      const logName = 'Form Validation Errors'
      logValidation(logName, errors)
      // console.log(errors)
      setShowErrors(true)
      return false
    }

    setShowErrors(false)
    return true
  }

  const nextStep = () => {
    if (!validateForm()) { return }
    if (form.supportsSaveAndContinue && submitSaveAndContinue) {
      const currentFormData = submittableForm()
      submitSaveAndContinue(currentFormData)
    }
    setCurrentStep(currentStep + 1)
  }

  const prevStep = () => {
    setCurrentStep(currentStep - 1)
  }

  const goToDashboard = () => {
    navigation.navigate('Dashboard')
  }

  if (!form) {
    return null
  }

  const submitForm = () => {
    if (!validateForm()) { return }
    console.log('no errors, submit')
    const submittableFormData = submittableForm()

    navigation.push('Processing', {
      processor: form.formProcessor,
      formData: submittableFormData,
      onSuccess: form.onSuccess,
    })
  }

  const saveAndExit = () => {
    if (!validateForm()) { return }

    if (form.supportsSaveAndContinue && submitSaveAndContinue) {
      const currentFormData = submittableForm()
      submitSaveAndContinue(currentFormData)
      Alert.alert(
        'Form Saved',
        'Your application form has been saved. Would you like to exit?',
        [
          { text: 'Exit', onPress: goToDashboard, style: "destructive" },
          { text: 'Cancel', style: "cancel" },
        ]
      )
    }
  }

  const showOfflineAlert = () => {
    Alert.alert(
      'Device is offline',
      `This action can't be performed because you are currently offline. Try again when you are online.`,
      [{ text: "Close" }]
    )
  }

  const refineErrors = (name: string): string|null => {
    if (!showErrors) { return null }
    if (errors[currentStep] === undefined) { return null}
    if (errors[currentStep][name] === undefined) { return null }
    if (errors[currentStep][name].length === 0) { return null }
    return errors[currentStep][name].join(', ')
  }


  const componentorise = (c: Component, columns: number) => {
    let dependantValueMatched = false
    if (c.dependentOn) {
      const dependentComponent = componentAtPath(c.dependentOn.key)
      const dependantComponentValue = valueForPath(dependentComponent.path)
      dependantValueMatched = dependantComponentValue === c.dependentOn.value
      if (c.dependentOn.type !== "disable" && (!dependantComponentValue || !dependantValueMatched)) {
        return
      }
    }

    let component

    switch (c.type) {
      case "text": {
        component = <DefaultTextInput
          type={"text"}
          placeholder={c.title}
          value={`${formValues[c.name.name]?.value ?? ''}`}
          setValue={(val) => setValueForComponent(c.name, val)}
          errorText={refineErrors(c.name.name)}
        />
        break
      }

      case "number": {
        let val = 0
        if (formValues[c.name.name]?.value) {
          val = parseInt(formValues[c.name.name].value)
        }

        component = (
          <NumberSelector
            title={c.title}
            errorText={refineErrors(c.name.name)}
            onValueUpdated={(value) => setValueForComponent(c.name, `${value}`)}
            value={val}
            valueTitle={c.vocab?.singular ?? ''}
            valueTitlePlural={c.vocab?.plural ?? ''}
          />
        )
        break
      }

      case "phone": {
        component = <DefaultTextInput
          type={"number"}
          placeholder={c.title}
          value={`${formValues[c.name.name]?.value ?? ''}`}
          setValue={(val) => setValueForComponent(c.name, val)}
          errorText={refineErrors(c.name.name)}
        />
        break
      }

      case "email": {
        component = <DefaultTextInput
          type={"email"}
          placeholder={c.title}
          value={`${formValues[c.name.name]?.value ?? ''}`}
          setValue={(val) => setValueForComponent(c.name, val)}
          errorText={refineErrors(c.name.name)}
        />
        break
      }

      case "money": {
        component = <DefaultTextInput
          type={"money"}
          placeholder={c.title}
          value={`${formValues[c.name.name]?.value ?? ''}`}
          setValue={(val) => setValueForComponent(c.name, val)}
          errorText={refineErrors(c.name.name)}
        />
        break
      }

      case "date": {
        if (c.name.name === "DateOfBirth" && !formValues[c.name.name]?.value && formValues.IdentityNumber?.value) {
          const idNumber = formValues.IdentityNumber.value
          const validDecades = ["2", "3", "4", "5", "6", "7", "8", "9"]
          if (validDecades.includes(idNumber.slice(0, 1))) {
            const dob = `19${idNumber.slice(0, 2)}-${idNumber.slice(2, 4)}-${idNumber.slice(4, 6)}`
            setValueForComponent(c.name, dob)
          }
        }

        component = (
          <DateField
            title={c.title}
            value={formValues[c.name.name]?.value ?? ''}
            onPressed={() => setShowDatePicker(c.name.name)}
          />
        )
        break
      }

      case "radio": {
        component = (
          <DefaultHorizontalRadioGroup
            title={c.title}
            radioList={c.options?.map(o => ({id: o.value, checked: false, label: o.title})) ?? []}
            radioListLookUp={c.optionsLookUp}
            onChange={(val) => setValueForComponent(c.name, val.id)}
            value={formValues[c.name.name]?.value}
          />
        )
        break
      }

      case "checkbox": {
        let options: Option[]|undefined = c.options
        if (!options && c.optionsLookUp) {
        // @ts-ignore
          options = lookUp[c.optionsLookUp].map((o: LookUpData) => ({ title: o.Description, value: `${o.Id}` }))
        }
        component = (
          <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={{display: 'flex', flexDirection: 'row', marginTop: 20}}
          >
            {options?.map(option => {
              return (
                <View style={{marginRight: 40}} key={option.title}>
                  <DefaultCheckBox
                    isChecked={formValues[c.name.name]?.value.toLowerCase() === "true"}
                    setChecked={(isChecked) => setValueForComponent(c.name, isChecked ? 'true' : 'false')}
                    text={option.title}
                  />
                </View>
              )
            })}
          </ScrollView>
        )
        break
      }

      case "checkbox_postalasphysical": {
        let options: Option[]|undefined = c.options
        if (!options && c.optionsLookUp) {
          // @ts-ignore
          options = lookUp[c.optionsLookUp].map((o: LookUpData) => ({ title: o.Description, value: `${o.Id}` }))
        }
        component = (
          <View style={{marginBottom: 20}}>
            {options?.map(option => {
              return (
                <View style={{marginRight: 40}} key={option.title}>
                  <DefaultCheckBox
                    isChecked={formValues[c.name.name]?.value.toLowerCase() === "true"}
                    setChecked={(isChecked) => {
                      setValueForComponent(c.name, isChecked ? 'true' : 'false')
                      if (isChecked) {
                        setValueForComponent({ name: "PostalAddressLine1", type: "string" }, formValues["PhysicalAddressLine1"]?.value ?? "")
                        setValueForComponent({ name: "PostalAddressLine2", type: "string" }, formValues["PhysicalAddressLine2"]?.value ?? "")
                        setValueForComponent({ name: "PostalAddressLine3", type: "string" }, formValues["PhysicalAddressLine3"]?.value ?? "")
                        setValueForComponent({ name: "PostalCode", type: "string" }, formValues["PhysicalAddressPostalCode"]?.value ?? "")
                        setValueForComponent({ name: `PostalCodePostalCountryId`, type: "string" }, formValues[`PhysicalAddressPostalCodePostalCountryId`]?.value)
                        setValueForComponent({ name: `PostalCodePostalTownId`, type: "string" }, formValues[`PhysicalAddressPostalCodePostalTownId`]?.value)
                        setValueForComponent({ name: `PostalCodePostalCodeId`, type: "string" }, formValues[`PhysicalAddressPostalCodePostalCodeId`]?.value)
                      }
                    }}
                    text={option.title}
                  />
                </View>
              )
            })}
          </View>
        )
        break
      }

      case "uploadbutton": {
        component = (
          <View style={{marginRight: 40}} key={c.name.name}>
            <DefaultCheckBox
              isChecked={(formValues[c.name.name]?.value.toLowerCase() ?? "false") === "true"}
              setChecked={() => {
                setShowDocumentUploader(c.name.name)
              }}
              text={c.options![0].title}
            />
          </View>
        )
        break
      }

      case "expanding_cell": {
        const subcomponents = []

        for (let i = 0; i < (c.children?.length ?? 0); i++) {
          subcomponents.push(componentorise(c.children![i], 1))
        }

        component = (
          <ExpandingContentCell
            title={c.title}
            subTitle={c.details!.subtitle}
            description={c.details?.description}
            iconType={c.details!.icon}
          >
            {subcomponents}
          </ExpandingContentCell>
        )
        break
      }

      case "vertical_radio": {
        component = (
          <DefaultVerticalRadioGroup
            title={c.details!.leftLabel}
            rightTitle={c.details!.rightLabel}
            radioList={c.options?.map(o => ({
              id: o.value,
              checked: false,
              label: o.title,
              hasRightLabel: true,
              rightLabel: o.extra!,
            })) ?? []}
            radioListLookUp={c.optionsLookUp}
            value={formValues[c.name.name]?.value ?? ''}
            onChange={(group) => setValueForComponent(c.name, group.id)}
          />
        )
        break
      }

      case "button": {
        switch (c.action) {
          case "submit": {
            const isDisabled = c.dependentOn ? !dependantValueMatched : false
            component = (
              <View style={{marginTop: 40, alignItems: "flex-end", justifyContent: "flex-end", flex: 1}}>
                <DefaultButton buttonType={"default"} title={c.title} disabled={isDisabled} onPress={submitForm}/>
              </View>
            )
            break
          }
          case "saveAndExit": {
            component = (
              <View style={{marginTop: 40}}>
                <DefaultButton buttonType={"smallLight"} title={c.title} disabled={false} onPress={saveAndExit} width={140}/>
              </View>
            )
            break
          }
          case "documentUploader": {
            component = (
              <View style={{marginTop: 40}}>
                <DefaultButton
                  buttonType={"smallLight"}
                  title={c.title}
                  disabled={false}
                  onPress={() => {
                    if (isOnline) {
                      setShowDocumentUploader('')
                    } else {
                      showOfflineAlert()
                    }
                  }}
                  width={210}
                />
              </View>
            )
          }
        }
        break
      }

      case "repayment_period": {
        let divisionError: string | null = null
        let repaymentError: string | null = null
        if (formValues.DivisionId === undefined || (formValues.DivisionId && formValues.DivisionId.value.length === 0)) {
          divisionError = "Please select a division"
        }
        if (formValues.RepaymentPeriodId === undefined || (formValues.RepaymentPeriodId && formValues.RepaymentPeriodId.value.length === 0)) {
          repaymentError = "Please select a repayment period"
        }

        return (
          <Column columns={columns} key={c.path} isDropDown={true}>
            <RepaymentPeriod
              idNumber={formData?.IdentityNumber?.value}
              detailsSelected={(division, repaymentPeriodId) => {
                setValueForComponent({ name: "DivisionId", type: "int" }, division)
                setValueForComponent({ name: "RepaymentPeriodId", type: "int" }, repaymentPeriodId)
              }}
              divisionValue={formValues.DivisionId?.value}
              repaymentPeriodValue={formValues.RepaymentPeriodId?.value}
              divisionErrorText={showErrors ? divisionError : null}
              repaymentPeriodErrorText={showErrors ? repaymentError : null}
            />
          </Column>
        )
      }

      case "blank": {
        component = <View/>
        break
      }

      case "postalcode": {
        component = <ReadOnlyField
          title={"Postal Code"}
          value={formValues[c.name.name]?.value ?? ''}
          onPressed={async () => setShowPostalCodeModal(c.name.name)}
          errorText={refineErrors(c.name.name)}
        />
        break
      }

      case "signature": {
        component = <SignatureField
          value={formValues.PIRCustomerSignature?.value ?? ''}
          onPress={() => setShowSignatureModal(true)}
          errorText={refineErrors(c.name.name)}
        />
        break
      }

      case "dropdown": {
        let value = ''
        if (formValues[c.name.name]?.value && c.optionsLookUp) {
          // @ts-ignore
          const luData = lookUp[c.optionsLookUp].filter(data => `${data.Id}` === formValues[c.name.name]?.value)
          if (luData.length > 0) {
            value = luData[0].Description
          } else {
            setValueForComponent(c.name, '')
          }
        }
        component = (
          <ReadOnlyField
            title={c.title}
            value={value}
            onPressed={() => setShowLookUpSelector({ value: c.name.name, name: c.optionsLookUp ?? '' })}
            errorText={refineErrors(c.name.name)}
          />
        )
        break
      }

      case "timeperiod": {
        component = (
          <TimePeriodSelector
            title={c.title}
            onValueChanged={(years: number, months: number) => {
              const y = years < 10 ? `0${years}` : `${years}`
              const m = months < 10 ? `0${months}` : `${months}`
              setValueForComponent(c.name, `${y}${m}`)
            }}
            errorText={refineErrors(c.name.name)}
          />
        )
        break
      }
    }

    let key = c.path
    if (c.path === undefined) {
      key = uuid()
    }

    return (
      <Column columns={columns} key={key} isDropDown={false}>
        {component}
      </Column>
    )
  }

  const currentPage = currentStep - 1
  const totalSections = form.pages[currentPage].sections.length ?? 0

  const sections = (form.pages[currentPage].sections ?? []).map((section, sectionIdx) => {
    const rows = (section.components ?? []).map((row, idx) => {
      const columns = row.map(component => {
        return componentorise(component, row.length)
      })

      return (
        <Row key={idx} zIndex={(row.length + 10) - idx}>
          {columns}
        </Row>
      )
    })
    return (
      <Section key={sectionIdx} zIndex={(totalSections + 10) - sectionIdx}>
        <View style={{marginBottom: 34}}>
          <PageTitle
            titleType={'pageTitle'}
            textAlign={'left'}
            title={section.title}
            subtitle={section.subtitle}
            key={section.title}
          />
        </View>
        {rows}
      </Section>
    )
  })


  return (
    <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} keyboardVerticalOffset={70}>
      <Container contentContainerStyle={{ paddingBottom: 200 }}>
        {form.title && <PageTitle title={form.title} titleType={'largeRed'} textAlign={'left'}/>}
        <StepsSubNav
          currentStep={currentStep}
          totalSteps={form.pages.length}
          title={form.pages[currentPage].subtitle}
          description={form.pages[currentPage].description}
          onNext={nextStep}
          onBack={prevStep}
        />
        {sections}

        {showModal && <ApplicationRequirementsModal>{termsModal.current}</ApplicationRequirementsModal>}


        <DefaultModal
          visible={showPostalCodeModal !== null}
          title={"Search Postal Codes"}
          headerType={"small"}
          showBottomButtons={false}
          width={"650px"}
          height={"480px"}
        >
          <SearchPostalCodesModal
            onSelectedPostalCode={(code) => {
              setValueForComponent({ name: showPostalCodeModal!, type: "string" }, `${code.code}`)
              setValueForComponent({ name: `${showPostalCodeModal}PostalCountryId`, type: "string" }, `${code.countryId}`)
              setValueForComponent({ name: `${showPostalCodeModal}PostalTownId`, type: "string" }, `${code.townId}`)
              setValueForComponent({ name: `${showPostalCodeModal}PostalCodeId`, type: "string" }, `${code.codeId}`)
              setShowPostalCodeModal(null)
            }}
          />
        </DefaultModal>

        <DefaultModal
          visible={showSignatureModal}
          title={"Signature"}
          headerType={"small"}
          showBottomButtons={false}
          width={"720px"}
          height={"350px"}
        >
          <CaptureSignature
            onCancelButtonPressed={() => setShowSignatureModal(false)}
            onActionButtonPressed={(base64) => {
              const sanitised = base64.replace(/\r?\n|\r/g, "")
              setValueForComponent({ name: "PIRCustomerSignature", type: "string" }, sanitised)
              setShowSignatureModal(false)
            }}
          />
        </DefaultModal>

        <DefaultModal
          visible={showDocumentUploader !== null}
          title={"Supporting Documents"}
          headerType={"large"}
          showBottomButtons={true}
          height={"600px"}
          width={"600px"}
          actionButtonTitle={"Close"}
          onActionButtonPressed={() => setShowDocumentUploader(null)}
        >
          <UploadDocuments
            communicationQueueId={submittableForm().CommunicationQueueId}
            documentUploaded={(isSuccess) => {
              if (showDocumentUploader!.length === 0) { return }
              setValueForComponent({ name: "ProofOfIncomeProvidedUpfront", type: "bool" }, isSuccess ? "true" : "false")
            }}
          />
        </DefaultModal>

        <DateTimePickerModal
          isVisible={showDatePicker !== null}
          mode={"date"}
          date={(showDatePicker) ? (formValues[showDatePicker]?.value ? new Date(formValues[showDatePicker].value) : (new Date())) : (new Date())}
          onConfirm={(date) => {
            setValueForComponent({ name: showDatePicker!, type: "date" }, date.toISOString().slice(0, 10))
            setShowDatePicker(null)
          }}
          onCancel={() => setShowDatePicker(null)}
        />

        <DefaultModal
          visible={showLookUpSelector !== null}
          title={"Select a value"}
          headerType={"small"}
          showBottomButtons={false}
          height={'500px'}
          width={'576px'}
        >
          <ListSelector
            lookUp={showLookUpSelector?.name ?? ''}
            onValueSelected={(value) => {
              setValueForComponent({ type: "string", name: showLookUpSelector?.value ?? '' }, `${value.Id}`)
              setShowLookUpSelector(null)
            }}
          />
        </DefaultModal>
      </Container>
    </KeyboardAvoidingView>
  )
}


const Container = styled.ScrollView`
  padding: 0px 21px 21px 21px;
`

const Section = styled.View<{ zIndex: number }>`
  margin-top: 64px;
  z-index: ${props => props.zIndex};
`

const ApplicationRequirementsModal = styled.View`
  width: 100%; 
  height: 100%; 
  position: absolute; 
  top: 0; 
  left: 0;
  background-color: #FFFFFF; 
  z-index: 100;
`
