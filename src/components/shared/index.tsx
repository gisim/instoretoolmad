import styled from "styled-components/native"

export type FormErrors = { [key: string]: string }

export const Container = styled.ScrollView`
  padding: 0 21px 21px 21px;
`

export const LoadingContainer = styled.View`
  display: flex;
  align-items: center;
  justify-content: center;
`
