import {ValueType} from "./form-service"

export const sanitizeCustomerInfo = (details: { [key: string]: any }): { [key: string]: ValueType } => {
  const customerDetailKeys = Object.keys(details)
  const customerDetails: { [key: string]: ValueType } = {}
  for (let cdi = 0; cdi < customerDetailKeys.length; cdi++) {
    let key = customerDetailKeys[cdi]
    if (details[key] === null || details[key] === "null") { continue }
    let value = `${details[key]}`
    if (key === "DateOfBirth" && value.length > 10 && value.includes("T")) {
      value = value.slice(0, 10)
    }

    key = key
      .replace("PostAddress", "PostalAddress")
      .replace("StreetAddress", "PhysicalAddress")
      .replace("HomePhone", "HomeTelephone")
      .replace("WorkPhone", "WorkTelephone")
      .replace("MobileNumber", "CellphoneNumber")

    customerDetails[key] = { value, type: "string" }
  }
  return customerDetails
}

export const sanitizeApplicationData = (details: { [key: string]: string }): { [key: string]: ValueType } => {
  const keys = Object.keys(details)
  const newFormData: { [key: string]: ValueType } = {}
  for (let i = 0; i < keys.length; i++) {
    const value = details[keys[i]]
    const key = keys[i]
      .replace("FamilyFuneralId", "FamilyFuneralPlan")
      .replace("CustomerProtection", "CustomerProtectionPlan")
      .replace("LostCardProtection", "LostCardProtectionPlan")
      .replace("ThreeSixtyDegreeCPPId", "ThreeSixtyCustomerProtectionPlan")

    newFormData[key] = { value, type: "string" }
  }
  return newFormData
}
