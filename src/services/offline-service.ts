import {database} from "./db-service"
import {Q} from "@nozbe/watermelondb"
import {OfflineApplication} from "../models/db/application"
import NetInfo from '@react-native-community/netinfo'

export const uploadOfflineApplicationsMutation = async () => {
  const netInfo = await NetInfo.fetch()

  if (!netInfo.isConnected || !netInfo.isInternetReachable) {
    return Promise.resolve(null)
  }

  const applications = await getApplicationsQuery('desc', Q.asc)
  if (applications.length === 0) { return Promise.resolve() }

  for (let i = 0; i < applications.length; i++) {
    const response = await retryOffline(applications[i])
    if (response === 200) {
      await removeOfflineApplication(applications[i].name)
    }
  }
}

export const getApplicationsQuery = async (key: string, sortOrder: "asc" | "desc" = Q.desc) => {
  const applicationCollection = database.collections.get('applications')
  const allApplications = await applicationCollection.query(
    Q.experimentalSortBy("created_at", sortOrder),
  ).fetch()
  return allApplications as OfflineApplication[]
}

export const getApplicationsCountQuery = async (): Promise<number> => {
  const applicationCollection = database.collections.get('applications')
  return await applicationCollection.query().fetchCount()
}

const retryOffline = async (application: OfflineApplication): Promise<number> => {
  const response = await fetch(application.endpoint, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: application.data,
  })
    .then((resp) => resp.status)

  console.log('status', response)
  return response
}

export async function removeOfflineApplication(name: string) {
  const offlineApplicationCollection = database.collections.get('applications')
  const offlineApplications = await offlineApplicationCollection.query(Q.where('name', name)).fetch()

  await database.action(async () => {
    await offlineApplications.forEach(app => {
      app.destroyPermanently()
      console.log(`Removing application:`, name)
    })
  })
}
