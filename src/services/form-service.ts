import {Component, Form, FormNameType, FormFieldName} from "../models/form"
import {useState, useRef, useEffect} from "react"

export enum FormType {
  New = 'new-application',
  Complete = 'complete-application'
}

const formForType = (formType: FormType): Form => {
  switch (formType) {
    case FormType.Complete: {
      return require(`../forms/complete-application.json`)
    }
    case FormType.New: {
      return require(`../forms/new-application.json`)
    }
  }
}


export type FormFieldErrorType = { [key: string]: string[] }

export type FormService = {
  form: Form
  valueForPath: (key: string) => string | number | null
  componentAtPath: (path: string) => Component
  setValueForComponent: (name: FormFieldName, value: string) => void
  submittableForm: () => { [key: string]: any }
  formValues: { [key: string]: ValueType }
  errors: FormFieldErrorType[]
}

export type ValueType = {
  type: FormNameType
  value: string
}

export const useForm = (formType: FormType): FormService => {
  const [formValues, setFormValues] = useState<{ [key: string]: ValueType }>({})
  const [errors, setErrors] = useState<FormFieldErrorType[]>([])
  const hasCompletedSetup = useRef(false)
  const form = useRef(formForType(formType))

  useEffect(() => {
    if (hasCompletedSetup.current) { return }
    form.current.pages.forEach((page, pageIdx) => {
      page.sections?.forEach((section, sectionIdx) => {
        section.components?.forEach((row, rowIdx) => {
          row.forEach((column, columnIdx) => {
            if (form.current.pages[pageIdx].sections[sectionIdx].components) {
              const path = `${pageIdx}/${sectionIdx}/${rowIdx}/${columnIdx}`
              form.current.pages[pageIdx].sections[sectionIdx].components![rowIdx][columnIdx].path = path

              // Ensures all non-form components like titles, buttons etc are not required. This speeds things up upon validation.
              if (form.current.pages[pageIdx].sections[sectionIdx].components![rowIdx][columnIdx].name === undefined) {
                form.current.pages[pageIdx].sections[sectionIdx].components![rowIdx][columnIdx].required = false
              } else {
                if (form.current.pages[pageIdx].sections[sectionIdx].components![rowIdx][columnIdx].required === undefined) {
                  form.current.pages[pageIdx].sections[sectionIdx].components![rowIdx][columnIdx].required = true
                }
              }
            }
          })
        })
      })
    })
    hasCompletedSetup.current = true
  })

  useEffect(() => {
    if (!hasCompletedSetup) { return }
    if (isEmpty(formValues)) { return }
    checkForErrors()
  }, [formValues])

  const componentAtPath = (path: string): Component => {
    const indicies = path.split("/")
    const pageIdx = parseInt(indicies[0])
    const sectionIdx = parseInt(indicies[1])
    const rowIdx = parseInt(indicies[2])
    const columnIdx = parseInt(indicies[3])

    return form.current.pages[pageIdx].sections[sectionIdx].components![rowIdx][columnIdx]
  }

  const valueForPath = (key: string): string | number | null => {
    const field = componentAtPath(key)
    if (field.name && field.name.name in formValues) {
      return formValues[field.name.name].value
    }
    return null
  }

  const checkForErrors = () => {
    for (let pageIdx = 0; pageIdx < form.current.pages.length; pageIdx++) {
      for (let sectionIdx = 0; sectionIdx < form.current.pages[pageIdx].sections.length; sectionIdx++) {
        for (let rowIdx = 0; rowIdx < (form.current.pages[pageIdx].sections[sectionIdx].components?.length ?? -1); rowIdx++) {
          for (let columnIdx = 0; columnIdx < form.current.pages[pageIdx].sections[sectionIdx].components![rowIdx].length; columnIdx++) {
            const column = form.current.pages[pageIdx].sections[sectionIdx].components![rowIdx][columnIdx]
            checkComponentForErrors((pageIdx + 1), column)
          }
        }
      }
    }
  }

  const checkComponentForErrors = (page: number, component: Component) => {
    if (component.name === undefined) { return }

    if (component.required) {
      if (formValues[component.name.name] !== undefined) {
        if (formValues[component.name.name].value.length === 0) {
          setErrorForComponent(page, component, `Required`)
        } else {
          removeErrorForComponent(page, component, `Required`)
        }
      } else {
        setErrorForComponent(page, component, `Required`)
      }
    }

    if (formValues[component.name.name] === undefined || formValues[component.name.name].value.length === 0) {
      return
    }

    const fieldValue = formValues[component.name.name].value

    switch (component.name.type) {
      case "int":
      case "float": {
        if (!/^([\d.]+)$/.test(fieldValue)) {
          setErrorForComponent(page, component, 'Invalid value')
        } else {
          removeErrorForComponent(page, component, 'Invalid value')
        }
        break
      }
    }

    switch (component.type) {
      case "email": {
        if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(fieldValue)) {
          setErrorForComponent(page, component, 'Invalid email address')
        } else {
          removeErrorForComponent(page, component, 'Invalid email address')
        }
        break
      }

      case "phone": {
        // @ts-ignore because isNaN does actually work with strings
        if (isNaN(fieldValue)) {
          setErrorForComponent(page, component, 'Invalid value')
        } else {
          removeErrorForComponent(page, component, 'Invalid value')
        }

        if (fieldValue.length < 10 || fieldValue.length > 20) {
          setErrorForComponent(page, component, `Invalid phone number`)
        } else {
          removeErrorForComponent(page, component, 'Invalid phone number')
        }

        break
      }
    }

    if (component.name && component.name.name) {
      switch (component.name.name) {
        case "EmployeeNumber": {
          if (!/^[a-z0-9]+$/i.test(fieldValue)) {
            setErrorForComponent(page, component, `Only letters and numbers allowed`)
          } else {
            removeErrorForComponent(page, component, 'Only letters and numbers allowed')
          }
          break
        }
      }
    }
  }

  const setErrorForComponent = (page: number, component: Component, error: string) => {
    setErrors(prevState => {
      const updatedValues = prevState.slice(0)

      if (updatedValues[page] === undefined) {
        updatedValues[page] = {}
      }

      if (updatedValues[page][component.name.name] === undefined) {
        updatedValues[page][component.name.name] = [error]
      } else {
        if (updatedValues[page][component.name.name].includes(error) === false) {
          updatedValues[page][component.name.name].push(error)
        }
      }

      return updatedValues
    })
  }

  const removeErrorForComponent = (page: number, component: Component, error: string) => {
    setErrors(prevState => {
      const updatedValues = prevState.slice(0)

      if (updatedValues[page] === undefined) {
        return prevState
      }

      if (updatedValues[page][component.name.name] === undefined) {
        return prevState
      }

      if (updatedValues[page][component.name.name].includes(error)) {
        updatedValues[page][component.name.name] = updatedValues[page][component.name.name].filter(existingError => existingError !== error)
      }

      if (updatedValues[page][component.name.name].length === 0) {
        delete updatedValues[page][component.name.name]
      }

      return updatedValues
    })
  }

  const setValueForComponent = (name: FormFieldName, value: string) => {
    setFormValues(prevState => {
      const updatedValues: { [key: string]: ValueType } = {}
      updatedValues[name.name] = {value, type: name.type}
      return {...prevState, ...updatedValues}
    })
  }

  const submittableForm = () => {
    const keys = Object.keys(formValues)
    const transformedForm: { [key: string]: any } = {}
    for (let i = 0; i < keys.length; i++) {
      if (formValues[keys[i]].value === null) {
        transformedForm[keys[i]] = ""
      } else {
        switch (formValues[keys[i]].type) {
          case "string": {
            transformedForm[keys[i]] = formValues[keys[i]].value.trim()
            break
          }
          case "int": {
            transformedForm[keys[i]] = parseInt(formValues[keys[i]].value)
            break
          }
          case "float": {
            transformedForm[keys[i]] = parseFloat(formValues[keys[i]].value.replace(",", "."))
            break
          }
          case "date": {
            const date = new Date(formValues[keys[i]].value)
            transformedForm[keys[i]] = date.toISOString()
            break
          }
          case "bool": {
            switch (formValues[keys[i]].value) {
              case "1":
              case "true":
              case "True":
              case "TRUE":
                transformedForm[keys[i]] = "TRUE"
                break
              default:
                transformedForm[keys[i]] = "FALSE"
            }
            break
          }
          default: {
            console.log(`Unknown: ${formValues[keys[i]]}`)
            break
          }
        }
      }
    }
    return transformedForm
  }

  return {form: form.current, componentAtPath, valueForPath, setValueForComponent, submittableForm, formValues, errors}
}

export const hasErrors = (page: number, errors: FormFieldErrorType[]): boolean => {
  if (errors[page] === undefined) { return false }
  return !isEmpty(errors[page])
}

const isEmpty = (object: { [key: string]: any }): boolean => {
  return Object.keys(object).length === 0 && object.constructor === Object
}
