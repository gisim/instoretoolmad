import { Database } from '@nozbe/watermelondb'
import SQLiteAdapter from '@nozbe/watermelondb/adapters/sqlite'
import schema from '../models/db/schema'
import {Log} from '../models/db/log'
import migrations from "../models/db/migrations"
import {OfflineApplication} from "../models/db/application"
import {PostalCode} from "../models/db/postal-code"

const adapter = new SQLiteAdapter({
  schema,
  dbName: 'mrp-money-instore-tool',
  migrations,
})

export const database = new Database({
  adapter,
  modelClasses: [
    Log,
    OfflineApplication,
    PostalCode,
  ],
  actionsEnabled: true,
})

