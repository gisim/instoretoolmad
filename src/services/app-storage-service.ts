import {LookUp} from "../models/lookup-data"
import {getAllLookupDataQuery, getApplicationFormModelQuery} from "./api-service"
import {ActiveStore} from "../models/active-store"
import AsyncStorage from "@react-native-community/async-storage"
import {SessionKeys} from "./session-keys"
import {ApplicationFormModel} from "../models/application-form-model"
import { LoginResponse, User } from "../models/login-response"
import {database} from "./db-service"

class PrivateAppStorageService {
  // @ts-ignore
  public lookUp: LookUp
  // @ts-ignore
  public activeStoreId: number

  start() {
    const lookUpPromise = getAllLookupDataQuery()
      .then(lookUpData => {
        this.lookUp = lookUpData
        console.log('AppStorageService: started')
        return lookUpData
      })
      .catch(error => {
        console.log('App Storage Service: lookUpPromise error', error)
      })

    const getApplicationFormModelData = getApplicationFormModelQuery()
      .then(data => {
        this.setApplicationFormModel(data)
        return data
      })
      .catch(error => {
        console.log('App Storage Service: getApplicationFormModelData error', error)
      })

    const setActiveStoreProp = this.getActiveStore()
      .then((store) => {
        if (!store) { return }
        this.activeStoreId = store.StoreId
      })
      .catch(error => {
        console.log('App Storage Service: setActiveStoreProp error', error)
      })

    return Promise.all([
      lookUpPromise,
      getApplicationFormModelData,
      setActiveStoreProp,
      this.runPostalCodeMigration(),
    ])
  }

  /**
   * User / Login
   */
  async getUser(): Promise<User> {
    const json = await AsyncStorage.getItem(SessionKeys.login)
    if (!json) {
      return Promise.reject(new Error('User not logged in'))
    }
    const response = JSON.parse(json) as LoginResponse
    return response.User
  }

  /**
   * Log out / Reset
   */
  reset() {
    return Promise.all([
      AsyncStorage.removeItem(SessionKeys.activeStore),
      AsyncStorage.removeItem(SessionKeys.activeStaffCode),
      AsyncStorage.removeItem(SessionKeys.login),
      AsyncStorage.removeItem(SessionKeys.claims),
    ])
    .catch(console.error)
  }

  /**
   * Active Store
   */
  setActiveStore(store: ActiveStore) {
    AsyncStorage.setItem(SessionKeys.activeStore, JSON.stringify(store))
    this.activeStoreId = store.StoreId
  }

  async getActiveStore(): Promise<ActiveStore> {
    const store = await AsyncStorage.getItem(SessionKeys.activeStore)
    if (!store) {
      return Promise.reject(new Error("Store not set"))
    }
    return JSON.parse(store) as ActiveStore
  }

  /**
   * Active Staff Code
   */
  setActiveStaffCode(staffCode: string) {
    AsyncStorage.setItem(SessionKeys.activeStaffCode, staffCode)
  }

  getActiveStaffCode(): Promise<string|null> {
    return AsyncStorage.getItem(SessionKeys.activeStaffCode)
  }

  /**
   * Last Store Code Input
   */
  setLastStoreCodeInput(input: string) {
    AsyncStorage.setItem(SessionKeys.lastStoreCodeInput, input)
  }

  getLastStoreCodeInput(): Promise<string|null> {
    return AsyncStorage.getItem(SessionKeys.lastStoreCodeInput)
  }

  /**
   * Look Up Data
   */
  private setLookUpData(data: LookUp) {
    AsyncStorage.setItem(SessionKeys.lookUpData, JSON.stringify(data))
  }

  async getLocalLookUpData(): Promise<LookUp> {
    const lookUpData = await AsyncStorage.getItem(SessionKeys.lookUpData)
    if (!lookUpData) {
      return Promise.reject(new Error('No lookup data exists'))
    } else {
      const localLookUp = JSON.parse(lookUpData) as LookUp
      this.lookUp = localLookUp
      return Promise.resolve(localLookUp)
    }
  }

  /**
   * Application Form Model
   */
  private setApplicationFormModel(data: ApplicationFormModel) {
    AsyncStorage.setItem(SessionKeys.applicationFormModel, JSON.stringify(data))
  }

  async getApplicationFormModel(): Promise<ApplicationFormModel> {
    const jsonData = await AsyncStorage.getItem(SessionKeys.applicationFormModel)
    return JSON.parse(jsonData!) as ApplicationFormModel
  }

  /**
   * Postal Codes
   */
  private async runPostalCodeMigration() {
    console.log('Checking if postal code migration is necessary…')
    const postalCodesCollection = database.collections.get('postal_codes')
    const postalCodesCount = await postalCodesCollection.query().fetchCount()
    if (postalCodesCount > 0) {
      console.log('Postal code migration not required')
      return
    }
    console.log('Running postal code migration…')
    const start = new Date()

    const localPostalCodes: { [key: string]: any }[] = require('../assets/data/postalcodes.json')

    database.action(async () => {
      await database.batch(
        ...localPostalCodes.map(c => postalCodesCollection.prepareCreate(pc => {
            // @ts-ignore
            pc.codeId = c.codeId
            // @ts-ignore
            pc.townId = c.townId
            // @ts-ignore
            pc.countryId = c.countryId
            // @ts-ignore
            pc.code = c.code
            // @ts-ignore
            pc.suburb = c.suburb
            // @ts-ignore
            pc.town = c.town
        }))
      )
    })

    const end = new Date()
    // @ts-ignore
    console.log(`Postal code migration complete! Took: ${(end - start) / 1000} seconds`)
  }
}

export const AppStorageService = new PrivateAppStorageService()
