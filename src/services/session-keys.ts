export enum SessionKeys {
  login = "mrpm.login",
  claims = "mrpm.claims",
  activeStore = "mrpm.activestore",
  activeStaffCode = "mrpm.activestorecode",
  lookUpData = "mrpm.lookupdata",
  applicationFormModel = "mrpm.applicationformmodel",

  lastStoreCodeInput = "mrpm.laststorecodeinput",
}
