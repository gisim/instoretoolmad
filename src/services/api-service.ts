import {LoginResponse} from "../models/login-response"
import {GetClaimsResponse} from "../models/get-claims-response"
import AsyncStorage from "@react-native-community/async-storage"
import {ActiveDivision} from "../models/active-divison"
import {LookUp} from "../models/lookup-data"
import {ValidateIDNumberResponse} from "../models/validate-id-number-response"
import {ActiveStore} from "../models/active-store"
import {SessionKeys} from "./session-keys"
import {GetCustomerApplicationsResponse} from "../models/get-customer-applications-response"
import {GetAvailableAccountOptionsResponse} from "../models/get-available-account-options-response"
import { PerformBureauCheckResponse } from "../models/perform-bureau-check-response"
import {ApplicationFormModel} from "../models/application-form-model"
import {SaveAndContinueModel} from "../models/save-and-continue"
import {PostalCodes} from "../models/search-postal-codes-response"
import { FinalOutcome } from "../models/final-outcome"
import {UploadedDocuments} from "../models/uploaded-documents"
import CookieManager from '@react-native-community/cookies'
import 'react-native-get-random-values'
import {v4 as uuid} from 'uuid'
import { database } from "./db-service"
import {removeOfflineApplication} from './offline-service'
import { logResponse } from "./debugger-service"
import {Q} from "@nozbe/watermelondb"
import {LinkCardData} from "../models/link-card-data"
import {ProcessLinkAccountRequest} from "../models/process-link-account-request"
import { ProcessLinkAccountResponse } from "../models/process-link-account-response"
import {GetPreAgreementDataRequest} from "../models/get-pre-agreement-data-request"
import { GetPreAgreementDataResponse } from "../models/get-pre-agreement-data-response"
import {InsuranceAccount} from "../models/insurance-account"
import {GetInsuranceProductsResponse} from "../models/get-insurance-products-response"
import {GetInsuranceProductsRequest} from "../models/get-insurance-products-request"
import {UploadInsuranceDocumentRequest} from "../models/upload-insurance-document-request"
import {CreateFinancialServiceProductWithBeneficiaryRequest} from "../models/create-financial-product-request"
import {CreateFinancialServiceProductWithBeneficiaryResponse} from "../models/create-financial-product-response"
import {ExistingInsuranceProduct} from "../models/existing-insurance-product"

const BaseUrl = "ho-mpminte-dev"
const ApplicationID = "20"

enum APIServices {
  security = "mrpMoney.SecurityV3",
  financial = "FinancialServices.Debtors.WebService",
  c3po = "mrpMoney.C3PO.WS",
  c3poUi = "mrpMoney.C3PO.UI",
  customer = "mrpGroup.Customer.WS",
  insurance = "FinancialServices.Debtors.Hangfire.Webservice"
}

enum Endpoints {
  getInsuranceAccount         = "api/Account/GetAccount",
  getPostalCodeSearchResults  = "api/AutoComplete/GetPostalCodeSearchResults",
  performGet                  = "api/C3PO/PerformGet",
  getClaims                   = "api/Claims/GetUserClaim",
  getCustomerInfo             = "api/Customer/GetCustomerInfo",
  validateIdNumber            = "api/Customer/ValidateIdentityNumber",
  uploadDocument              = "api/Document/AddSupportingDocument",
  getUploadedDocuments        = "api/Document/GetDocumentDetails",
  getLinkCardData             = "api/FormData/GetLinkCardData",
  createNewApplication        = "api/Instore/CreateNewApplication",
  emailPreAgreement           = "api/Instore/EmailPreAgreement",
  getCustomerApplications     = "api/Instore/GetCustomerApplications",
  getInstoreApplicationData   = "api/Instore/GetInstoreApplicationData",
  createFinancialProduct      = "api/Insurance/CreateFinancialServiceProductWithBeneficiary_Multiple",
  getExistingProducts         = "api/Insurance/GetFinancialServicesProductsByAccountCode",
  getInsuranceDisclaimer      = "api/Insurance/GetInsuranceDisclaimer",
  getFinalOutcome             = "api/Instore/GetInstoreFinalOutcome",
  performBureauCheck          = "api/Instore/PerformBureauCheck",
  performPreScreening         = "api/Instore/PerformPrescreening",
  saveAndContinue             = "api/Instore/SaveAndContinueInstoreApplication",
  submitApplication           = "api/Instore/SaveInstoreApplicationDetail",
  getInsuranceProducts        = "api/Insurance/AccountInsuranceEligible",
  uploadInsuranceDocument     = "api/Insurance/UploadDocument",
  getProcessFlow              = "api/Process/GetProcessFlow",
  getActiveDivisions          = "api/MasterManagement/GetActiveDivisions",
  getActiveStores             = "api/MasterManagement/GetActiveStores",
  getAvailableAccountOptions  = "api/NewAccounts/GetAvailableAccountOptions",
  getAllLookUpData            = "api/Reference/GetAllLookupData",
  getPreAgreementData         = "api/Reference/GetPreAgreementData",
  processLinkAccount          = "api/Reference/ProcessLinkAccount",
  login                       = "api/User/UserAlternateSignIn",
}

const OfflineEndpoints = [Endpoints.saveAndContinue, Endpoints.submitApplication]

export const loginMutation = (params: { username: string, password: string }): Promise<LoginResponse> => {
  return CookieManager.clearAll()
    .then(() => {
      return get(APIServices.security, Endpoints.login, `${params.username}/${params.password}/${ApplicationID}`)
    })
    .then(resp => {
      AsyncStorage.setItem(SessionKeys.login, JSON.stringify(resp))
      return resp
    }) as Promise<LoginResponse>
}

export const getClaimsQuery = (params: { userId: string }): Promise<GetClaimsResponse> => {
  return get(APIServices.security, Endpoints.getClaims, `${params.userId}/${ApplicationID}`)
    .then(resp => {
      AsyncStorage.setItem(SessionKeys.claims, JSON.stringify(resp))
      return resp
    }) as Promise<GetClaimsResponse>
}

export const getActiveDivisionsQuery = (): Promise<ActiveDivision[]> => {
  return get(APIServices.financial, Endpoints.getActiveDivisions)
}

export const getActiveStoresQuery = (params: { query: string }): Promise<ActiveStore[]> => {
  return get(APIServices.financial, Endpoints.getActiveStores, params.query)
    .then((response) => {
      if (Array.isArray(response)) {
        return response
      } else {
        throw (response as any).error
      }
    })
}

export const getAllLookupDataQuery = (): Promise<LookUp> => {
  return get(APIServices.c3po, Endpoints.getAllLookUpData)
}

export const validateIdentityNumberQuery = (params: { idNumber: string }): Promise<ValidateIDNumberResponse> => {
  return get(APIServices.customer, Endpoints.validateIdNumber, `${params.idNumber}/1`)
}

export const getCustomerApplicationsQuery = (params: { idNumber: string, processId: number }): Promise<GetCustomerApplicationsResponse> => {
  return get(APIServices.c3po, Endpoints.getCustomerApplications, `${params.idNumber}/${params.processId}`)
}

export const getAvailableAccountOptions = (params: { idNumber: string }): Promise<GetAvailableAccountOptionsResponse> => {
  return get(APIServices.financial, Endpoints.getAvailableAccountOptions, params.idNumber)
}

export const createNewApplicationMutation = (params: { form: { [key: string]: any } }) => {
  return post(APIServices.c3po, Endpoints.createNewApplication, params.form) as Promise<{ [key: string]: any }>
}

export const performPreScreeningMutation = (params: { form: { [key: string]: any } }) => {
  return post(APIServices.c3po, Endpoints.performPreScreening, params.form) as Promise<{ [key: string]: any }>
}

export const performBureauCheckMutation = (params: { form: { [key: string]: any } }) => {
  return post(APIServices.c3po, Endpoints.performBureauCheck, params.form) as Promise<PerformBureauCheckResponse>
}

export const getApplicationFormModelQuery = (): Promise<ApplicationFormModel> => {
  return get(APIServices.c3po, Endpoints.getProcessFlow, "1")
}

export const saveAndContinueMutation =  (params: { model: SaveAndContinueModel }) => {
  return post(APIServices.c3po, Endpoints.saveAndContinue, params.model) as Promise<ApplicationFormModel>
}

export const getInstoreApplicationDataQuery = (params: { newApplicationId: string|number, communicationQueueId: string|number }) => {
  return get(APIServices.c3po, Endpoints.getInstoreApplicationData, `${params.communicationQueueId}/${params.newApplicationId}`) as Promise<{ [key: string]: string }>
}

export const searchPostalCodesQuery = (params: { postalCode: string }) => {
  return get(APIServices.c3po, Endpoints.getPostalCodeSearchResults, params.postalCode) as Promise<PostalCodes>
}

export const submitApplicationMutation = (params: { model: SaveAndContinueModel }) => {
  return post(APIServices.c3po, Endpoints.submitApplication, params.model) as Promise<ApplicationFormModel>
}

export const getFinalOutcomeQuery = (params: { communicationQueueId: string }) => {
  return get(APIServices.c3po, Endpoints.getFinalOutcome, params.communicationQueueId) as Promise<FinalOutcome>
}

export const uploadDocumentMutation = (params: { body: { [key: string]: any } }) => {
  return post(APIServices.c3po, Endpoints.uploadDocument, params.body, params.body.CommunicationQueueId) as Promise<UploadedDocuments>
}

export const getUploadedDocumentsQuery = (params: { communicationQueueId: string }) => {
  return get(APIServices.c3po, Endpoints.getUploadedDocuments, params.communicationQueueId) as Promise<UploadedDocuments>
}

export const getCustomerInfo = (params: { idNumber: string }) => {
  return get(APIServices.financial, Endpoints.getCustomerInfo, params.idNumber) as { [key: string]: any }
}

export const getLinkCardDataMutation = (params: { idNumber: string }) => {
  return get(APIServices.c3po, Endpoints.getLinkCardData, params.idNumber) as Promise<LinkCardData>
}

export const processLinkAccountMutation = (params: ProcessLinkAccountRequest) => {
  return post(APIServices.c3po, Endpoints.processLinkAccount, params) as Promise<ProcessLinkAccountResponse>
}

export const getPreAgreementDataMutation = (params: GetPreAgreementDataRequest) => {
  return post(APIServices.c3po, Endpoints.getPreAgreementData, params) as Promise<GetPreAgreementDataResponse>
}

export const emailPreAgreementMutation = (params: { communicationQueueId: number }) => {
  return get(APIServices.c3po, Endpoints.emailPreAgreement, `${params.communicationQueueId}`) as Promise<UploadedDocuments>
}

export const getInsuranceAccountQuery = (params: { storeCard: string }) => {
  return get(APIServices.insurance, Endpoints.getInsuranceAccount, `${params.storeCard}`) as Promise<InsuranceAccount>
}

export const getInsuranceProductsQuery = (params: GetInsuranceProductsRequest) => {
  return post(APIServices.insurance, Endpoints.getInsuranceProducts, params) as Promise<GetInsuranceProductsResponse>
}

export const getExistingInsuranceProductsQuery = (params: { accountCode: string }) => {
  return get(APIServices.insurance, Endpoints.getExistingProducts, `?AccountCode=${params.accountCode}`) as Promise<ExistingInsuranceProduct[]>
}

export const uploadInsuranceDocumentMutation = (params: UploadInsuranceDocumentRequest) => {
  return post(APIServices.insurance, Endpoints.uploadInsuranceDocument, [params]) as Promise<GetInsuranceProductsResponse>
}

export const getInsuranceDisclaimer = () => {
  return getText(APIServices.insurance, Endpoints.getInsuranceDisclaimer, `?DisclaimerId=1`)
}

export const createFinancialProduct = (params: CreateFinancialServiceProductWithBeneficiaryRequest[]) => {
  return post(APIServices.insurance, Endpoints.createFinancialProduct, params) as Promise<CreateFinancialServiceProductWithBeneficiaryResponse>
}




function get<T>(service: APIServices, endpoint: Endpoints, extra?: string): Promise<T> {
  let url = `http://${BaseUrl}/${service}/${endpoint}`
  if (extra) {
    url = `${url}/${extra}`
  }
  return fetch(url)
    .then(resp => resp.json())
    .then(json => logResponse(url, json)) as Promise<T>
}

function post<T>(service: APIServices, endpoint: Endpoints, body: { [key: string]: any }, extra?: string): Promise<T> {
  let url = `http://${BaseUrl}/${service}/${endpoint}`
  if (extra) {
    url = `${url}/${extra}`
  }

  let saveForOfflinePromise: Promise<void>
  let offlineApplicationName: string|null = null

  if (OfflineEndpoints.includes(endpoint)) {
    offlineApplicationName = uuid()
    saveForOfflinePromise = saveOffline(url, offlineApplicationName, body)
  } else {
    saveForOfflinePromise = Promise.resolve()
  }

  return saveForOfflinePromise
    .then(() => {
      return fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
      })
    })
    .then(resp => resp.json())
    .then(json => logResponse(url, json))
    .then((json) => {
      if (!offlineApplicationName) { return json }
      return removeOfflineApplication(offlineApplicationName)
        .then(() => {
          return json
        })
    }) as Promise<T>
}

function getText(service: APIServices, endpoint: Endpoints, extra?: string): Promise<string> {
  let url = `http://${BaseUrl}/${service}/${endpoint}`
  if (extra) {
    url = `${url}/${extra}`
  }
  return fetch(url)
    .then(resp => resp.text())
    .then(json => logResponse(url, json)) as Promise<string>
}

async function saveOffline(url: string, name: string, data: { [key: string]: any }) {
  const offlineApplicationCollection = database.collections.get('applications')
  let commQueueId: number|null = null

  if (data.InstoreApplicationDetail !== undefined) {
    if (data.InstoreApplicationDetail.CommunicationQueueId !== undefined) {
      commQueueId = parseInt(data.InstoreApplicationDetail.CommunicationQueueId)
    }
  }

  if (!commQueueId) {
    console.log('No CommQueueID, not saving application')
    return
  }

  const existingApplications = await offlineApplicationCollection.query(
    Q.where('commQueueId', commQueueId)
  ).fetch()

  if (existingApplications.length === 0) {
    await database.action(async () => {
      await offlineApplicationCollection.create(a => {
        // @ts-ignore
        a.createdAt = new Date()
        // @ts-ignore
        a.name = name
        // @ts-ignore
        a.endpoint = url
        // @ts-ignore
        a.method = 'POST'
        // @ts-ignore
        a.data = JSON.stringify(data)
        // @ts-ignore
        a.commQueueId = commQueueId!
      })
    })
  } else {
    for (let i = 0; i < existingApplications.length; i++) {
      database.action(async () => {
        await database.batch(
          ...existingApplications.map(ex => ex.prepareUpdate(a => {
            // @ts-ignore
            a.createdAt = new Date()
            // @ts-ignore
            a.name = name
            // @ts-ignore
            a.endpoint = url
            // @ts-ignore
            a.method = 'POST'
            // @ts-ignore
            a.data = JSON.stringify(data)
          }))
        )
      })
    }
  }
}


