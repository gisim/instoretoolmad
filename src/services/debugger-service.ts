import {database} from "./db-service"
import {v4 as uuid} from "uuid"
import {FormFieldErrorType} from "./form-service"
import {Config} from "../config"

export async function logResponse(url: string, json: any) {
  if (!Config.debug) {
    return json
  }

  const logCollection = database.collections.get('logs')
  await database.action(async () => {
    await logCollection.create(log => {
      // @ts-ignore
      log.logId = uuid()
      // @ts-ignore
      log.logType = 'network'
      // @ts-ignore
      log.createdAt = new Date()
      // @ts-ignore
      log.data = JSON.stringify(json, null, 2)
      // @ts-ignore
      log.name = url
    })
  })
  return json
}

export async function logValidation(name: string, errors: FormFieldErrorType[]) {
  if (!Config.debug) {
    return
  }

  const logCollection = database.collections.get('logs')
  await database.action(async () => {
    await logCollection.create(log => {
      // @ts-ignore
      log.logId = uuid()
      // @ts-ignore
      log.logType = 'validation'
      // @ts-ignore
      log.createdAt = new Date()
      // @ts-ignore
      log.data = JSON.stringify(errors, null, 2)
      // @ts-ignore
      log.name = name
    })
  })
}
