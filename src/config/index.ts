import config from './config.json'
import {ValueType} from "../services/form-service"

export interface ConfigType {
  mode: "develop" | "release"
  debug: boolean

  /**
   * ⚠️ Temporary!
   */
  TemporaryInitialState: { [key: string]: ValueType }
}

export const Config = config as ConfigType


