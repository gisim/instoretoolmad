import {FormProcessors} from "./form-processors"
import {ValueType} from "../services/form-service"

export type CompleteType =
  | CompleteFormType
  | CompleteErrorType
  | CompletePendingType
  | CompleteFinalType

export type CompleteFormType = {
  status: "complete"
  formData?: { [key: string]: ValueType }
}

export type CompleteErrorType = {
  status: "error"
  type: "rejected" | "technical"
  error: { title: string, message: string }
}

export type CompletePendingType = {
  status: "pending"
  creditLimit: string
}

export type CompleteFinalType = {
  status: "final"
  creditLimit: string
}

export type ProcessorType = "submit" | "saveAndContinue"

export type AvailableProcessors = {
  submit: UseFormReturnType,
  saveAndContinue?: UseFormReturnType
}

export type UseFormReturnType = {
  message: string,
  complete: CompleteType | null
  submit?: (formData: { [key: string]: any }) => void,
  isLoading?: boolean
  error?: string|null
}

export const useProcessor = (name: string|undefined, type: ProcessorType, formData: { [key: string]: any }|undefined): UseFormReturnType => {
  if (!name) { throw `processor is undefined` }
  if (!formData) { throw `formData is undefined` }

  return FormProcessors(name, type, formData)
}

