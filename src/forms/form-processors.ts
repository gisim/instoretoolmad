import {ProcessorType, UseFormReturnType} from "./form-processor"
import {NewApplicationFormProcessor} from "./new-application-form-processor"
import {
  CompleteApplicationFormProcessor,
  CompleteApplicationSaveAndContinueProcessor,
} from "./complete-application-form-processor"

export const FormProcessors = (name: string, type: ProcessorType, formData: { [key: string]: any }): UseFormReturnType => {
  const invalidMessage = `Processor ${name} does not exist`
  const invalidReturnType = { complete: null, message: invalidMessage }

  switch (name) {
    case 'new-application': {
      switch (type) {
        case "submit": {
          return NewApplicationFormProcessor(formData)
        }
        default: {
          return invalidReturnType
        }
      }
    }
    case 'complete-application': {
      switch (type) {
        case "submit": {
          return CompleteApplicationFormProcessor(formData)
        }
        case "saveAndContinue": {
          return CompleteApplicationSaveAndContinueProcessor(formData)
        }
        default: {
          return invalidReturnType
        }
      }
    }
    default: {
      return invalidReturnType
    }
  }
}
