import {useEffect, useRef, useState} from "react"
import {CompleteType} from "./form-processor"
import {AppStorageService} from "../services/app-storage-service"
import {ApplicationFormModel} from "../models/application-form-model"
import {useMutation} from "react-query"
import {getFinalOutcomeQuery, saveAndContinueMutation, submitApplicationMutation} from "../services/api-service"
import {SaveAndContinueModel} from "../models/save-and-continue"
import NetInfo from "@react-native-community/netinfo"

export const CompleteApplicationFormProcessor = (formData: { [key: string]: any }) => {
  const [message, setMessage] = useState('Loading…')
  const [complete, setComplete] = useState<CompleteType | null>(null)

  const [submitApplication, {data: submitApplicationData, isLoading: submitApplicationLoading, isError}] = useMutation(submitApplicationMutation)
  const [getFinalOutcome] = useMutation(getFinalOutcomeQuery)

  useEffect(() => {
      if (isError) {
        return
      }
      if (submitApplicationLoading) {
        return
      }
      if (submitApplicationData) {
        return
      }

      setMessage('Processing your application…')

      AppStorageService.getApplicationFormModel()
        .then((applicationFormModel) => {
          return processApplicationForm(applicationFormModel, formData)
        })
        .then((form) => {
          const submitModel: SaveAndContinueModel = {
            InstoreApplicationDetail: {
              CommunicationQueueId: formData.CommunicationQueueId,
              NewApplicationId: formData.NewApplicationId,
            },
            ProcessFlowDTO: form,
          }

          if ("PIRCustomerSignature" in formData) {
            submitModel.PromotionalInformationResult = [{
              TabId: "tab_LostCardProtection",
              IsChecked: true,
              HasSignatureComponent: true,
              SignatureBase64String: `data:image/png;base64,${formData.PIRCustomerSignature}`,
              DataField: 2,
              CustomCheckBox: [],
            }]
          }

          return submitApplication({model: submitModel})
        })
        .then(() => {
          return hasConnectivity()
            .then((connected) => {
              if (connected) {
                setMessage('Application in progress…')
                setTimeout(checkFinalOutcome, 3000)
              } else {
                setComplete({
                  status: "error", type: "technical", error: {
                    title: "Device offline",
                    message: "This device is offline but we have saved this application and it will upload automatically once it is connected to the network.",
                  },
                })
              }
            })
        })
        .catch((error) => {
          hasConnectivity()
            .then((connected) => {
              if (!connected && complete?.status !== "error") {
                setComplete({
                  status: "error", type: "technical", error: {
                    title: "Device offline",
                    message: "This device is offline but we have saved this application and it will upload automatically once it is connected to the network.",
                  },
                })
              }
            })
        })
    }, [submitApplication, submitApplicationData, submitApplicationLoading, setMessage, setComplete, isError]
  )

  useEffect(() => {
    if (!isError) {
      return
    }

    hasConnectivity()
      .then((connected) => {
        if (connected) {
          return
        }
        setComplete({
          status: "error", type: "technical", error: {
            title: "Device offline",
            message: "This device is offline but we have saved this application and it will upload automatically once it is connected to the network.",
          },
        })
      })
  }, [isError, setComplete])

  const checkFinalOutcome = () => {
    hasConnectivity()
      .then((connected) => {
        if (connected) {
          return getFinalOutcome({communicationQueueId: formData.CommunicationQueueId})
            .then((data) => {
              if (!data) {
                return
              }

              switch (data.ProcessStatusId) {
                case "91":
                case "92":
                case "93":
                case "94": {
                  setComplete({
                    status: "error",
                    type: "rejected",
                    error: {
                      title: "Technical error",
                      message: 'Oh Nooo! Something seems to have gone wrong. Please contact support.',
                    },
                  })
                  break
                }
                case "3":
                case "96":
                case "97": {
                  setMessage(`Performing your bureau check…`)
                  setTimeout(checkFinalOutcome, 3000)
                  break
                }
                case "4": {
                  setComplete({status: "final", creditLimit: data.CreditLimit})
                  break
                }
                case "5": {
                  setComplete({
                    status: "error",
                    type: "rejected",
                    error: {
                      title: "Rejected",
                      message: "Your application is unsuccessful due to you not meeting the minimum criteria, however you can apply for a Lay-By account (Subject to In Store availability). To get more information regarding your application, contact our call centre on 0800 222 639."
                    },
                  })
                  break
                }
                case "7": {
                  setComplete({status: "pending", creditLimit: data.InitialCreditLimit})
                  break
                }
              }
            })
        } else {
          throw "Not connected"
        }
      })
      .catch(error => {
        console.log('Complete Application Form Processor error:', error)
      })
  }

  const hasConnectivity = async (): Promise<boolean> => {
    try {
      const state = await NetInfo.fetch()
      return state.isConnected && (state.isInternetReachable ?? false)
    } catch {
      return false
    }
  }

  return {message, complete}
}

export const CompleteApplicationSaveAndContinueProcessor = (formData: { [key: string]: any }) => {
  const submittableFormData = useRef(formData)
  const applicationFormModel = useRef<ApplicationFormModel | null>(null)
  const [submitForm, {isLoading, isError}] = useMutation(saveAndContinueMutation)

  const error = isError ? "An error occurred while saving your form. Please try again later." : null

  const submit = async (newFormData: { [key: string]: any }) => {
    if (!applicationFormModel.current) {
      applicationFormModel.current = await AppStorageService.getApplicationFormModel()
    }
    submittableFormData.current = newFormData

    applicationFormModel.current = processApplicationForm(applicationFormModel.current!, submittableFormData.current)
    const saveAndContinueModel: SaveAndContinueModel = {
      InstoreApplicationDetail: {
        CommunicationQueueId: newFormData.CommunicationQueueId,
        NewApplicationId: newFormData.NewApplicationId,
      },
      ProcessFlowDTO: applicationFormModel.current!,
    }

    submitForm({model: saveAndContinueModel})
  }

  return {message: 'Loading…', complete: null, submit, error, isLoading}
}

const processApplicationForm = (form: ApplicationFormModel, submittableFormData: { [key: string]: any }): ApplicationFormModel => {
  const model = form
  const keys = Object.keys(submittableFormData)

  model.ProcessForms.forEach((processForm, processFormIndex) => {
    processForm.ProcessFormSections.forEach((processFormSection, processFormSectionIndex) => {
      processFormSection.ProcessFormFields.forEach((processFormField, processFormFieldIndex) => {
        if (keys.includes(processFormField.DestinationField)) {
          model
            .ProcessForms[processFormIndex]
            .ProcessFormSections[processFormSectionIndex]
            .ProcessFormFields[processFormFieldIndex]
            .ModelField = submittableFormData[processFormField.DestinationField]
        }
      })
    })
  })

  // ⚠️ Important!
  model.CurrentStepId = 0

  return model
}
