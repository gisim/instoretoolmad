import {useEffect, useState} from "react"
import {useMutation} from "react-query"
import {
  createNewApplicationMutation,
  performBureauCheckMutation,
  performPreScreeningMutation,
} from "../services/api-service"
import { CompleteType } from "./form-processor"
import {ValueType} from "../services/form-service"

export const NewApplicationFormProcessor = (formData: { [key: string]: any }) => {
  const [message, setMessage] = useState('Loading…')
  const [complete, setComplete] = useState<CompleteType|null>(null)

  const [createNewApplication, { data: createNewApplicationData, isLoading: createNewApplicationLoading, isError: createNewApplicationIsError }] = useMutation(createNewApplicationMutation)
  const [performPreScreening, { data: performPreScreeningData, isLoading: performPreScreeningLoading, isError: performPreScreeningIsError }] = useMutation(performPreScreeningMutation)
  const [performBureauCheck, { data: performBureauCheckData, isLoading: performBureauCheckLoading, isError: performBureauCheckIsError }] = useMutation(performBureauCheckMutation)

  useEffect(() => {
    if (createNewApplicationIsError) { return }
    if (createNewApplicationLoading) { return }
    if (createNewApplicationData) { return }
    setMessage('Processing your application…')
    createNewApplication({ form: formData })
      .catch(console.error)
  }, [createNewApplicationData, createNewApplicationLoading, createNewApplication, createNewApplicationIsError])

  useEffect(() => {
    if (performPreScreeningIsError) { return }
    if (!createNewApplicationData) { return }
    if (performPreScreeningData || performPreScreeningLoading) { return }
    setMessage('Performing your pre-screening check…')
    performPreScreening({ form: createNewApplicationData })
      .catch(console.error)
  }, [performPreScreeningData, performPreScreeningLoading, createNewApplicationData, performPreScreening, performPreScreeningIsError])

  useEffect(() => {
    if (performBureauCheckIsError) { return }
    if (!createNewApplicationData) { return }
    if (!performPreScreeningData) { return }
    if (performBureauCheckData || performBureauCheckLoading) { return }
    setMessage('Performing your bureau check…')
    performBureauCheck({ form: performPreScreeningData })
      .catch(console.error)
  }, [performBureauCheckData, performBureauCheckLoading, performPreScreeningData, performBureauCheck, performBureauCheckIsError])

  useEffect(() => {
    if (!performBureauCheckData) { return }
    if (!performBureauCheckData.BureauChecksPowerCurveResultDetail) { return }

    const { PowerCurveReasons } = performBureauCheckData.BureauChecksPowerCurveResultDetail

    if (PowerCurveReasons && PowerCurveReasons.length > 0) {
      const {ReasonDescription, ReasonCode} = PowerCurveReasons[0]
      console.log(`${ReasonDescription} (${ReasonCode})`)
      if (ReasonCode === "C0010") {
        const nextFormData: { [key: string]: ValueType } = {
          "CommunicationQueueId": { type: "int", value: `${performBureauCheckData.CommunicationQueueId}` },
          "NewApplicationId": { type: "int", value: `${performBureauCheckData.NewApplicationId}` },
        }
        setComplete({ status: "complete", formData: nextFormData })
      }
      else if (["RR01", "RR02", "RR03", "PE01", "PE02", "PE03", "PE04", "PE05", "PE06", "PE07", "PE08", "PE09"].includes(ReasonCode)) {
        console.log('You should never get there')
        setComplete({ status: "error", type: "technical", error: { title: "Technical Error", message: `Error: ${ReasonCode} / ${ReasonDescription}` }})
      }
      else {
        console.log(`${ReasonDescription} (${ReasonCode})`)
        setComplete({ status: "error", type: "rejected", error: { title: "Declined", message: "Your application is unsuccessful due to you not meeting the minimum criteria, however you can apply for a Lay-By account (Subject to In Store availability). To get more information regarding your application, contact our call centre on 0800 222 639." }})
      }
    } else {
      setComplete({ status: "error", type: "technical", error: { title: "Technical Error", message: 'Oh Nooo! Something seems to have gone wrong. Please contact support.' }})
    }
  }, [performBureauCheckData])

  const retryBureau = () => {
    if (!performPreScreeningData) { return }
    console.log('Retrying bureau check…')
    setMessage('Performing your bureau check…')
    performBureauCheck({ form: performPreScreeningData })
      .catch(console.error)
  }

  return { message, complete, submit: retryBureau }
}
