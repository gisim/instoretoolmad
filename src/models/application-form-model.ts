export interface ApplicationFormModel {
  ProcessFlowId:          number;
  CommunicationTypeId:    number;
  ProcessTypeId:          number;
  ProcessFlowName:        string;
  ProcessFlowDescription: string;
  IsImageViewerEnabled:   boolean;
  IsActive:               boolean;
  ProcessFlowHeader:      null;
  ToolTip:                null;
  ProcessForms:           ProcessForm[];
  InformationWindows:     any[];
  CurrentStepId:          number;
  ProcessFlowActions:     any[];
  ModelErrors:            null;
  CommunicationQueueId:   number;
  NewApplicationId:       number;
  ResponseStatus:         number;
  ResponseMessage:        string;
  ResponseMessageDetail:  null;
  IsValid:                boolean;
  ID:                     number;
  UserData:               null;
  UserName:               null;
  User:                   null;
}

export interface ProcessForm {
  ProcessName:          string;
  ProcessFormCommand:   null;
  ProcessDescription:   string;
  ProcessHeader:        string;
  StepId:               number;
  FormName:             string;
  RequiresValidation:   boolean;
  GoToNext:             boolean;
  IsActive:             boolean;
  ShowDone:             boolean;
  ShowNext:             boolean;
  ShowPrevious:         boolean;
  StepNameId:           number;
  StepState:            number;
  IsFormValid:          boolean;
  ProcessFormSections:  ProcessFormSection[];
  ValidationResult:     ValidationResult;
  ProcessFormActions:   any[];
  CommunicationQueueId: number;
  ProcessFormId:        number;
  NewApplicationId:     number;
}

export interface ProcessFormSection {
  ProcessFormId:          number;
  ProcessFormSectionId:   number;
  ProcessFormSectionDesc: string;
  DisplayOrder:           number;
  IsActive:               boolean;
  ProcessFormFields:      ProcessFormField[];
  CssClass:               CSSClass;
}

export enum CSSClass {
  ColS12M12L12 = "col s12 m12 l12",
  ColS6M6L6 = "col s6 m6 l6",
}

export interface ProcessFormField {
  ProcessFormId:        number;
  ProcessFormSectionId: number;
  ProcessFormFieldId:   number;
  ProcessFormFieldDesc: string;
  AttributeId:          number;
  DestinationField:     string;
  FieldInstance:        number;
  DisplayOrder:         number;
  ModelField:           any|null;
  AttributeDetail:      AttributeDetail;
  IsActive:             boolean;
  CssClass:             string;
  CustomAttributes:     null | string;
  Tooltip:              null | string;
  HasError:             boolean;
  ErrorMessage:         null;
}

export interface AttributeDetail {
  AttributeId:         number;
  AttributeName:       string;
  AttributeTypeId:     number;
  AttributeLookupId:   number | null;
  AttributeLookupName: string;
  IsActive:            boolean;
  AutoCompleteUrl:     AutoCompleteURL | null;
}

export enum AutoCompleteURL {
  AutoCompleteGetSuburbSearchResults = "/AutoComplete/GetSuburbSearchResults/",
  Empty = "",
}

export interface ValidationResult {
  IsFormValid:   boolean;
  FormStepState: number;
  Errors:        null;
}
