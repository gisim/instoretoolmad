export interface GetPreAgreementDataRequest {
  StoreCode:       number;
  UserCode:        string;
  UserName:        string;
  StaffId:         number;
  TillId:          number;
  CountryId:       number;
  DivisionId:      number;
  ReferenceNumber: string;
  IdentityNumber:  string;
}
