export interface ExistingInsuranceProduct {
  DivisionId:                number;
  FinancialServiceGroupId:   number;
  FinancialServiceGroupCode: string;
  FinancialServiceGroupDesc: string;
  FinancialServiceTypeId:    number;
  FinancialServiceTypeCode:  string;
  FinancialServiceTypeDesc:  string;
}
