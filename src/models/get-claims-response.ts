export interface GetClaimsResponse {
  UserClaim:             UserClaim[];
  ResponseStatus:        number;
  ResponseMessage:       string;
  ResponseMessageDetail: null;
}

export interface UserClaim {
  Id:            number;
  ClaimType:     string;
  ClaimValue:    string;
  UserId:        string;
  ApplicationId: number;
  AspNetClaimId: number;
  ClaimName:     string;
  IsActive:      null;
}
