export interface ValidateIDNumberResponse {
  IsIdentityNumberValid: boolean;
  IsFemale:              boolean;
  DateOfBirth:           string;
  DateOfBirthAsDateTime: Date;
  Age:                   number;
  ResponseStatus:        number;
  ResponseMessage:       string;
  IsValid:               boolean;
  ID:                    number;
}
