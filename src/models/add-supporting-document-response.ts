export interface AddSupportingDocumentResponse {
  SupportDocumentId:      number;
  UploadStatus:           boolean;
  SupportingDocumentType: number;
  FileType:               null;
  FileBytes:              null;
  IsActive:               boolean;
  DateUploaded:           Date;
  UploadedBy:             string;
  DocumentDescription:    string;
  ModelErrors:            null;
  CommunicationQueueId:   number;
  NewApplicationId:       number;
  ResponseStatus:         number;
  ResponseMessage:        string;
  ResponseMessageDetail:  null;
  IsValid:                boolean;
  ID:                     number;
  UserData:               null;
  UserName:               null;
  User:                   null;
}
