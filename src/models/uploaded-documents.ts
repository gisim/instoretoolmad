export interface UploadedDocuments {
  CommunicationQueueId:  number;
  DocumentImages:        DocumentImage[];
  ResponseStatus:        number;
  ResponseMessage:       ResponseMessage;
  ResponseMessageDetail: null;
  IsValid:               boolean;
  ID:                    number;
  UserData:              null;
  UserName:              null;
  User:                  null;
}

export interface DocumentImage {
  FaxImageFilename:      string;
  RotationAngle:         number;
  PageFromOffset:        number;
  PageToOffset:          number;
  CommunicationQueueId:  number;
  TotalFrameCount:       number;
  IsActive:              boolean;
  Base64Image:           null;
  Content:               null;
  ModelErrors:           null;
  NewApplicationId:      number;
  ResponseStatus:        number;
  ResponseMessage:       ResponseMessage;
  ResponseMessageDetail: null;
  IsValid:               boolean;
  ID:                    number;
  ImageList:             string[];
}

export enum ResponseMessage {
  Success = "Success",
}
