export interface InsuranceAccount {
  AccountDTO:            AccountDTO|null;
  ModelErrors:           null;
  ResponseStatus:        number;
  ResponseMessage:       string;
  ResponseMessageDetail: null;
  IsValid:               boolean;
  ID:                    number;
  UserData:              null;
  UserName:              null;
  User:                  null;
}

export interface AccountDTO {
  CustomerCategoryId:              number;
  CustomerId:                      number;
  DivisionId:                      number;
  CountryId:                       number;
  AccountId:                       number;
  AccountCode:                     string;
  PostalAccountAddressId:          number;
  ResidentialAccountAddressId:     number;
  ClubId:                          number;
  ClubTierId:                      number;
  ClubFeeMultiplyPercent:          number;
  IsLcpCharged:                    boolean;
  LcpfeeMultiplyPercent:           number;
  IsOpen:                          boolean;
  OpenedDate:                      Date;
  ReOpenedDate:                    Date;
  ClosedDate:                      Date;
  PreferenceStoreId:               number;
  IsSmsallowed:                    boolean;
  Modifyuser:                      string;
  ModifyStation:                   string;
  ModifyDate:                      Date;
  CreateUser:                      string;
  CreateStation:                   string;
  CreateDate:                      Date;
  StatementTypeId:                 number;
  IsTelephoneAllowed:              boolean;
  IsEmailAllowed:                  boolean;
  IsPostAllowed:                   boolean;
  IsCreditAssessmentAllowed:       boolean;
  AccountOriginId:                 number;
  IsMmsallowed:                    boolean;
  IsUsuryToNcaconversion:          boolean;
  HasFirstNcapurchase:             boolean;
  IsNcaconversionRefused:          boolean;
  IsOldToNewAgreementConversion:   boolean;
  HasFirstNewAgreementPurchase:    boolean;
  IsNewAgreementConversionRefused: boolean;
  AccountClosureId:                null;
}
