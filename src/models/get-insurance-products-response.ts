export interface GetInsuranceProductsResponse {
  InsuranceEligibleStatus:    boolean;
  NumOfFinancialServiceGroup: number;
  FinancialServiceGroup:      FinancialServiceGroup[];
  ModelErrors:                null;
  ResponseStatus:             number;
  ResponseMessage:            string;
  ResponseMessageDetail:      null;
  IsValid:                    boolean;
  ID:                         number;
  UserData:                   null;
  UserName:                   null;
  User:                       null;
}

export interface FinancialServiceGroup {
  FinancialServiceGroupId:       number;
  FinancialServiceGroupDesc:     string;
  NumOfFinancialServiceType:     number;
  BenefitsWording:               string;
  PolicyUrl:                     string;
  IsBeneficiaryInfoRequired:     boolean;
  IsRetrenchmentBenefitRequired: boolean;
  FinancialServiceType:          FinancialServiceType[];
}

export interface FinancialServiceType {
  FinancialServiceGroupId:  number;
  FinancialServiceTypeId:   number;
  FinancialServiceTypeDesc: string;
  FinancialServiceTypeRate: string;
  GroupName:                string;
}
