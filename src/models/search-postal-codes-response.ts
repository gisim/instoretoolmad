export interface PostalCodes {
  ArrayOfPostalCode:     ArrayOfPostalCode;
}

export interface ArrayOfPostalCode {
  PostalCode?: PostalCode[]|PostalCode;
}

export interface PostalCode {
  Description:       string;
  PostalTown:        string;
  PostalSuburb:      string;
  PostalCodeDesc:    string;
  PostalCountryId:   string;
  PostalTownId:      string;
  PostalCodeId:      string;
  PostalCodeType:    string;
  PostalCountryDesc: string;
}
