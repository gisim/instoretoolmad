export interface ActiveStore {
  DivisionId:        number;
  DivisionCode:      string;
  DivisionDesc:      string;
  StoreId:           number;
  StoreCode:         string;
  StoreDesc:         string;
  StoreLocationId:   number;
  StoreLocationCode: string;
  StoreLocationDesc: string;
}
