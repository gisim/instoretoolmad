export interface CreateFinancialServiceProductWithBeneficiaryResponse {
  ResponseStatus:        number;
  ResponseMessage:       string;
  ResponseMessageDetail: null;
  IsValid:               boolean;
  ID:                    number;
  UserData:              null;
  UserName:              null;
  User:                  null;
}
