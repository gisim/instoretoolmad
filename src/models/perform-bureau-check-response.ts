export interface PerformBureauCheckResponse {
  CommunicationQueueId:                  number;
  NewApplicationId:                      number;
  NewInstoreApplicationDetailDTO:        NewInstoreApplicationDetailDTO;
  InternalChecksPowerCurveResultDetail:  PowerCurveResultDetail;
  BureauChecksPowerCurveResultDetail:    PowerCurveResultDetail|null;
  RiskCalculationsResultDetail:          null;
  AffordabilityCalculationsResultDetail: null;
  ResponseStatus:                        number;
  ResponseMessage:                       string;
  ResponseMessageDetail:                 string|null;
  IsValid:                               boolean;
  ID:                                    number;
  UserData:                              null;
  UserName:                              null;
  User:                                  null;
}

export interface PowerCurveResultDetail {
  CommunicationQueueId:        number;
  PowerCurveResultId:          number;
  PowerCurveResultType:        number;
  PowerCurveInput:             string;
  PowerCurveOutput:            string;
  PowerCurveStage:             number;
  PowerCurveReasons:           PowerCurveReason[]|null;
  ApplicantId:                 number;
  OverrideAllowed:             boolean;
  NextProcessStatusId:         number;
  BureauProviderType:          number;
  RiskLimitCalculationsResult: null;
  FinalBusinessTermsResult:    null;
  ResponseStatus:              number;
  ResponseMessage:             string;
  ResponseMessageDetail:       null;
  IsValid:                     boolean;
  ID:                          number;
  UserData:                    null;
  UserName:                    null;
  User:                        null;
}

export interface PowerCurveReason {
  ReasonId:          number;
  ReasonType:        number;
  ReasonDescription: string;
  ReasonCode:        string;
}

export interface NewInstoreApplicationDetailDTO {
  TitleId:              number;
  FirstName:            string;
  FirstName2:           null;
  Surname:              string;
  IdentityNumber:       string;
  IdentityTypeId:       number;
  RepaymentPeriodId:    number;
  DivisionId:           number;
  IsUnderDebtReview:    boolean;
  IsCreditConsentGiven: boolean;
  CellphoneNumber:      string;
  GrossMonthlyIncome:   number;
  EmployerName:         string;
  EmploymentPeriod:     string;
  DateOfBirth:          Date;
  StaffCode:            null;
  GenderId:             number;
  StoreId:              number;
  OccupationId:         null;
  CommunicationTypeId:  null;
  OccupationDesc:       null;
}
