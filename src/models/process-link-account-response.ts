export interface ProcessLinkAccountResponse {
  CustomerCategoryId:    number;
  CustomerId:            number;
  DivisionId:            number;
  CountryId:             number;
  AccountId:             number;
  ContractGroupId:       number;
  ContractTypeId:        number;
  RepaymentPeriodId:     number;
  InterestTypeId:        number;
  CollectionAgentId:     number;
  AccountFacilityId:     number;
  EmbossCardNumberId:    number;
  EmbossCardTypeid:      number;
  ResponseStatus:        number;
  ResponseMessage:       string;
  IsValid:               boolean;
  ID:                    number;
}
