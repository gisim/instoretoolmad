export interface GetCustomerApplicationsResponse {
  ApplicationDetailList: ApplicationDetailList[];
}

export interface ApplicationDetailList {
  NewApplicationId:         number;
  CommunicationQueueId:     number;
  IdentityNumber:           string;
  AccountCode?:             string|null;
  ProcessAtDate:            Date;
  ProcessStatusId:          number;
  RepaymentPeriodId:        number;
  RepaymentPeriodDesc:      string;
  DivisionId:               number;
  DivisionDesc:             string;
  ChannelId:                number;
  StoreId:                  number;
  IsComplete:               string;
  IsInUse:                  string;
  CreateDate:               Date;
  ModifiedDate:             Date;
  ChannelDescription:       string;
}
