import { PreAgreementInformation } from "../components/data-display/credit-pre-agreement";

export interface GetPreAgreementDataResponse {
  preAgreementInformation: PreAgreementInformation
  AssociateDetail: {
    "StaffFirstName": string
    "StaffSurname": string
  }
}
