export interface ProcessLinkAccountRequest {
  IdentityNumber: string
  DivisionId:     string
  StoreCode:      string
  eReferenceCode: string
  AccountCode:    string
}
