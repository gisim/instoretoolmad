import {appSchema, tableSchema} from '@nozbe/watermelondb'

export default appSchema({
  version: 5,
  tables: [
    tableSchema({
      name: 'logs',
      columns: [
        { name: 'log_id', type: 'string', isIndexed: true },
        { name: 'logtype', type: 'string' },
        { name: 'created_at', type: 'number', isIndexed: true },
        { name: 'data', type: 'string' },
        { name: 'name', type: 'string' },
      ],
    }),
    tableSchema({
      name: 'applications',
      columns: [
        { name: 'created_at', type: 'number', isIndexed: true },
        { name: 'name', type: 'string', isIndexed: true },
        { name: 'endpoint', type: 'string' },
        { name: 'method', type: 'string' },
        { name: 'data', type: 'string' },
        { name: 'commQueueId', type: 'number', isIndexed: true },
      ],
    }),
    tableSchema({
      name: 'postal_codes',
      columns: [
        { name: 'codeId', type: 'number' },
        { name: 'townId', type: 'number' },
        { name: 'countryId', type: 'number' },
        { name: 'code', type: 'number', isIndexed: true },
        { name: 'suburb', type: 'string', isIndexed: true },
        { name: 'town', type: 'string', isIndexed: true },
      ],
    }),
  ],
})
