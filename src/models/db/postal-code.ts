import { Model } from "@nozbe/watermelondb"
import { field } from '@nozbe/watermelondb/decorators'

export class PostalCode extends Model {
  static table = 'postal_codes'

  @field('codeId') codeId!: number
  @field('townId') townId!: number
  @field('countryId') countryId!: number
  @field('code') code!: number
  @field('suburb') suburb!: string
  @field('town') town!: string
}
