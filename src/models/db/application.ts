import { Model } from "@nozbe/watermelondb"
import { field, date } from '@nozbe/watermelondb/decorators'

export class OfflineApplication extends Model {
  static table = 'applications'

  @date('created_at') createdAt!: Date
  @field('name') name!: string
  @field('endpoint') endpoint!: string
  @field('method') method!: string
  @field('data') data!: string
  @field('commQueueId') commQueueId!: number
}
