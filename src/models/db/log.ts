import { Model } from "@nozbe/watermelondb"
import { field, date } from '@nozbe/watermelondb/decorators'

export class Log extends Model {
  static table = 'logs'

  @field('log_id') logId!: string
  @field('logtype') logType!: string
  @date('created_at') createdAt!: Date
  @field('data') data!: string
  @field('name') name!: string
}
