import {schemaMigrations, addColumns, createTable} from '@nozbe/watermelondb/Schema/migrations'

export default schemaMigrations({
  migrations: [
    {
      toVersion: 2,
      steps: [
        addColumns({
          table: 'logs',
          columns: [
            { name: 'name', type: 'string' },
          ],
        }),
      ],
    },
    {
      toVersion: 3,
      steps: [
        createTable({
          name: 'applications',
          columns: [
            { name: 'created_at', type: 'number', isIndexed: true },
            { name: 'name', type: 'string', isIndexed: true },
            { name: 'endpoint', type: 'string' },
            { name: 'method', type: 'string' },
            { name: 'data', type: 'string' },
          ],
        }),
      ],
    },
    {
      toVersion: 4,
      steps: [
        createTable({
          name: 'postal_codes',
          columns: [
            { name: 'codeId', type: 'number' },
            { name: 'townId', type: 'number' },
            { name: 'countryId', type: 'number' },
            { name: 'code', type: 'number', isIndexed: true },
            { name: 'suburb', type: 'string', isIndexed: true },
            { name: 'town', type: 'string', isIndexed: true },
          ],
        }),
      ],
    },
    {
      toVersion: 5,
      steps: [
        addColumns({
          table: 'applications',
          columns: [
            { name: 'commQueueId', type: 'number', isIndexed: true },
          ],
        }),
      ],
    },
  ],
})
