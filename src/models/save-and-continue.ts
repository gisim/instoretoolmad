import {ApplicationFormModel} from "./application-form-model"

export interface SaveAndContinueModel {
  InstoreApplicationDetail: {
    CommunicationQueueId: number
    NewApplicationId: number
  }
  ProcessFlowDTO: ApplicationFormModel,
  PromotionalInformationResult?: PromotionalInformationResult[],
}

export interface PromotionalInformationResult {
  TabId: string
  DataField: number
  HasSignatureComponent: boolean
  SignatureBase64String: string
  IsChecked: boolean
  CustomCheckBox: CustomCheckBox[]
}

interface CustomCheckBox {
  CheckBoxField: string
  IsChecked: boolean
}
