export interface GetAvailableAccountOptionsResponse {
  ApplicationApplyObjectListDTO?: ApplicationApplyObjectListDTO[];
  ModelErrors:                    string;
  ResponseStatus:                 number;
  ResponseMessage:                string;
  IsValid:                        boolean;
  ID:                             number;
  error?:                         string
}

export interface ApplicationApplyObjectListDTO {
  DivisionId:               number;
  DivisionDescription:      string;
  RepaymentPeriod:          RepaymentPeriod[];
  DivisionLogoBase64String: string;
}

export interface RepaymentPeriod {
  RepaymentPeriodId:          number;
  RepaymentPeriodDescription: string;
  CurrentStatus:              null;
  IsInProgress:               boolean;
  HasGotAccountAlready:       boolean;
}
