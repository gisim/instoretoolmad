export interface Form {
  title?: string
  pages: Page[]
  formProcessor: string
  initialModal?: string
  onSuccess: FormOnSuccess
  supportsSaveAndContinue: boolean
}

export type FormOnSuccessFormType = {
  type: "form"
  name: string
}

export type FormOnSuccessSuccessType = {
  type: "success"
}

export type FormOnSuccess =
  | FormOnSuccessSuccessType
  | FormOnSuccessFormType


export interface Page {
  page: number
  title?: string
  subtitle?: string
  description?: string
  sections: Section[]
}

export interface Section {
  title?: string
  subtitle?: string
  components?: Array<Component[]>
}

export interface Component {
  type: "button" | "checkbox" | "dropdown" | "email" | "expanding_cell" | "money" | "number" | "phone" | "postalcode" | "radio" | "signature" | "text" | "uploadbutton" | "date" | "vertical_radio" | "repayment_period" | "timeperiod" | "blank" | "checkbox_postalasphysical"
  title: string
  name: FormFieldName
  options?: Option[]
  optionsLookUp?: string
  required?: boolean
  dependentOn?: Dependency
  details?: { [key: string]: string }
  children?: Component[]
  action?: "submit" | "saveAndExit" | "documentUploader"
  vocab?: {
    singular: string
    plural: string
  }

  // These is populated by the form service
  path: string
}

export interface Option {
  title: string
  value: string
  extra?: string
}

export interface Dependency {
  key: string
  value: string
  type: "hide" | "disable"
}

export interface FormFieldName {
  name: string
  type: FormNameType
}

export type FormNameType =
  | "string"
  | "int"
  | "float"
  | "date"
  | "bool"
