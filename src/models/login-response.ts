export interface LoginResponse {
  ResponseMessage:       string;
  ResponseMessageDetail: string|null;
  ResponseStatus:        number;
  User:                  User;
  UserLoginToken:        string;
}

export interface User {
  Email:                string;
  Firstname:            null;
  Id:                   string;
  PasswordHash:         string;
  QuickLoginExpiryDate: Date;
  Surname:              null;
  UserName:             string;
}
