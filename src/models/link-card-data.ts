export interface LinkCardData {
  IdentityNumber: string
  DivisionId:     string
  StoreCode:      string
  StoreId:        string
  StoreDesc:      string
  DivisionDesc:   string
  eReferenceCode: string
  CreditLimit:    string
  AccountCode:    string
}
