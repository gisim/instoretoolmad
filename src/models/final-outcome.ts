export interface FinalOutcome {
  CommunicationQueueId: string
  ProcessStatusId:      string
  ProcessStatus:        string
  CreditLimit:          string
  InitialCreditLimit:   string
  RepaymentPeriod:      string
  Division:             string
  eReferenceCode:       string
}
