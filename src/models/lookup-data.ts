export interface LookUp {
  Titles:                          LookUpData[];
  Divisions:                       LookUpData[];
  Genders:                         LookUpData[];
  RepaymentPeriods:                LookUpData[];
  CellphoneServiceProviders:       LookUpData[];
  IdentityTypes:                   LookUpData[];
  MarketSectors:                   LookUpData[];
  DebtorsBrand:                    LookUpData[];
  MaritalStatus:                   LookUpData[];
  CellProviderPlanTypes:           LookUpData[];
  PreferredLanguage:               LookUpData[];
  ResidentialStatus:               LookUpData[];
  PostalTown:                      LookUpData[];
  PostalCodes:                     LookUpData[];
  Occupation:                      LookUpData[];
  EmploymentContractTypes:         LookUpData[];
  FinancialInstitutions:           LookUpData[];
  Relations:                       LookUpData[];
  SupportingDocuments:             LookUpData[];
  EmploymentType:                  LookUpData[];
  StatementTypes:                  LookUpData[];
  PreferredCommunicationType:      LookUpData[];
  CustomerSalaryFrequency:         LookUpData[];
  CustomerSalaryDate:              LookUpData[];
  OverrideAppealReasons:           LookUpData[];
  OverrideDecisions:               LookUpData[];
  OverrideReasons:                 LookUpData[];
  ProcessTypes:                    LookUpData[];
  YesNoOption:                     LookUpData[];
  FinancialServiceType:            LookUpData[];
  CustomerProtectionPlans:         LookUpData[];
  LostCardProtectionPlans:         LookUpData[];
  RedCapFoundation:                LookUpData[];
  MedinetPlans:                    LookUpData[];
  CommuterPersonalAccidentPlans:   LookUpData[];
  DomesticCarePlans:               LookUpData[];
  FamilyFuneralPlans:              LookUpData[];
  ThreeSixtyDegreeProtectionPlans: LookUpData[];
  CommunicationTypes:              LookUpData[];
  ProcessStatuses:                 LookUpData[];
  RejectApplicationReasons:        LookUpData[];
  EmploymentConfirmed:             LookUpData[];
  TelephoneCallOutcome:            LookUpData[];
}

export interface LookUpData {
  Id:          number;
  Code:        string|null;
  Description: string;
}
