export interface CreateFinancialServiceProductWithBeneficiaryRequest {
  AccountCode:                string;
  ActualSalePrice:            number;
  FinancialServiceGroupId:    number;
  FinancialServiceTypeId:     number;
  SalesChannel:               string;
  BeneficiaryIndentityNumber: null;
  BeneficiaryFirstName:       null;
  BeneficiaryLastName:        null;
  IMEINumber:                 string;
  SaleReferenceNumber:        string;
  DeviceCoverProductDesc:     string;
}
