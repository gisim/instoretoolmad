export interface GetInsuranceProductsRequest {
  DivisionId: number
  AccountCode: string
  CommunicationTypeId: number
}
