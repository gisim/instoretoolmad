export interface UploadInsuranceDocumentRequest {
  CustomerCategoryId: number,
  CustomerId: number
  DocumentTypeId: number
  ImageData: string
}
