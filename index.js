/**
 * @format
 */

import {AppRegistry} from 'react-native'
import App from './App'
import {name as appName} from './app.json'
import { LogBox } from 'react-native'

// import {Promise} from "bluebird"
// global.Promise = Promise

LogBox.ignoreLogs([
    'componentWillReceiveProps has been renamed',
    'componentWillMount has been renamed',
])

AppRegistry.registerComponent(appName, () => App)
